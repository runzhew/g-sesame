<?php
	session_start();

	require("config.php");

	$tool_id = $_GET['id'];
	$tpl->assign("tool_id", $tool_id);
	

	switch ($tool_id) {
		case 1:
			$tool_name = "Semantic Similarity of two GO terms";
			$tool_page = "GOCompareTwo1";
			break;
		case 2:
			$tool_name = "Semantic Similarity of two GO terms (statistical methods)";
			$tool_page = "GOCompareTwo1_AIC";
			break;
		case 3:
			$tool_name = "Semantic Similarity of two GO term sets";
			$tool_page = "GOCompareMultiple1";
			break;
		case 4:
			$tool_name = "Semantic Similarity of two GO term sets (statistical methods)";
			$tool_page = "GOCompareMultiple1_AIC";
			break;
		case 5:
			$tool_name = "Functional Similarity of two genes";
			$tool_page = "geneCompareTwo1";
			break;
		case 6:
			$tool_name = "Functional Similarity of two genes (statistical methods)";
			$tool_page = "geneCompareTwo1_AIC";
			break;
		case 7:
			$tool_name = "Gene Clustering Tool Based on G-SESAME Method";
			$tool_page = "geneCluster1";
			break;
		case 8:
			$tool_name = "Gene Clustering Tool Based on Resnik, Jiang, and Lin's Method";
			$tool_page = "geneClusterStatistics1";
			break;
		case 9:
			$tool_name = "Search Top N Similar Genes";
			$tool_page = "geneTop1";
			break;
		
		default:
			break;
	}

	$tpl->assign("tool_name", $tool_name);
	$tpl->assign("tool_page", $tool_page);
	$html = $tpl->draw('tool', $return_string = true);
	echo $html;
?>