<?php include( "../XHTML/header.txt" ); ?>

<h2>Semantic similarities of two GO term sets (Statitical methods)</h2>
   
<ul>   

<li>Select two files containing GO term accession numbers, such as <b>0005739</b>, for uploading (one accession number per line).<b> Sample file: <a href = "../XHTML/term.txt"> Click Here!</a></b></li>
<li>Press "Submit" button and wait for the results.</li>
</ul>
   
   
<form enctype="multipart/form-data" action="GOCompareMultiple2_AIC.php" method="POST">
    <table>
<tr><td>File 1:</td>
<td><input name="uploadedfile1" type="file" ></td></tr>
<tr><td>File 2:</td> 
<td><input name="uploadedfile2" type="file" ></td>
</tr>
</table>

    <p> Methods</p>

    <input type = "radio" name = "method" value = "Resnik" checked> Resnik
    <input type = "radio" name = "method" value = "Lin"> Lin
          <input type = "radio" name = "method" value = "Jiang"> Jiang
    <input type = "radio" name = "method" value = "AIC"> AIC

      
<p></p>

                                
<h4>Since measuring the semantic similarities of two sets of GO terms may take very long time, you may check the email option and fill in your email address. The results will be mailed to you when they are ready.</h4>


<p><input name="email" type= "checkbox"> Email option<br />

<table border="0">
                                                    
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Email address: </td><td><input name="emailAddress" type="text" size="20" maxlength="40" ></td></tr>
                                                    
<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;</td><td>Name of the result: </td><td><input name= "description" type="text" size= "10" maxlength ="30"> (Optional )</td></tr>
                                                    
</table>
                                                    </p>

  <input name="submit" value="Submit" type="submit">  <input name="Reset" type="reset">
      
</form>
     

   <?php include( "../XHTML/footer.txt" ); ?>

