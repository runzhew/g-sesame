<?php 
session_start();
require("../config.php");
include_once ("Lib/GOCompareTwo_AIC.php"); 
include_once ("Lib/dot.php"); 
include_once ("Lib/GOCompareTwoStatistics.php");

//obtain data from web

$tpl->assign("tool_id", $_POST['tool_id']);
$term1 = $_POST['GOTerm1'];
$term1 = trim($term1);
$term15 = $term1;
$term1 = prepareGOTerm($term1);
$tpl->assign("term1", $term1);

$term2 = $_POST['GOTerm2'];
$term2 = trim($term2);
$term25 = $term2;
$term2 = prepareGOTerm($term2);
$tpl->assign("term2", $term2);

$method = $_POST['method'];
$tpl->assign("method", $method);

//connect to the database
$link = connectToMySQL();


$id1 = getTermId ($term1);
$tpl->assign("id1", $id1);

$id2 = getTermId ($term2); 
$tpl->assign("id2", $id2);

$termName1 = getTermName ($id1);
$tpl->assign("termName1", $termName1);

$termName2 = getTermName ($id2);
$tpl->assign("termName2", $termName2);


//get term def
$termDefinition1 = getTermDefinition ($id1);
$termDefinition2 = getTermDefinition ($id2);
$tpl->assign("termDefinition1", $termDefinition1);
$tpl->assign("termDefinition2", $termDefinition2);

$termSynonym1 = getTermSynonym ($id1);
$termSynonym2 = getTermSynonym ($id2);
$tpl->assign("termSynonym1", $termSynonym1);
$tpl->assign("termSynonym2", $termSynonym2);

$termFrequencies = array();

$ontology1 = getOntology($id1);
$ontology2 = getOntology($id2);



if ($ontology1 != $ontology2)
     $similarity = 0;
else {
     /* $produceCountAll = getProductCountAll(); */
     /* get count on specific ontology */
     /* $produceCountAll = getProductCountOntology($ontology1); */

     $produceCountAll = getOntologyCount_database($ontology1);
// get count on all three ontologies

     if ($method == "AIC")
	  $similarity = compareTwoGOIdsAIC($id1, $id2, $produceCountAll);
     else {
	  $tmp = "compareTwoGOIds". $method;
	  $similarity = $tmp($id1, $id2, $termFrequencies, $produceCountAll);
     }
}

$similarity2 = number_format ($similarity, 3);
$tpl->assign("similarity2", $similarity2);

  $result_page = "result1+2";
  $tpl -> assign("result_page", $result_page);
  getDotDAGFromTwoGOs ($id1, $id2, $imageFileName);
  $tpl->assign("imageFileName", $imageFileName);

 $html = $tpl->draw('tool', $return_string = true);
 echo $html;


//echo "<h2>Semantic Similarity of Two GO Terms ($method's method)</h2>";


//echo "<div class=\"emphasis1\">Semantic similarity of GO terms " . $term1 . " and " . $term2 . " is: <div class=\"emphasis2\">" . $similarity2 . "</div></div>";


//getClientInformation ("GOCompareTwo", "no");


?>


<!-- <h3>More information:</h3>

<table border="1">
  
  <tr><th> &nbsp;  </th> <th> GO Term 1: </th><th> GO Term 2: </th></tr>
  
 <tr>
  <td>Accession&nbsp;ID:</td>
  <td><?php echo "$term1 (id is: $id1)"; ?></td>
  <td><?php echo "$term2 (id is: $id2)"; ?></td>
  </tr> 
  
 <tr>
  <td>Name:</td>
  <td><?php echo $termName1; ?></td>
  <td><?php echo $termName2; ?></td>
  </tr>
  
  
  <tr>
  <td>Definition:</td>
  <td><?php echo $termDefinition1; ?></td>
  <td><?php echo $termDefinition2; ?></td>
  </tr>
  
  
  <tr>
  <td>Synonyms:</td>
  <td><?php echo $termSynonym1; ?></td>
  <td><?php echo $termSynonym2; ?></td>
  </tr>
  
  
  <tr>
  <td>DAG:</td>
  <td colspan="2"> -->
                                 
/*<?php 
  /*getDotDAGFromTwoGOs ($id1, $id2, $imageFileName);
echo "<a href=\"$imageFileName\" target=\"_blank\">";
echo "<img src=\"$imageFileName\" height=\"100\", width=\"100\" />";
echo "</a>"; */
?> 



<!--<div class="emphasis1">Notes:</div>

<ul>
<li>Click the icon to display the full size graph.</li>
<li>
<div class="colorCyan"><b>The Cyan nodes</b></div>&nbsp is the GO term <?php echo "$term1"; ?>; 
<div class="colorOrange"><b>The Orange nodes</b></div>&nbsp is the GO term <?php echo "$term2"; ?>; 
</li>
<li>Solid arrows represent "is-a" relations and dashed arrows represent "part-of" relations.</li>
</ul>

</td>
</tr>

</table> -->


<?php 
mysql_close ($link);
//include("../XHTML/footer.txt");
?>

