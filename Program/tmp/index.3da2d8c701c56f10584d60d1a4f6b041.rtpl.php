<?php if(!class_exists('raintpl')){exit;}?><?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("head") . ( substr("head",-1,1) != "/" ? "/" : "" ) . basename("head") );?>

<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("header") . ( substr("header",-1,1) != "/" ? "/" : "" ) . basename("header") );?>

<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("fullViewLogo") . ( substr("fullViewLogo",-1,1) != "/" ? "/" : "" ) . basename("fullViewLogo") );?>

<div class="container">
	<hr>
	<div class="row">
		<div class="col-lg-12 ">
			<div class="q1" style="margin-bottom: 100px">
				<h2 class="section-lead">
					<b>Who are using the G-SESAME tools?</b>
				</h2>
				<p>
					G-SESAME tools have been used more than 76.6 million times by researchers from 71 countries between October 2006 and May 2014 according to our
					<a href="web_log.php">
						
					 web log recordes.
					</a>
				</p>
			</div>
			<div class="q2" style="margin-bottom: 100px">
				<h2 class="section-lead">
					<b>Which Version of the gene ontology database is used by G-SESAME tools?</b>
				</h2>
				<p>
					G-SESAME is currently using the gene ontology database published by the gene ontology consortium in
					<a href="http://archive.geneontology.org/full/2013-06-01/" target="_blank">
						June, 2013.
					</a> 
				</p>
			</div>
			<div class="q3" style="margin-bottom: 100px">
				<h2 class="section-lead">
					<b>How to cite the G-SESAME tools?</b>
				</h2>
				<p>
					Please cite the following paper published in Bioinformatics:
				   <ul>
					   <li>
							James Z. Wang, Zhidian Du, Rapeeporn Payattakool, Philip S. Yu and Chin-Fu Chen, A New Method to Measure the Semantic Similarity of GO Terms, Bioinformatics, 2007, 23: 1274-1281; doi: 10.1093/bioinformatics/btm087 
						</li>
					   <li>
					   		James Z. Wang, Zhidian Du, Philip S. Yu and Chin-Fu Chen, An Effient Online Tool to Search Top-N Genes with Similar 	Biological Functions in Gene Ontology Database, IEEE BIBM, 2007
						</li>
					   <li>
							Zhidian Du, Lin Li, Chin-Fu Chen, Philip S. Yu, and James Z. Wang. G-SESAME: web tools for go term based gene similarity analysis and knowledge discovery. Nucleic Acids Research, 37:W345-W349, 2009.
						</li>

						<li>
							Xuebo Song, Lin Li, Pradip K. Srimani, Philip S. Yu and James Z. Wang. Measure the semantic similarity of go terms using aggregate information content, ISBRA, 2013.
						</li>
					</ul>
				</p>
			</div>
			<div class="q4">
				<h2 class="section-lead">
					<b>Sponsor Information?</b>
				</h2>
				<p>
					This project is supported by NSF grant DBI-0960586 and DBI-0960443.
				</p>
			</div>
		</div>
	</div>
</div>
	
<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("footer") . ( substr("footer",-1,1) != "/" ? "/" : "" ) . basename("footer") );?>

