<?php if(!class_exists('raintpl')){exit;}?><p>
	<b> Semantic similarity of GO terms</b> <i><?php echo $term1;?></i> and <i><?php echo $term2;?></i> is <span style="color: red;"><b><?php echo $similarity2;?></b></span>
</p>

<h2 style="color: red;"><b> More Information </b></h2>

<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<th><?php echo $term1;?></th>
			<th><?php echo $term2;?></th>
		</tr>	
	</thead>
	<tbody>
		<tr>
			<td><b>Accession ID</b></td>
			<td><?php echo $term1;?> (id is <?php echo $id1;?>)</td>
			<td><?php echo $term2;?> (id is <?php echo $id2;?>)</td>
		</tr>
		<tr>
			<td><b>Name</b></td>
			<td><?php echo $termName1;?></td>
			<td><?php echo $termName2;?></td>
		</tr>
		<tr>
			<td><b>Definition</b></td>
			<td><?php echo $termDefinition1;?></td>
			<td><?php echo $termDefinition2;?></td>
		</tr>
		<tr>
			<td><b>Synonyms</b></td>
			<td><?php echo $termSynonym1;?></td>
			<td><?php echo $termSynonym2;?></td>
		</tr>
		<tr>
			<td><b>DAG</b></td>
			<td colspan="2">
				<a href="<?php echo $imageFileName;?>" target="_blank">
					<img src="<?php echo $imageFileName;?>" height="100" width="100" alt="image">
				</a>
				<div class="emphasis1">Notes:</div>

<ul>
<li>Click the icon to display the full size graph.</li>
<li>
<div class="colorCyan"><b>The Cyan nodes</b></div>&nbsp is the GO term <?php echo $term1;?>; 
<div class="colorOrange"><b>The Orange nodes</b></div>&nbsp is the GO term <?php echo $term2;?>; 
</li>
<li>Solid arrows represent "is-a" relations and dashed arrows represent "part-of" relations.</li>
</ul>
			</td>
		</tr>
	</tbody>




</table>


