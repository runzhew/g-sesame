<?php if(!class_exists('raintpl')){exit;}?><?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("head") . ( substr("head",-1,1) != "/" ? "/" : "" ) . basename("head") );?>

<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("header") . ( substr("header",-1,1) != "/" ? "/" : "" ) . basename("header") );?>

<div class="container" >
	<div class="row">
		<div class="col-sm-3 col-md-3 section">
			<h3> Tools </h3>
			<hr>
			<ul class=" list-unstyled">
				<li class="nav-header">
                	<a href="#" data-toggle="collapse" data-target="#tool1" style="font-size: 14px;">Term Analysis <b class="right-caret"></b></a>
                	<ul class='nav nav-tab list-unstyled collapse <?php if( $tool_id < 5 ){ ?> in <?php } ?>' id="tool1">
                		<li class='<?php if( $tool_id == 1 ){ ?> active <?php } ?>'><a href="/rain/tools.php?id=1">Semantic Similarity of two GO terms</a></li>
			            <li class='<?php if( $tool_id == 2 ){ ?> active <?php } ?>'><a href="/rain/tools.php?id=2">Semantic Similarity of two GO terms (statistical methods)</a></li>
			            <li class='<?php if( $tool_id == 3 ){ ?> active <?php } ?>'><a href="/rain/tools.php?id=3">Semantic Similarity of two GO term sets</a></li>
			            <li class='<?php if( $tool_id == 4 ){ ?> active <?php } ?>'><a href="/rain/tools.php?id=4">Semantic Similarity of two GO term sets (Statistical methods)</a></li>
                	</ul>
                </li>
                <li class="nav-header">
                	<a href="#" data-toggle="collapse" data-target="#tool2" style="font-size: 14px;">Gene Analysis <b class="right-caret"></b></a>
                	<ul class='nav nav-tab list-unstyled collapse <?php if( ($tool_id > 4) && ($tool_id < 7) ){ ?> in <?php } ?>' id="tool2">
                		<li class='<?php if( $tool_id == 5 ){ ?> active <?php } ?>'><a href="/rain/tools.php?id=5">Functional Similarity of two genes</a></li>
			            <li class='<?php if( $tool_id == 6 ){ ?> active <?php } ?>'><a href="/rain/tools.php?id=6">Functional Similarity of two genes (Statistical methods)</a></li>
                	</ul>
                </li>
                <li class="nav-header">
                	<a href="#" data-toggle="collapse" data-target="#tool3" style="font-size: 14px;">Knowledge Discovery <b class="right-caret"></b></a>
                	<ul class='nav nav-tab list-unstyled collapse <?php if( $tool_id > 6 ){ ?> in <?php } ?>' id="tool3">
                		<li class='<?php if( $tool_id == 7 ){ ?> active <?php } ?>'><a href="/rain/tools.php?id=7">Gene Clustering Tool Based on G-SESAME Method</a></li>
			            <li class='<?php if( $tool_id == 8 ){ ?> active <?php } ?>'><a href="/rain/tools.php?id=8">Gene Clustering Tool Based on Resnik, Jiang, and Lin's Method</a></li>
			            <li class='<?php if( $tool_id == 9 ){ ?> active <?php } ?>'><a href="/rain/tools.php?id=9">Search Top N Similar Genes</a></li>
            		</ul>
            	</li>
        	</ul>
		</div>
		<div class="col-sm-9 col-md-9 section" role="main" id="video_section">
			<div class="tab-pane active" id="popular" style="margin: 0; padding: 0;" >
				<h3 id="catergory_tite"><?php if( isset($_POST['submit']) == true ){ ?><b> Result: </b> <?php }else{ ?> <?php echo $tool_name;?> <?php } ?> </h3>
				<hr>
			</div>
				<?php if( isset($_POST['submit']) == true ){ ?>

					<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("".$result_page."") . ( substr("".$result_page."",-1,1) != "/" ? "/" : "" ) . basename("".$result_page."") );?> 
					<!-- <p> hello</p> -->
				<?php }else{ ?>

					<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("".$tool_page."") . ( substr("".$tool_page."",-1,1) != "/" ? "/" : "" ) . basename("".$tool_page."") );?>

				<?php } ?>

				
		</div>
	</div>	
</div>
<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("footer") . ( substr("footer",-1,1) != "/" ? "/" : "" ) . basename("footer") );?>

