<?php if(!class_exists('raintpl')){exit;}?><p>
	<b> Semantic similarity of two GO term sets is: </b> 
		<span style="color: red;">
			<b>
				<?php echo  number_format($result,3) ?></b>
		</span>
</p>
<h2 style="color: red;"><b> Similarity Table </b></h2>

<table class="table table-hover">
	<thead>
		<tr>
			<th></th>
			<?php $counter1=-1; if( isset($GOTerms1) && is_array($GOTerms1) && sizeof($GOTerms1) ) foreach( $GOTerms1 as $key1 => $value1 ){ $counter1++; ?>

				<th> GO:<?php echo $value1;?> </th>
			<?php } ?>

		</tr>
	</thead>
	<tbody>
	<?php $counter1=-1; if( isset($GOTerms2) && is_array($GOTerms2) && sizeof($GOTerms2) ) foreach( $GOTerms2 as $key1 => $value1 ){ $counter1++; ?>

	<tr>
		<th>GO: <?php echo $value1;?></th>
			<!-- <?php echo $row_counter = $counter1;?> -->
			<?php $counter2=-1; if( isset($similarity_list) && is_array($similarity_list) && sizeof($similarity_list) ) foreach( $similarity_list as $key2 => $value2 ){ $counter2++; ?>

				<?php if( $row_counter*$size <= $counter2 ){ ?>

					<?php if( $counter2 < ($row_counter + 1) * $size ){ ?>

						<td><?php echo $value2;?></td>
					<?php } ?>

				<?php } ?>

			<?php }else{ ?>

			...
		<?php } ?>

	</tr>
	<?php } ?>

</tbody>

</table>