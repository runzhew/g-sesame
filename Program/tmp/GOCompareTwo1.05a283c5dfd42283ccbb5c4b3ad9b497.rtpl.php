<?php if(!class_exists('raintpl')){exit;}?><ul style="margin-top: 50px;">
	<li>Enter GO term assession numbers, such as 0005739 and 0005777, in input fields.</li>
	<li>Assign semantic contribution factors (0.0 - 1.0) for "is-a" and "part-of" relationships respectively.</li>
	<li>Press "Submit" button and wait for the results.</li>
</ul>

<br>
<form class="form-horizontal" role="form" method="post" action="Program/GOCompareTwo2.php">
	<input type="text" class="hidden" name="form" value="Semantic Similarity of two GO terms">
	<input type="text" class="hidden" name="tool_id" value="1">
	<div class="form-group">
		<label for="term1" class="col-sm-2 control-label">GO Term 1: </label>
		<div class="col-sm-10">
			<input class="form-control" name="GOTerm1" type="text"></input>
		</div>
	</div>
	<div class="form-group">
		<label for="term2" class="col-sm-2 control-label">GO Term 2: </label>
		<div class="col-sm-10">
			<input class="form-control" name="GOTerm2" type="text"></input>
		</div>
	</div>
<?php $tpl = new RainTPL;$tpl_dir_temp = self::$tpl_dir;$tpl->assign( $this->var );$tpl->draw( dirname("isAPartOf") . ( substr("isAPartOf",-1,1) != "/" ? "/" : "" ) . basename("isAPartOf") );?>


<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input class="btn btn-default" name="submit" value="Submit" type="submit"> <input class="btn btn-default"name="reset" type="reset">  </p> 
</form>


<!-- 
<form name="form1" method="post" action="Program/GOCompareTwo2.php">
   
    <table>
<tr><td>GO Term 1:</td><td><input name="GOTerm1" type="text"> </td></tr>
<tr><td>GO Term 2:</td><td><input name="GOTerm2" type="text"></td></tr>
</table> -->

