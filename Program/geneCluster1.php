<?php include ("../XHTML/header.txt"); ?>


<h2>Gene Clustering</h2>
        
<ul>
<li>Upload a file containing gene names line by line. <b> Sample file: <a href = "../XHTML/gene.txt"> Click Here</a></b></li>
<li>Assign semantic contribution factors (0.0 - 1.0) for "is-a" and "part-of" relationships respectively.</li>
<li>Set the similarity threshold decremental interval (< 0.5) for the hierarchical clustering.</li>
<li>Select proper filters (ontologies, species, data sources, evidence codes).</li>
<li>Press "Upload and Calculate" button and wait for the results.</li>
</ul>


<form enctype="multipart/form-data" action="geneCluster2.php" method="POST">

    <p>File: <input name="uploadedFile" type="file" ></p>

    <?php include ("../XHTML/isAPartOf.txt"); ?>

<?php include ("../XHTML/interval.txt"); ?> 

<?php include ("../XHTML/ontology.txt"); ?> 


<!-- no mutiples for species can be select-->
<!-- only one species is allowed, as it is 1:many relation with gene ids -->
<!-- These ids are not species ids in the table, but the ncbi_taxa_id in the table -->
<p>Species</p>
<select name="species" multiple="yes" size="3">
    <option value="All" selected>Select one species</option>
    <?php include ("../XHTML/species.txt"); ?>
</select>


<p>Data sources</p>
<select name="dataSources[]" multiple="yes "size="3">
    <?php include ("../XHTML/dataSource.txt"); ?>
</select>


<p>Evidence codes </p>  
<select name="evidenceCodes[]"  multiple="yes" size="3">
    <?php include ("../XHTML/evidenceCode.txt"); ?>
</select>
                

<?php include ("../XHTML/email.txt"); ?>
                        

<input type="submit" value="Upload and Calculate" >
    
    <input type="reset" value="Reset" >
    
    </form>

<?php include ("../XHTML/footer.txt"); ?>

