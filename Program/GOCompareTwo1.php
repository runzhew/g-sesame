<?php include ("../XHTML/header.txt"); ?>

<h2>Semantic Similarity of GO Terms</h2>
   
<ul>
<li>Enter GO term assession numbers, such as 0005739 and 0005777, in input fields.</li>
<li>Assign semantic contribution factors (0.0 - 1.0) for "is-a" and "part-of" relationships respectively.</li>
<li>Press "Submit" button and wait for the results.</li>
</ul>

<form name="form1" method="post" action="GOCompareTwo2.php">
   
    <table>
<tr><td>GO Term 1:</td><td><input name="GOTerm1" type="text"> </td></tr>
<tr><td>GO Term 2:</td><td><input name="GOTerm2" type="text"></td></tr>
</table>

<?php include ("../XHTML/isAPartOf.txt"); ?>

<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<input name="submit" value="Submit" type="submit"> <input name="reset" type="reset">  </p> 
</form>



<?php include ("../XHTML/footer.txt"); ?>

