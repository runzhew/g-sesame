<?php include ("../XHTML/header.txt"); ?>



<h2>Search Top N Similar Genes</h2>

<ul>
   <li>Enter a gene symbol, such as FAA1, and select "S. cerevisiae" in sepcies field, and other proper filtering parameters.</li>
   <li>Assign semantic contribution factors (0.0 - 1.0) for "is_a" and "part_of" relationships respectively.</li>
   <li>Press "Submit" button and wait for the results. </li>
</ul>

<form name="formGeneTop" method="post" action="geneTop2.php">
       
<table>
 <tr><td>Given gene symbol: </td> <td><input name="gene" type="text"></td></tr>
 <tr><td>The number genes to be returned: </td> <td><input name="searchNumber" type="text" value="20" ></td></tr>
</table>
   

<?php include ("../XHTML/isAPartOf.txt"); ?>
<?php include ("../XHTML/ontology.txt"); ?> 


   
<table border="0">  
    
<tr>
<th>
Source gene filters
</th>

<th>
&nbsp; &nbsp;
</th>

<th>
Target gene filters
</th>
</tr>


<tr>
<td>

<!-- no mutiples for species can be select-->
<!-- only one species is allowed, as it is 1:many relation with gene ids -->
<!-- These ids are not species ids in the table, but the ncbi_taxa_id in the table -->
         <p>Species</p>          
<select name="species1" size="3">               
     <?php include ("../XHTML/species.txt"); ?>
</select>     

</td>

<!-- add one space -->
<td>
&nbsp;
</td>

<!-- only one species is allowed, as it is 1:many gene id relationship -->
<td>
<p>Species</p>          
<select name="species2" size="3">               
         <?php include ("../XHTML/species.txt"); ?>
</select>  
</td>
</tr>

<tr>
<td>
<p>Data sources</p>
<select name="dataSources1[]" multiple="yes "size="3">
    <?php include ("../XHTML/dataSource.txt"); ?>
</select>
</td>

<!-- add one space -->
<td>
&nbsp;
</td>

<td>
<p>Data sources</p>
<select name="dataSources2[]" multiple="yes "size="3">
    <?php include ("../XHTML/dataSource.txt"); ?>
</select>
</td>
</tr>


<tr>
<td>
<p>Evidence codes </p>  
<select name="evidenceCodes1[]"  multiple="yes" size="3">
    <?php include ("../XHTML/evidenceCode.txt"); ?>
</select>
</td>

<!-- add one space -->
<td>
&nbsp;
</td>

<td>              
<p>Evidence codes </p>  
<select name="evidenceCodes2[]"  multiple="yes" size="3">
    <?php include ("../XHTML/evidenceCode.txt"); ?>
</select>
</td>
</tr>    
</table>


<p>
           <input type="submit" name="Submit" value="Submit">
           <input type="reset" name="reset">
</p>
</form>

<?php include ("../XHTML/footer.txt"); ?>

