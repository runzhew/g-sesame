<?php include ("../XHTML/header.txt"); ?>


<h2>Functional similarity of two genes</h2>

<ul>
<li>Enter gene names, such as FAA1 and FAA2, in input fields.</li>
<li>Assign semantic contribution factors (0.0 - 1.0) for "is_a" and "part_of" relationships respectively.</li>
<li>Press "Submit" button and wait for the results.</li>
</ul>


<form name="geneCompareTwo" method="post" action="geneCompareTwo2.php">
    
<table>
<tr>
<td>Gene1: </td> <td><input name="symbol1" type="text"></td>
<td>Gene2: </td> <td><input name="symbol2" type="text"></td>
</tr>
</table>



<?php include( "../XHTML/isAPartOf.txt" ); ?>
<?php include ("../XHTML/ontology.txt"); ?> 


   
<table border="0">  
    
 <tr>
 <th>
Source gene filters
</th>

<th>
&nbsp; &nbsp;
</th>

<th>
Target gene filters
</th>
</tr>


<tr>
<td>

<!-- no mutiples for species can be select-->
<!-- only one species is allowed, as it is 1:many gene id relationship -->
<!-- These ids are not species ids in the table, but the ncbi_taxa_id in the table-->
         <p>Species</p>          
<select name="species1" size="3">  
<option value="All" selected>All</option>             
     <?php include ("../XHTML/species.txt"); ?>
</select>     

</td>

<!-- add one space -->
<td>
&nbsp;
</td>

<!-- only one species is allowed, as it is 1:many gene id relationship -->
<td>
<p>Species</p>          
<select name="species2" size="3">               
<option value="All" selected>All</option>         
<?php include ("../XHTML/species.txt"); ?>
</select>  
</td>
</tr>

<tr>
<td>
<p>Data sources</p>
<select name="dataSources1[]" multiple="yes "size="3">
    <?php include ("../XHTML/dataSource.txt"); ?>
</select>
</td>

<!-- add one space -->
<td>
&nbsp;
</td>

<td>
<p>Data sources</p>
<select name="dataSources2[]" multiple="yes "size="3">
    <?php include ("../XHTML/dataSource.txt"); ?>
</select>
</td>
</tr>


<tr>
<td>
<p>Evidence codes </p>  
<select name="evidenceCodes1[]"  multiple="yes" size="3">
    <?php include ("../XHTML/evidenceCode.txt"); ?>
</select>
</td>

<!-- add one space -->
<td>
&nbsp;
</td>

<td>              
<p>Evidence codes </p>  
<select name="evidenceCodes2[]"  multiple="yes" size="3">
    <?php include ("../XHTML/evidenceCode.txt"); ?>
</select>
</td>
</tr>    
</table>

<p>
<input type="submit" name="submit" value="Submit">
         <input type="reset" name="reset">
         </p>

</form>


<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>

<?php include ("../XHTML/footer.txt"); ?>

