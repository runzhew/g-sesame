//package evaluation;



import java.io.*;
import java.util.*;
import java.util.concurrent.*;


public class MyEvaluation {
	private static int NUM_OF_TASKS = 20;
	Object result;
	int cnt = 0;
	long begTest, endTest;
	public static Map<Integer, Double> weightMap = new HashMap<Integer, Double>();
	//array for GO term set 1
	String[] term1 = new String[100];
	//array for GO term set 2
	String[] term2 = new String[120];
	
	public void callBack(Object result) {
		System.out.println("result " + result);
		this.result = result;
		if (++cnt == 20) {
			Double secs = new Double((new java.util.Date().getTime() - begTest)*0.001);
			System.out.println("run time " + secs + " secs");
			System.exit(0);
		}
	}
	
	public void run() {
		int nrOfProcessors = Runtime.getRuntime().availableProcessors();
		System.out.println("number of processors " + nrOfProcessors);
		 ExecutorService es = Executors.newFixedThreadPool(nrOfProcessors);
	     // ExecutorService es = Executors.newFixedThreadPool(50);
	      for(int i = 0;  i < NUM_OF_TASKS; i++) {
	         ComputeTask task = new ComputeTask(5*i, i, term1, term2);
	         task.setCaller(this);
	         es.submit(task);
	    	  
	         // at this point after submitting the tasks the
	         // main thread is free to perform other work.
	      }
	}
	
	public static void main(String[] args) {
		Evaluation object = new Evaluation();
		
		
		//popularize 2 GO term arrays
		try {
	         FileInputStream fstream = new FileInputStream("./input1.csv");
	         DataInputStream in = new DataInputStream(fstream);
	         BufferedReader br = new BufferedReader(new InputStreamReader(in));
	         String strLine;
             int i = 0;
	         while ((strLine = br.readLine()) != null) {
	        	 object.term1[i] = strLine.substring(1, 11);
	        	 i++;
	         }

	         in.close();
	      }catch (Exception e) {
	         System.err.println("Error: " + e.getMessage());
	      }
	      
	      try {
		         FileInputStream fstream = new FileInputStream("./input2.csv");
		         DataInputStream in = new DataInputStream(fstream);
		         BufferedReader br = new BufferedReader(new InputStreamReader(in));
		         String strLine;
	             int i = 0;
		         while ((strLine = br.readLine()) != null) {
		        	 object.term2[i] = strLine.substring(1, 11);
		        	 i++;
		         }

		         in.close();
		      }catch (Exception e) {
		         System.err.println("Error: " + e.getMessage());
		      }
		      
		      /*for (int i = 0; i < 100; i++)
		    	  System.out.println(object.term1[i]);
		      
		       for (int j = 0; j < 120; j++)
		    	  System.out.println(object.term2[j]);
		      */
		      
		    //fill the weightmap
		      try {
			         FileInputStream fstream = new FileInputStream("./correctoutput5");
			         DataInputStream in = new DataInputStream(fstream);
			         BufferedReader br = new BufferedReader(new InputStreamReader(in));
			         String strLine;

			         while ((strLine = br.readLine()) != null) {
			             String[] temp = strLine.split(" ");
			             //System.out.println(temp[1]+ " " + temp[3]);
			             weightMap.put(Integer.valueOf(temp[1]), Double.valueOf(temp[3]));
			         }

			         in.close();
			      }catch (Exception e) {
			         System.err.println("Error: " + e.getMessage());
			      }
			      
			      object.begTest = new java.util.Date().getTime();
	              object.run();
		    	  
	}

}
