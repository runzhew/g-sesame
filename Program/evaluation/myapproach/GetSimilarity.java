//package evaluation;

import java.sql.Connection;
import java.util.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.io.*;


class Queue {
	private final int SIZE = 100;
	private int[] queArray;
	private int front;
	private int rear;
	
	public Queue() {
		queArray = new int[SIZE];
		front = 0;
		rear = -1;
	}
	
	public void insert(int j) //put item at rear of queue
	{
		if (rear == SIZE - 1)
			rear = -1; //queue is full
		queArray[++rear] = j;
	}
	
	public int remove() //take item from front of queue
	{
		int temp = queArray[front++];
		if (front == SIZE)
			front = 0;
		return temp;
	}
	
	public boolean isEmpty() {
		return (rear + 1 == front) || (front + SIZE - 1 == rear);
	}
} //end class Queue



class Vertex2 {
	public int id;
	public String name;
	public String term_type;
	public String acc;
	public boolean wasVisited;
	public boolean bfsVisited;
	public int level;
	public double weight;

	public Vertex2(int vid) {
		id = vid;
		bfsVisited = false;
		weight = -1;
	}
	
}


class Graph2 {
	private final int MAX_VERTS = 1000;
	public Vertex2[] vertexList;
	public int[][] adjMat;
	private int nVerts;
	private Queue theQueue;
	public ArrayList<Integer> terms = new ArrayList<Integer>();
	public HashMap<Integer, Integer> indexValue = new HashMap<Integer, Integer>();
	int[] parentterm;
	
	public Graph2() {
		vertexList = new Vertex2[MAX_VERTS];
		adjMat = new int[MAX_VERTS][MAX_VERTS];
		nVerts = 0;

		for (int j = 0; j < MAX_VERTS; j++)
			for (int k = 0; k < MAX_VERTS; k++) {
				adjMat[j][k] = 0;
			}
		theQueue = new Queue();
	}//end constructor
	
	

	public void bfs(int sourceid) {
		parentterm = new int[nVerts];
		for (int i = 0; i < nVerts; i++) {
			parentterm[i] = -1;
		}
		
		
		int index = indexValue.get(sourceid);
		//vertexList[0].bfsVisited = true;
		//System.out.println("index is " + index);
		vertexList[index].bfsVisited = true;
		theQueue.insert(index);
		int v2;
		while (!theQueue.isEmpty()) {
			int v1 = theQueue.remove();
			while ((v2 = getAdjUnvisitedVertex(v1)) != -1) {
				vertexList[v2].bfsVisited = true;
				parentterm[v2] = v1;
				theQueue.insert(v2);
			}//end while
		}//end while(queue not empty)
		
		//queue is empty, so we are done
		for (int j = 0; j < nVerts; j++)
			vertexList[j].bfsVisited = false;
		
	} //end bfs();
	
	public void print_path(int sourceid, int targetid) {
		//int indexofsource = indexValue.get(sourceid);
		int indexoftarget = indexValue.get(targetid);
		if (sourceid == targetid)
			terms.add(targetid);
		else if (parentterm[indexoftarget] == -1) 
			System.out.println("No path");
		else {
		    terms.add(targetid);
			print_path(sourceid, vertexList[parentterm[indexoftarget]].id);
		}
	}
	
	//return an unvisited vertex adjacent to v
	public int getAdjUnvisitedVertex(int v) {
		for (int j = 0; j < nVerts; j++)
			if (adjMat[v][j] == 1 && vertexList[j].bfsVisited == false)
				return j;
		return -1;
	}
	
	

	public void addVertex(int vid) {
		

		if (vid == 34455)
			return;
		//only cares about the id
		vertexList[nVerts++] = new Vertex2(vid);
		//System.out.println(nVerts - 1 + " " + vertexList[nVerts - 1].id);
		
	}

	//get the number of vertexs
	public int getTotal() {
		return nVerts;	
	}


	public void addEdge(int start, int end) {

		adjMat[start][end] = 1;

	}
	
	public void printVertex(){
		for (int i = 0; i < nVerts; i++) {

			System.out.println(i + " " + vertexList[i].id );
		}
	}
	

}


public class GetSimilarity {
	int parentid, childid, typeid;
	
	ArrayList<Integer> parent = new ArrayList<Integer>();
	ArrayList<Integer> child = new ArrayList<Integer>();
	//ArrayList<Integer> type = new ArrayList<Integer>();
	Vertex2 commonVertex; //the common vertex with the largest weight 
	
	
	public int accToID(String acc) {
		String dbUrl = "jdbc:mysql://phoenix.cs.clemson.edu/mygo";
		String dbClass = "com.mysql.jdbc.Driver";
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		int id = 0;
                int count = 0;
                boolean isobsolete = false;
		String query5 = "SELECT * FROM term WHERE acc = '" + acc + "'";
		try{
			Class.forName(dbClass);

			con = DriverManager.getConnection(dbUrl, "ll", "$clemson$mmlab@");

			stmt = con.createStatement();

			rs = stmt.executeQuery(query5);	
                        
                         
			while (rs.next()) {
				id = rs.getInt(1);
                                if (rs.getInt(5) == 1) {
                                  //this term is obsolete
                                  isobsolete = true;
                                }
                                count++;
			}
			
			rs.close();
			stmt.close();
			con.close();
			con = null;

		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
       if ((count == 0) || (isobsolete == true))
                      id = -1;
                
		return id;
	}
	
	//return the term's id
    public  int findCommon(Graph2 theGraph1, Graph2 theGraph2) {
    	//int vertexTotal1 = theGraph1.getTotal();
    	//int vertexTotal2 = theGraph2.getTotal();
    	double weight = 1;
    	int tempid = 0;
    	double tempweight;
    	HashSet<Integer> graph1 = new HashSet<Integer>();
    	HashSet<Integer> graph2 = new HashSet<Integer>();
    	
  
    	for (int i = 0; i < theGraph1.getTotal(); i++)
    		graph1.add(theGraph1.vertexList[i].id);
    	
    	for (int i = 0; i < theGraph2.getTotal(); i++)
    		graph2.add(theGraph2.vertexList[i].id);
    	graph1.retainAll(graph2);
    	
        //System.out.println("common element number is " + graph1.size());
        if (graph1.size() == 1) {
             tempid = 6602;
        }else {
    	for (int i = 0; i < graph1.size(); i++) {
    		tempweight = Evaluation.weightMap.get(graph1.toArray()[i]);
    		if (tempweight < weight) {
    			weight = tempweight;
    			tempid = (Integer)graph1.toArray()[i];
    		}
    	}
        }	
    	
    	return tempid;
       
    }
	
	//find the DAG of each term
	public void fillGraph(Graph2 obj, int termid) {
		String dbUrl = "jdbc:mysql://phoenix.cs.clemson.edu/mygo";
		String dbClass = "com.mysql.jdbc.Driver";
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		parent.clear();
		child.clear();
		
		int count = 0;
		
		
		String query1 = "SELECT DISTINCT r.term1_id, r.term2_id FROM " +
		"graph_path INNER JOIN term AS t ON (t.id = graph_path.term2_id) " +
		"INNER JOIN term AS p ON (p.id = graph_path.term1_id) INNER JOIN " +
		"term2term AS r on (r.term2_id = p.id) WHERE t.id = " + termid;
		
		try{
			Class.forName(dbClass);

			con = DriverManager.getConnection(dbUrl, "ll", "$clemson$mmlab@");

			stmt = con.createStatement();

			rs = stmt.executeQuery(query1);	
		

			while (rs.next()) {
				parentid = rs.getInt(1);
				childid = rs.getInt(2);
				parent.add(parentid);
				child.add(childid);
				count++;
			}
			
			Set<Integer> union = new HashSet<Integer>(parent); 
			union.addAll(child);
			for (int i = 0; i < union.size(); i++)
				obj.addVertex((Integer)union.toArray()[i]);
			
			rs.close();
			stmt.close();
			con.close();
			con = null;

		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		for (int i = 0; i < obj.getTotal(); i++) {
			//System.out.println(obj.vertexList[i].id + " " + i);
			obj.indexValue.put(obj.vertexList[i].id, i); //ID, index in vertexlist
		}
		//add edges
		for (int i = 0; i < count; i++) {

			if (parent.get(i) == 34455) {
				//remove the edge connecting "ALL"
			}else {
				//obj.addEdge(obj.getID(child.get(i)), obj.getID(parent.get(i)));
				obj.addEdge(obj.indexValue.get(child.get(i)), obj.indexValue.get(parent.get(i)));
			}
		}
		
	}
	
	
	//new a graph object for term 1
	//public static double main(String[] args) {
	public double getsimilarity(String[] args) {
	
	Graph2 theGraph1 = new Graph2();
	Graph2 theGraph2 = new Graph2();
	//GetSimilarity test = new GetSimilarity();
	int termid1 = 0;
	int termid2 = 0; 
    int commonid = 0;
	double semDistance = 0;
        double similarity = 0;    

	String input1 = args[0];
	String input2 = args[1];
	
	termid1 = accToID(input1);
	termid2 = accToID(input2);
	//System.out.println("termid " + termid1 + " " + termid2);
	//System.out.println("termid 1, 2 is " + termid1 + " " + termid2);
        if ((termid1 == -1) || (termid2 == -1)) {
                similarity = -1;
                return similarity;
         } else {
	
  
    fillGraph(theGraph1, termid1);
    fillGraph(theGraph2, termid2);

	commonid = findCommon(theGraph1, theGraph2); //the one with the smallest weight
	//System.out.println("common id is " + commonid);
	theGraph2.bfs(termid2); //find the shortest path between termid2 and the common term
	theGraph2.print_path(termid2, commonid);
	theGraph1.bfs(termid1);
	theGraph1.print_path(termid1, commonid);
	
	for (int i = 0; i < theGraph2.terms.size(); i++) {
		//System.out.println(theGraph2.terms.get(i));
		semDistance = semDistance + Evaluation.weightMap.get(theGraph2.terms.get(i));
	}
	
	for (int j = 0; j < theGraph1.terms.size(); j++) {
		//System.out.println(theGraph1.terms.get(j));
		//only add commonid one time
		if (theGraph1.terms.get(j) != commonid) {
			semDistance = semDistance + Evaluation.weightMap.get(theGraph1.terms.get(j));
		}
	}
	//System.out.println("semantic distance is " + semDistance);
	
	double value = Math.atan(semDistance)/(Math.PI/2); //apply the transfer function
	
	DecimalFormat fourDForm = new DecimalFormat("#.####");
	
	//System.out.println("value is " + Double.valueOf(fourDForm.format(value)));
	
	similarity = Double.valueOf(fourDForm.format(1 - value));
	System.out.println("similarity is " + similarity);
	/*try {
		stmt.close();
	con.close();
	}catch(SQLException e) {
		System.err.println("close error.");
	}
	*/
        return similarity;
        }
	//Date date2 = new Date();
	//long after = date2.getTime();
	
	//System.out.println("after is " + after);
	//long duration = after - before;
	//System.out.println("duration is " + duration + "ms");
	
	}
//}
}

