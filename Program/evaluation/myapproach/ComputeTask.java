//package evaluation;

import java.util.concurrent.Callable;
import java.util.*;
import java.io.*;

public class ComputeTask implements Callable {
	private Evaluation oneobject;
	private int start;
	private int seq;
	private String[] local1, local2;
	//private ArrayList<String> dag1, dag2;
	public ComputeTask() {}
	public ComputeTask(int i, int index, String[] term1, String[] term2) 
	{
		start = i;
		seq = index;
        local1 = term1;
        local2 = term2;
	}
	
	public static double semanticsimilarity(ArrayList<String> dag1, ArrayList<String> dag2, GetSimilarity test) {
		//int length1 = 0, length2 = 0;
		double temp = 0, relation = 0;
		String[] para = new String[2];
		for (int i = 0; i < dag1.size(); i++) {
			for (int j = 0; j < dag2.size(); j++) {
				para[0] = dag1.get(i);
				
				para[1] = dag2.get(j);
				//System.out.println(para[0] + " " + para[1]);
				relation = test.getsimilarity(para);
				//System.out.println("relation is " + relation);
				if (relation > temp) {
					temp = relation;
				}
			}
		}
		
		return temp; //the maximum GO term similarity
		
	}
	
	
	public Object call() {
		int[] count = new int[1000]; //for both expression and semantic similarity
		double[] distribution = new double[1000]; //for both expression and semantic similarity
		//double[] average = new double[1000]; //expression similarity
		double[] semantic = new double[1000]; //semantic similarity
		double cor, tempcor;
        int index;
        
		int totalcount = 0;
         
		for (int i = 0; i <  1000; i++) {
			count[i] = 0;
			distribution[i] = 0;
			semantic[i] = 0;
		}
	      GetSimilarity test = new GetSimilarity();
	      
	      int end = 0;
	      
	      if (start < 95 ) {
	           end = start + 5;
	      }else if (start == 95) {
	    	  end = 100;
	      }
   
	      String[] para = new String[2];
	      
          for (int i = start; i <  end; i++) {
 			for (int j = 0; j < 120; j++) {
	        // int i = 32;
	         //int j = 2300;
             System.out.println(local1[i] + " " + local2[j] + " " + i + " " + j);
			//cor = semanticsimilarity(dagarray[i].dag, dagarray[j].dag, test);
			//System.out.println("semantic similarity: " + cor);
             para[0] = local1[i];
             para[1] = local2[j];
             cor = test.getsimilarity(para);
             
			tempcor = cor * 1000;
			if (tempcor == 1000) {
				index = 999;
			}else {
				index = (int)Math.floor(tempcor);
			}		
			distribution[index] = distribution[index] + cor;
			count[index]++;
			totalcount++;
		}
	}
           
           //write to files
           try {
               //String distfile = "I:\\workspace\\Gosimilarity\\src\\gosimilaritydistribution\\" + args[0];
        	   String distfile = "../output/distribution" + start;
               //String countfile = "count" + args[0];
               //FileWriter fstream = new FileWriter("semantic");
               //BufferedWriter out = new BufferedWriter(fstream);
               FileWriter fstream = new FileWriter(distfile);
               BufferedWriter out = new BufferedWriter(fstream);
               for (int i = 0; i <  1000; i++) {
                   //out.write(Double.toString(semantic[i]) + "\n");
                    out.write(Double.toString(distribution[i]) + "\n");
               } 
               out.close();
           }catch (Exception e) {
               System.err.println("Error: " + e.getMessage());
           }

           try {
               //String distfile = "distribution" + args[0];
              // String countfile = "./count" + args[0];
        	   String countfile = "../output/count" + start;
               //FileWriter fstream = new FileWriter("semantic");
               //BufferedWriter out = new BufferedWriter(fstream);
               FileWriter fstream = new FileWriter(countfile);
               BufferedWriter out = new BufferedWriter(fstream);
               for (int i = 0; i <  1000; i++) {
                   //out.write(Double.toString(semantic[i]) + "\n");
                    out.write(Double.toString(count[i]) + "\n");
               }
               //out.write("totolcount is " + totalcount + "\n");
               out.close();
           }catch (Exception e) {
               System.err.println("Error: " + e.getMessage());
           }
          
           oneobject.callBack(seq);
           return null;	
	}
	
	public void setCaller(Evaluation oneobject) {
		this.oneobject = oneobject;
	}

	public Evaluation getCaller() {
		return oneobject;
	}
}

