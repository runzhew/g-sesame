<?php
include_once (dirname(__FILE__) . "/preProcess.php"); 
include_once (dirname(__FILE__) . "/email.php"); 
include_once (dirname(__FILE__) . "/GOCompareTwo.php"); 
//include_once ("Display/GOCompareMultiple.php"); 
include_once (dirname(__FILE__) . "/time.php"); 



//This function is only called locally
//This function can be used to compare two sets of GO Ids for two genes comparison

//      term2 term2 term2 term2 term2 term2 term2 term2
//term1
//term1
//term1
//term1
//term1


// $similarities1{......} size of it is number of rows

// $similarities2{...........................}

/*
 This function is to find the maximum matched
*/
function getSimilarityTableMax ($size1, $size2, $GOTermComparisonTable) {


  //find the similarity 1

  $similarities1 = array();

  for ($index1 = 0; $index1 < $size1; $index1++){

    $similarityMax = 0;

    for ($index2 = 0; $index2 < $size2; $index2++){
  
      $similarity = $GOTermComparisonTable[$index1][$index2];

      //save the maxium value
      if  ($similarity > $similarityMax){
        $similarityMax =  $similarity;
      }

    }//for

    $similarities1[$index1] = $similarityMax; 

  }//for




  //find the similarity 2

  $similarities2 = array();

  for ($index2 = 0; $index2 < $size2; $index2++){

    $similarityMax = 0;

    for ($index1 = 0; $index1 < $size1; $index1++){
  
      $similarity = $GOTermComparisonTable[$index1][$index2];

      //save the maxium value
      if  ($similarity > $similarityMax){

        $similarityMax =  $similarity;
      }

    }//for

    $similarities2[$index2] = $similarityMax; 
    
  }//for


  //do calculation
  $sum1 = array_sum ($similarities1);
  $sum2 = array_sum ($similarities2);

  //  print_r ($similarities1);
  //print_r ($similarities2);

  $similarityResult =  ($sum1 + $sum2)/ ($size1 + $size2);


  return $similarityResult;
}


/*
 Precondition, size1 and size2 > 0
 All GO term ids are valid in the database

 Output:
 $similarityTable, which is not symmetric

 Return maximum matched
*/


function compareGOMultiple ($ids1, $ids2, $isA, $partOf) {


  $size1 = sizeof ($ids1);
  $size2 = sizeof ($ids2);
  
 
  //term in term1 with all ids in ids
  //find the similarity of term in term1 with all ids in term2

  //$similaritiesMaxRow = array();
  $similarityvalue = array();
  $similaritycount = array();
  for ($i = 0; $i < 1000; $i++) {
         $similarityvalue[$i] = 0;
         $similaritycount[$i] = 0;
  }
   
  foreach ($ids1 as $term1) {
    
    //$similarityMax = 0;

    foreach ($ids2 as $term2) {

      $similarity = compareTwoGOIds ($term1, $term2, $isA, $partOf);
      echo "term1 " . $term1 . " term2 " . $term2 . " " . $similarity . "\n";
      if ($similarity * 1000 == 1000) {
          $tempindex = 999;
      } else {
          $tempindex = floor($similarity * 1000);
      }
      
      $similarityvalue[$tempindex] = $similarityvalue[$tempindex] + $similarity;
      $similaritycount[$tempindex]++;
     
    }
   }

   //print out for testing
   for ($i = 0; $i < 1000; $i++)
  {
     echo $i . " " . $similaritycount[$i] . " " . $similarityvalue[$i] . "\n";
  }
  
   $myfile = "wangsimilarity";
   $fh = fopen($myfile, 'w');

  for ($j = 0; $j < 1000; $j++){
     if ($similaritycount[$j] == 0) {
        $average = $similarityvalue[$j];
      }else {
        $average = $similarityvalue[$j] / $similaritycount[$j];
       }
     fwrite($fh, $average . "\n");
  }
  fclose($fh);
}




/*


*/

function compareGOMultipleEmail ($GOTerms1, $GOTerms2, $isA, $partOf, $emailAddress, $description) {

  $size1 = sizeof ($GOTerms1);
  $size2 = sizeof ($GOTerms2);

  //2D
  $similarityTable = array();

  for ($index1 = 0; $index1 < $size1; $index1++) {

    //initialize $similarities array
    $similarities = array();

    for ($index2 = 0; $index2 < $size2; $index2++) {

      $term1 = $GOTerms1[$index1];
      $term2 = $GOTerms2[$index2];


      $id1 = getTermId ($term1);
      $id2 = getTermId ($term2);

      $similarity = compareTwoGOIds ($id1, $id2, $isA, $partOf);


      $similarityTable[$index1][$index2] = $similarity;
      
    }

  }


  $similarity = getSimilarityTableMax ($size1, $size2, $similarityTable);

  //output
  //return a string with space
  //$currentStamp = microtime (true);
  $currentStamp = microtime_float();


  $fileNameResult = $_SERVER['DOCUMENT_ROOT'] . "/Temp/" . $currentStamp . ".txt";   
  
  $fileHandleResult = fopen ($fileNameResult, "a");

  if (!$fileHandleResult) { 
    echo "Cannot open file $fileNameResult."; 
  }
 
  
  //will write to file also
  displayGoMultipleSimilarities ($fileHandleResult, $GOTerms1, $GOTerms2, $similarityTable);

  fclose ($fileHandleResult);  

  
  //send email or display on web by open the result file again

  $fileHandleResult = fopen ($fileNameResult, "r");

  
  if  (!$fileHandleResult) { 
    echo "Cannot open file $fileName."; 
  }

  $contentResult = fread ($fileHandleResult, filesize ($fileNameResult));


  if ($emailAddress == "no"){
    echo $contentResult;
  }
  else{//send email


    //add head
    $fileNameHead = "../XHTML/header.txt";

    $fileHandleHead = fopen ($fileNameHead, "r");

    if  (!$fileHandleHead) { 
      echo "Cannot open file file head."; 
    }

    $contentHead = fread ($fileHandleHead, filesize ($fileHandleHead));
 

    echo $contentHead;
    //return;

    //add foot
    $fileNameFoot = "../XHTML/footer.txt";

    $fileHandleFoot = fopen ($fileNameFoot, "r");

    if  (!$fileHandleFoot) { 
      echo "Cannot open file file foot."; 
    }

    $contentFoot = fread ($fileHandleFoot, filesize ($fileHandleFoot));
 

    //merge together
    $fileNameAll = "./Temp/". $currentStamp . "Result.html";   

    $fileHandleAll = fopen ($fileNameAll, "a");

    if (!$fileHandleAll) { 
      echo "Cannot open file file All."; 
    }

    fwrite ($fileHandleAll, $contentHead);
    fwrite ($fileHandleAll, $contentResult);
    fwrite ($fileHandleAll, $contentFoot);
 
    fclose ($fileHandleAll);


    $message = sendEmail ($emailAddress, $description, $fileNameAll);

    echo $message;

  }


  // echo "<b>Email" . $emailAddress . $description;

}//end of function 






/*
 Precondition, size1 and size2 > 0
 All GO term ids are valid in the database

 Output:
 $similarityTable, which is not symmetric

 Return maximum matched
*/


function compareGOMultipleStatistics ($ids1, $ids2,
                                      $termFrequencies, $produceCountAll, $method, 
                                      &$result, &$similarities) {
  

  $size1 = sizeof ($ids1);
  $size2 = sizeof ($ids2);
  
 
  //term in term1 with all ids in ids
  //find the similarity of term in term1 with all ids in term2

  $similaritiesMaxRow = array();

  foreach ($ids1 as $term1) {
    
    $similarityMax = 0;

    foreach ($ids2 as $term2) {
      
      if ($method == "Jiang") {
	$similarity = compareTwoGOIdsJiang ($term1, $term2, $termFrequencies, $produceCountAll);
      }
      else {
	if ($method == "Lin") {
	  $similarity = compareTwoGOIdsLin ($term1, $term2, $termFrequencies, $produceCountAll);
	}
	else {
	  $similarity = compareTwoGOIdsResnik ($term1, $term2, $termFrequencies, $produceCountAll);
	}
      }
      
      //save the maximum value
      if ($similarity > $similarityMax) {

        $similaritiesMaxRow[$term1] = $similarity;
        $similarityMax =  $similarity;
      }

      //generate the table for display by geneComparetwo
      $termComparisonTable[$term1][$term2] = $similarity;

    }
  }



  //find the similarity of term in term2 with all ids in term1
  $similaritiesMaxColumn = array();

  foreach ($ids2 as $term2) {

    $similarityMax = 0;
    foreach ($ids1 as $term1) {
  
      $similarityTemp = $termComparisonTable[$term1][$term2];

      //save the maxium value
      if ($similarityTemp > $similarityMax) {
        $similaritiesMaxColumn[$term2] = $similarity;
        $similarityMax = $similarityTemp;
      }

    }//for
  }//for


  //do calculation
  $sum1 = array_sum ($similaritiesMaxRow);
  $sum2 = array_sum ($similaritiesMaxColumn);
  
  //  print_r($similarity);
  $result = ($sum1 + $sum2) / ($size1 + $size2);

  return 0;
}







?>
