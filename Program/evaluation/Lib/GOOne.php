<?php
// include_once ("database.php"); 

// //include_once ("Display/display.php"); 

// //include_once ("log.php"); 

// //Name:
// //Project: Gene Ontology Comparison
// //Date: 8/11/07
// //This file contain all of the functions related with GO terms



// //Prepare the string for searching in database
// //the parameter is 7 digits number 
// //the function will add "GO:" to the front  
// // This to match with the mygo database on our server
// function prepareGOTerm ($term) {
//   $term = "GO:" . $term;
//   return $term;
// }//end of PrepareGOTerm


// /*
// getTermId from ACC, we assume it is 1:1
// Input: ACC, the formate is GO:0123456, not 0123456
// Output: 
// */

// function getTermId ($term) {
 
//   // writing the query to select the term
//   $result = mysql_query ("SELECT id FROM term WHERE acc = '$term';");
 
//   if (!$result) {
//     echo "<p>Error in getTermID: " . mysql_error() ."</p>";
//     exit ();
//   }


//   if (mysql_num_rows ($result) == 0 ){
//     echo "\n<p>No result found from GO term $term.</p>";
//     return 0;
//   }
  

//   $row = mysql_fetch_array ($result);
//   return $row["id"];
  
// }//end of function




// //Get acc, i.e. GO:0123456
// function getTermAcc ($id) {

//   // writing the query to select the term
//   $result = mysql_query ("SELECT acc FROM term WHERE id = '$id';");

//   if (!$result) {
//     echo "<p>Error performing query: " . mysql_error () ."</p>";
//     exit();
//   }

//   $answer = "";
//   //printout the data to a string
//   while ($row = mysql_fetch_array($result)) {
//     $answer = $row["acc"] . $answer;
//   }

//   return $answer;
// }//end of function



// /*
// //Get the name of the term
// // select the Term table and locate the matching value
// // the function will return the name of the term 
// // Prameter = term Accession 	

// Input:3926
// Output:mitochondrion

// */
// function getTermName ($id) {
//   // writing the query to select the term
//   $result = mysql_query ("SELECT name FROM term WHERE id = '$id'; ");
//   if (!$result) {
//     echo "\n<p>Error performing query: " . mysql_error() ."</p>";
//     exit();
//   }

//   $answer = "";
//   //print out the data to a string
//   while ($row = mysql_fetch_array($result)) {
//     $answer = $row["name"] . $answer;
//   }

//   return $answer;
// }//end of function


// //get definition from table term_definition
// function getTermDefinition ($id) {
//   // writing the query to select the term
//   $result = mysql_query ("SELECT term_definition FROM term_definition WHERE term_id ='$id';");

//   if (!$result) {
//     echo "\n<p>Error performing query: " . mysql_error() . "</p>";
//     exit();
//   }

//   $answer ="";
//   //print the data to the string
//   while ($row = mysql_fetch_array($result)) {
//     $answer = $row["term_definition"] . $answer;
//   }

//   if ($answer == "") {
//     $answer = "none";
//   }

//   return $answer;
// }//end of function




// //get synonym term_Synonym
// function getTermSynonym ($id) {
//   // writing the query to select the term
//   $result = mysql_query ("SELECT term_synonym FROM term_synonym WHERE term_id = '$id';");
//   if (!$result) {
//     echo "\n<p>Error performing query when get TermSynonym : " . mysql_error() . "</p>";
//     exit();
//   }

//   $answer = "";
//   //print the data to the string
//   while ($row = mysql_fetch_array($result)) {
//     $answer = $row[ "term_synonym" ] . $answer;
//   }

//   if ($answer == "") {
//     $answer = "none";
//   }

//   return $answer;
// }//end of function



// //input termid
// //output children ids
// //called by geneTop
// function getChildren ($term, &$children) {

//   $sqlString = "SELECT DISTINCT child.id ";
//   $sqlString .= "FROM term AS parent, ";
//   $sqlString .= "term2term, ";
//   $sqlString .= "term AS child ";
//   $sqlString .= "WHERE ";
//   $sqlString .= "parent.id = $term AND ";
//   $sqlString .= "parent.id = term2term.term1_id AND ";
//   $sqlString .= "child.id  = term2term.term2_id;";

//   $result = mysql_query ($sqlString);

//   while (list($id) = mysql_fetch_array ($result)) {	
//     $children[] = $id;     
//   }
// }


// //input termid
// //output children ids
// //called by geneTop

// function getParents ($term, &$parents) {

//   if ($term == 1) {
//     echo "root";
//     return;
//   }
//   $sqlString = "SELECT DISTINCT parent.id ";
//   $sqlString .= "FROM term AS parent, ";
//   $sqlString .= "term2term, ";
//   $sqlString .= "term AS child ";
//   $sqlString .= "WHERE ";
//   $sqlString .= "child.id = $term AND ";
//   $sqlString .= "parent.id = term2term.term1_id AND ";
//   $sqlString .= "child.id  = term2term.term2_id;";

//   $result = mysql_query ($sqlString);

//   while (list($id) = mysql_fetch_array ($result)) {
//     //reading one by one and put in the table
//     $children[] = $id;     
//   }
// }



// //input term id
// //output parents ids
// //called by random walk

// function getAllParents ($term) {

//   $parents = array();

//   if ($term == 1) {
//     echo "root";
//     $parents[] = $term;
//     return $parents;
//   }

//   $sqlString = "SELECT DISTINCT parent.id ";
//   $sqlString .= "FROM term AS parent, ";
//   $sqlString .= "term2term, ";
//   $sqlString .= "term AS child ";
//   $sqlString .= "WHERE ";
//   $sqlString .= "child.id = $term AND ";
//   $sqlString .= "parent.id = term2term.term1_id AND ";
//   $sqlString .= "child.id  = term2term.term2_id;";

//   $result = mysql_query ($sqlString);

//   while (list($id) = mysql_fetch_array ($result)) {
//     //reading one by one and put in the table
//     $parents[] = $id;     
//   }
//   return $parents;
// }





// mysql> SELECT DISTINCT r.term1_id, r.term2_id, r.relationship_type_id FROM graph_path INNER JOIN term AS t ON (t.id = graph_path.term2_id) INNER JOIN term AS p ON (p.id = graph_path.term1_id) INNER JOIN term2term AS r ON (r.term2_id = p.id) WHERE t.id = 3728;
// +----------+----------+----------------------+
// | term1_id | term2_id | relationship_type_id |
// +----------+----------+----------------------+
// |    16307 |     3728 |                    2 |
// |    16854 |     3728 |                    2 |
// |    16834 |    16854 |                    2 |
// |     3727 |    16854 |                    8 |
// |    16834 |     3727 |                    2 |
// |    16873 |    16834 |                    2 |
// |     3618 |    16834 |                    8 |
// |     3571 |    16873 |                    2 |
// |     3619 |    16873 |                    8 |
// |     3571 |     3619 |                    2 |
// |        1 |     3571 |                    2 |
// |    16873 |     3618 |                    2 |
// |    16303 |    16307 |                    2 |
// |    16305 |    16307 |                    2 |
// |    16302 |    16305 |                    2 |
// |    16834 |    16305 |                    2 |
// |     3571 |    16302 |                    2 |
// |    16302 |    16303 |                    2 |
// +----------+----------+----------------------+
// 18 rows in set (0.01 sec)

// term1_id does not include 3728
// term2_id does not include 1 (root, the most general one)

// arrow is from term2_id to term1_id 

// output
// //adjacency list

// //from term2_id -> from term1_id
// 3728 -> 16307, 16854
// 16854 -> 16834, 3727
// 3727 -> 16834
// 16834 -> 16873, 3618
// 16873 -> 3571, 3619
// 3619 -> 3571,
// 3571 -> 1
// 3618 -> 16873
// 16307 ->16303, 16305 
// 16305 -> 16302, 16834
// 16302 -> 3571
// 16303 -> 16302


// This DAG contains relationships (edge) also

// the dementation 1 is term2_id, which does not include 1 (root, the most general one)

// Array
// (
//     [3728] => Array
//         (
//             [16307] => 2
//             [16854] => 2
//        )

//     [16854] => Array
//         (
//             [16834] => 2
//             [3727] => 8
//        )

//     [3727] => Array
//         (
//             [16834] => 2
//        )

//     [16834] => Array
//         (
//             [16873] => 2
//             [3618] => 8
//        )

//     [16873] => Array
//         (
//             [3571] => 2
//             [3619] => 8
//        )

//     [3619] => Array
//         (
//             [3571] => 2
//        )

//     [3571] => Array
//         (
//             [1] => 2
//        )

//     [3618] => Array
//         (
//             [16873] => 2
//        )

//     [16307] => Array
//         (
//             [16303] => 2
//             [16305] => 2
//        )

//     [16305] => Array
//         (
//             [16302] => 2
//             [16834] => 2
//        )

//     [16302] => Array
//         (
//             [3571] => 2
//        )

//     [16303] => Array
//         (
//             [16302] => 2
//        )

// )

// The condition  r.term1_id <> 1 is to remove the All root, whose id is 1 

// function getDAG ($id, &$DAG) {

//   $sqlString = "SELECT DISTINCT r.term1_id, r.term2_id, r.relationship_type_id
//                 FROM
//                 graph_path 
//                 INNER JOIN
//                 term AS t ON (t.id = graph_path.term2_id)
//                 INNER JOIN
//                 term AS p ON (p.id = graph_path.term1_id)
//                 INNER JOIN
//                 term2term AS r ON (r.term2_id = p.id)
//                 WHERE r.term1_id <> 1 AND t.id = $id;";

//   // writing the query to select the term
// //  echo $sqlString;
//   $result = mysql_query ($sqlString);

//   if (!$result) {
//     echo "\n<p>Error performing query from getting DAG " . mysql_error() ."</p>";
//     echo $sqlString;
//     return 1;
//   }

//   //change the graph from list (matrix) into adjacency list
   

//   if (mysql_num_rows ($result) == 0 ){
//     echo "\n<p>No result found from the current query to get DAG </p>";
//     return 0;
//   }

//   while (list ($id1, $id2, $type) = mysql_fetch_array ($result)) {
//     $DAG[$id2][$id1] = $type; 
//   }
//   return 0;
// }




// function getDAG2 ($id, &$DAG) {

//   //$sqlString = "SELECT DISTINCT term1_id, term2_id relationship_type_id FROM graph_path2 WHERE term3_id = $termId;";
//   $sqlString = "SELECT term1_id, term2_id, relationship_type_id FROM graph_path2 WHERE term3_id = $id;";

//   /*
//   $sqlString = "SELECT DISTINCT r.term1_id, r.term2_id, r.relationship_type_id
//                 FROM
//                 graph_path 
//                 INNER JOIN
//                 term AS t ON (t.id = graph_path.term2_id)
//                 INNER JOIN
//                 term AS p ON (p.id = graph_path.term1_id)
//                 INNER JOIN
//                 term2term AS r ON (r.term2_id = p.id)
//                 WHERE t.id = $id;";

//   */
//   // writing the query to select the term
//   $result = mysql_query($sqlString);

//   if (!$result) {
//     echo "\n<p>Error performing query from getting DAG " . mysql_error() ."</p>";
//     return 1;
//   }


//   //change the graph from edge list (matrix) into adjacency list  
//   while (list($id1, $id2, $type) = mysql_fetch_array($result)) {
//     $DAG[$id2][$id1] = $type; 
//   }

//   return 0;
// }


// /**
// This funciton is only cal by dot to query the DAG with the root for dispaly simple.
// */
// function getDAG3 ($id, &$DAG) {

//   $sqlString = "SELECT DISTINCT r.term1_id, r.term2_id, r.relationship_type_id
//                 FROM
//                 graph_path 
//                 INNER JOIN
//                 term AS t ON (t.id = graph_path.term2_id)
//                 INNER JOIN
//                 term AS p ON (p.id = graph_path.term1_id)
//                 INNER JOIN
//                 term2term AS r ON (r.term2_id = p.id)
//                 WHERE t.id = $id;";

//   // writing the query to select the term
// //  echo $sqlString;
//   $result = mysql_query ($sqlString);

//   if (!$result) {
//     echo "\n<p>Error performing query from getting DAG " . mysql_error() ."</p>";
//     echo $sqlString;
//     return 1;
//   }

//   //change the graph from list (matrix) into adjacency list
   

//   if (mysql_num_rows ($result) == 0 ){
//     echo "\n<p>No result found from the current query to get DAG </p>";
//     return 0;
//   }

//   while (list ($id1, $id2, $type) = mysql_fetch_array ($result)) {
//     $DAG[$id2][$id1] = $type; 
//   }
//   return 0;
// }




?>

