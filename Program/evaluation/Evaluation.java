import java.io.*;

public class Evaluation{

	 public static double pearcorrelation(double[] gene1, double[] gene2) {
  		double avg1 = 0, avg2 = 0;
  		double sum1 = 0, sum2 = 0;
  		double operand1 = 0, operand2 = 0, result = 0;
  		double part1 = 0, part2 = 0;
  		for (int i = 0; i < gene1.length; i++) {
  			sum1 = sum1 + gene1[i];
  			sum2 = sum2 + gene2[i];
  		}
  		avg1 = sum1 / gene1.length;
  		
  		avg2 = sum2 / gene2.length;
  		//System.out.println("avg1 " + avg1 + " avg2 " + avg2);
  		for (int i = 0; i < gene1.length; i++) {
  			operand1 = operand1 + (gene1[i] - avg1) * (gene2[i] - avg2);
  		}
  		for (int i = 0; i < gene2.length; i++) {
  			part1 = part1 + Math.pow((gene1[i] - avg1), 2);
  			part2 = part2 + Math.pow((gene2[i] - avg2), 2);
  		}
  		operand2 = Math.sqrt(part1) * Math.sqrt(part2);
  		result = operand1 / operand2;
  		result = Math.abs(result);
  		return result;
  		
  	}
	
	public static void main(String[] args) {
		double[] distribution = new double[1000];
		double[] count = new double[1000];
        double[] average = new double[1000];
        double[] genesimilarity = new double[1000];
		 
		for (int i = 0; i < 1000; i++){
			distribution[i] = 0;
		    count[i] = 0;
		}
		
		for (int i = 0; i < 20; i++) {
			
			try {
				FileReader input = new FileReader("./output/distribution" + i*5);
				BufferedReader bufRead = new BufferedReader(input);
				String line;
				int index = 0;
				line = bufRead.readLine();
				while (line != null) {
					distribution[index] = distribution[index] + Double.parseDouble(line);
					index++;
					line = bufRead.readLine();
				}
			}catch(IOException e) {
				//e.printStackTrace();
			}
			
			//for (int j = 0; j < 1000; j++)
				//System.out.println(distribution[j]);
				
			try {
				FileReader input = new FileReader("./output/count" + i*5);
				BufferedReader bufRead = new BufferedReader(input);
				String line;
				int index = 0;
				line = bufRead.readLine();
				while (line != null) {
					count[index] = count[index] + Double.parseDouble(line);
					index++;
					line = bufRead.readLine();
				}
			}catch(IOException e) {
				//e.printStackTrace();
			}
			
			//for (int j = 0; j < 1000; j++)
				//System.out.println(count[j]);
			
		}
		
		 for (int i = 0; i < 1000; i++) {
             if (count[i] == 0) {
                     average[i] = distribution[i]; //no occurance, average = 0;
             }else {
             average[i] = distribution[i] / count[i];
             }
             //System.out.println(average[i]);
     }
		 
		 //write to file
		/* try {
             FileWriter fstream = new FileWriter("semsimilarity");
             BufferedWriter out = new BufferedWriter(fstream);
             for (int i = 0; i <  1000; i++) {
                 out.write(Double.toString(average[i]) + "\n");
             } 
             out.close();
         }catch (Exception e) {
             System.err.println("Error: " + e.getMessage());
         }
         */
		 //read the result of wang's method into file
		 try {
				FileReader input = new FileReader("./output/wangsimilarity");
				BufferedReader bufRead = new BufferedReader(input);
				String line;
				int index = 0;
				line = bufRead.readLine();
				while (line != null) {
					genesimilarity[index] = Double.parseDouble(line);
					index++;
					line = bufRead.readLine();
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			
         //compute the pearson correlation
         double result = 0;
         result = pearcorrelation(average, genesimilarity);
         System.out.println("pearson correlation is " + result);
      
     

	}

}

