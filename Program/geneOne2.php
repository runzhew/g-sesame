<?php session_start(); 

include( "../XHTML/header.html" ); 
include( "./Lib/geneOne.php" ); 
include( "./Lib/Display/geneOne.php" ); 


$gene2 = $_GET['gene2'];


$isA = $_SESSION['isA'];
$partOf = $_SESSION['partOf'];


$ontologies = $_SESSION['ontologies'];

$species2 = $_SESSION['species2'];
$dataSources2 = $_SESSION['dataSources2'];
$evidenceCodes2 = $_SESSION['evidenceCodes2'];
  
//connect to the database
$link = connectToMySQL();

$geneSymbol2 = getGeneSymbolFromGeneId( $gene2 );


$speciesGene = array();
//$species2 = getSpeciesFromGeneId( $gene2 );

$dataSourcesGene = array();
getDataSourcesFromGeneId( $gene2, $dataSources2, $dataSourcesGene );

$evidenceCodesGene = array();
getEvidenceCodesFromGeneId( $gene2, $evidenceCodes2, $evidenceCodesGene );
  


echo "<h4>$geneSymbol2 annotation information:</h4>";




//the following programs can be simplied for fewer functions
// for terms

$ids = array();
$accs = array();
$names = array();
$species = array();
$dataSources = array();
$evidenceCodes = array();

getTermsFromGene( $gene2, $ontologies, $species2, $dataSources2, $evidenceCodes2, $ids, $accs, $names, $species, $dataSources, $evidenceCodes );



echo "<br>$geneSymbol2 is annotated by the following GO terms:</br>";

displayTermsFromGene( $accs, $names, $dataSources, $evidenceCodes );



$terms = array();
$terms = array_pad( $terms, 3, array() );

//get terms

/*
echo "<br> One: " . $gene2;
echo "<br> One: " . $ontologies;
echo "<br> One: " . $species2;
echo "<br> One: " . $dataSources2;
echo "<br> One: " . $evidenceCodes2;
echo "<br>";
*/

getTermIdsFromGeneIdThreeOntologies( $gene2, $ontologies, $species2, $dataSources2, $evidenceCodes2, $terms );


//generate dot files  
//input the same parameters of first two

$terms2 = array(); 
for( $index = 0; $index < 3; $index++ ){
  $terms2 = array_merge( $terms2, $terms[$index] );
 }


//this function is called by geneCompareTwo by one ontology
getDotDAGFromTwoTerms( $terms2, $terms2, $imageFileName );



mysql_close( $link );

echo "<br>Click the following icon to show the DOT graph.</br>";
echo "<a href=\"$imageFileName\" target=\"_blank\">";
echo "<img src=\"$imageFileName\" height=\"100\", width=\"100\">";
echo "</a>";



include( "../XHTML/footer.html" ); 
?>

