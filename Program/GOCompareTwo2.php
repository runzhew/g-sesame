
<?php 
  session_start();
  require("../config.php");

  include_once ("evaluation/Lib/GOCompareTwo.php"); 
  include_once ("Lib/dot.php"); 

  //obtain data from web
  $tpl->assign("tool_id", $_POST['tool_id']);
  $form = $_POST['form'];
  $tpl->assign("tool_name", $form);

  $term1 = $_POST['GOTerm1'];
  $term1 = trim($term1);
  $term15 = $term1;
  $term1 = prepareGOTerm($term1);

  $tpl->assign("term1", $term1);

  $term2 = $_POST['GOTerm2'];
  $term2 = trim($term2);
  $term25 = $term2;
  $term2 = prepareGOTerm($term2);

  $tpl->assign("term2", $term2);

  $isA = $_POST['isA'];
  $tpl->assign("isA", $isA);
  
  $partOf = $_POST['partOf'];
  $tpl->assign("partOf", $partOf);

  //connect to the database
  $link = connectToMySQL();


  //get term acc
  //$termAcc1 = 'GO:' . $term1; 
  //$termAcc2 = 'GO:' . $term2; 


  $id1 = getTermId ($term1);
  $tpl->assign("id1", $id1);
  $id2 = getTermId ($term2);
  $tpl->assign("id2", $id2);


  $termName1 = getTermName ($id1);
  $termName2 = getTermName ($id2);
  $tpl->assign("termName1", $termName1);
  $tpl->assign("termName2", $termName2);


  //get term def
  $termDefinition1 = getTermDefinition ($id1);
  $termDefinition2 = getTermDefinition ($id2);
  $tpl->assign("termDefinition1", $termDefinition1);
  $tpl->assign("termDefinition2", $termDefinition2);

  $termSynonym1 = getTermSynonym ($id1);
  $termSynonym2 = getTermSynonym ($id2);
  $tpl->assign("termSynonym1", $termSynonym1);
  $tpl->assign("termSynonym2", $termSynonym2);

  $similarity = compareTwoGOIds ($id1, $id2, $isA, $partOf);
  $similarity2 = number_format ($similarity, 3);
  $tpl->assign("similarity", $similarity);
  $tpl->assign("similarity2", $similarity2);

  $tpl->assign("submit", "1");

  getClientInformation ("GOCompareTwo", "no");
  getDotDAGFromTwoGOs ($id1, $id2, $imageFileName);
  
  $tpl->assign("imageFileName", $imageFileName);

//  $result_page = "result1+2";
  $result_page = "result1+2";
  $tpl -> assign("result_page", $result_page);
  
  $html = $tpl->draw('tool', $return_string = true);
  echo $html;


?>




  <?php 
  mysql_close ($link);
  ?>