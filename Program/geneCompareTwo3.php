<?php session_start();
include ("../XHTML/header.txt");
include_once ("./Lib/geneCompareTwo.php"); 

$id1 = $_GET['id1'];
$id2 = $_GET['id2'];


//session
//here is id, not symbol
//$gene = $_SESSION['gene1'];

$isA = $_SESSION['isA'];
$partOf = $_SESSION['partOf'];


$ontology = $_SESSION['ontology'];


$species1 = $_SESSION['species1'];
$dataSources1 = $_SESSION['dataSources1'];
$evidenceCodes1 = $_SESSION['evidenceCodes1'];
  

$species2 = $_SESSION['species2'];
$dataSources2 = $_SESSION['dataSources2'];
$evidenceCodes2 = $_SESSION['evidenceCodes2'];




//connect to the database
$link = connectToMySQL ();


//the following codes are the same as geneCompareTwo.php, 
//the lower part of the symbol comparison function.


$termComparisonTable = array ();
$status = compareTwoGeneIds ($id1, $id2, $ontology, 
			     $dataSources1, $evidenceCodes1, 
			     $dataSources2, $evidenceCodes2, 
			     $isA, $partOf, $result, $termComparisonTable);
  

//the following is to display
displayGeneCompareTwo ($id1, $id2, $result, $termComparisonTable);


mysql_close ($link);
getClientInformation ("geneCompareTwo", "no");

include_once ("../XHTML/footer.txt"); 

?>

