<?php
session_start();
include_once "function.php";

/******************************************************
*
* upload document from user
*
*******************************************************/


//Create Directory if doesn't exist
//The directory's authority is 0744 
//0 means octal, 7 is 111 in binary which means it has the 1-read, 1-write, 1-execute right of the owner
//4 is 100 in binary means 1-read 0-write 0-execute right, the first 4 means for the group of the owner
//And the last 4 means for the other user.

function fileextension($file) {
	return strtolower(substr($file, strrpos($file,".")));
}
//echo "Filename " . $_POST['filename'];
//if(!file_exists('uploads/'))
	//mkdir('uploads/', 0744);
	
//     $prefix = date('YmdHms');
	
	  
	/*  if ($_POST['filename'] == "") {
        $name = urlencode($_FILES["file"]["name"]);
		
		
        $pos = strpos($name, ".CEL");
        $name = substr($name, 0 , $pos);
		
		
        $tempname = "";
        $namearray = explode(" ", $name);
        for($i = 0; $i < sizeof($namearray); $i++) {
            $tempname = $tempname . $namearray[$i]; 
        } 
		
		$subpath = $prefix . $tempname;
		
	  //$upfile = "uploads/" . $prefix . $tempname . ".CEL";
	  $upfile = "uploads/" . $subpath . "/" . $prefix . $tempname . ".CEL";
	  }else {
        $name = "";
        $namearray = explode(" ", $_POST['filename']);
        for($i = 0; $i < sizeof($namearray); $i++) {
            $name = $name . $namearray[$i]; 
        } 
     
	    $subpath = $prefix . $name;
		
       // $upfile = "uploads/" . $prefix . $name . ".CEL";
	   $upfile = "uploads/" . $subpath . "/" . $prefix . $name . ".CEL";
      
	  }
	*/  
	/*  if ($_FILES['file']['name']=="") {
        $result = "7";

      } else if ($_FILES['file']['name']<>"") {
	   
       $ext = fileextension($_FILES['file']['name']);
       
       if ($ext != ".cel") {
        $result = "8";
	
	   }else { */
	    
		
	   $_SESSION['email'] = $_POST['email'];
    
	 if ($_SESSION['email'] == "") {
	   $_SESSION['result'] = "17";
	  
	  require_once "index.php";
	  exit;
	}
	
	if ($_POST['bgcorrect'] == "0") {
	   $_SESSION['bgcorrect'] = "mas";
	}else if ($_POST['bgcorrect'] == "1") {
	   $_SESSION['bgcorrect'] = "rma";
	}else {
	   $_SESSION['result'] = "18";
	   require_once "index.php";
	  exit;
	}
	
	
	if ($_POST['normalization'] == "0") {
	   $_SESSION['normalization'] = "constant";
	}else if ($_POST['normalization'] == "1") {
	   $_SESSION['normalization'] = "contrasts";
	}else if ($_POST['normalization'] == "2") {
	   $_SESSION['normalization'] = "invariantset";
	}else if ($_POST['normalization'] == "3") {
	   $_SESSION['normalization'] = "loess";
	}else if ($_POST['normalization'] == "4") {
	   $_SESSION['normalization'] = "methods";
	}else if ($_POST['normalization'] == "5") {
	   $_SESSION['normalization'] = "qspline";
	}else if ($_POST['normalization'] == "6") {
	   $_SESSION['normalization'] = "quantiles";
	}else if ($_POST['normalization'] == "7") {
	   $_SESSION['normalization'] = "quantiles.robust";
	}else if ($_POST['normalization'] == "8") {
	   $_SESSION['normalization'] = "yBatch.methods";
	}else {
	   $_SESSION['result'] = "19";
	   require_once "index.php";
	  exit;
	}
	
	if ($_POST['summarization'] == "0") {
	   $_SESSION['summarization'] = "avgdiff";
	}else if ($_POST['summarization'] == "1") {
	   $_SESSION['summarization'] = "liwong";
	}else if ($_POST['summarization'] == "2") {
	   $_SESSION['summarization'] = "mas";
	}else if ($_POST['summarization'] == "3") {
	   $_SESSION['summarization'] = "medianpolish";
	}else if ($_POST['summarization'] == "4") {
	   $_SESSION['summarization'] = "playerout";
	}else {
	   $_SESSION['result'] = "20";
	   require_once "index.php";
	  exit;
	}
	
	$subpath = "hgu95dataset";
	//create subdirectory
	$subpath = "uploads/" . $subpath . "/";
//	if(!file_exists($subpath))
	//mkdir($subpath, 0744);
	
	//   	if(is_uploaded_file($_FILES["file"]["tmp_name"]))
		//	{
				//Check if the file has been moved from temporary directory to target directory
			/*	if(!move_uploaded_file($_FILES["file"]["tmp_name"],$upfile))
				{
					$result="5"; 
				}
				else 
				{
					
					
					$result="0";
				}
			}
			else  
			{
					$result="6";
			}
      }
		}
		
	*/
		
	//check whether the file is sorted 
	

//  $_SESSION['total'] = $total;
//if ( $result != "0") {
	//$_SESSION['result'] = $result;
	
   //require_once "index.php";
 
//}else {
	//upload file successfully
	

//	if ( $result == "0") { 
	//   $result = "10";
	   
	//  $_SESSION['filename'] = $upfile;
   
	 //$filename = $_SESSION['filename'];
	
	 $email = $_SESSION['email'];
	 $bgcorrect = $_SESSION['bgcorrect'];
	 $normalization = $_SESSION['normalization'];
	 $summarization = $_SESSION['summarization'];
	 
	 $logpath = $subpath .  "log.txt";
	 
/*	 if ($_POST['filename'] == "") {
          $name = $_FILES['file']['name'];
        $pos = strpos($name, ".CEL");
        $name = substr($name, 0 , $pos);
        $tempname = "";
        $namearray = explode(" ", $name);
        for($i = 0; $i < sizeof($namearray); $i++) {
            $tempname = $tempname . $namearray[$i]; 
        } 
     
        $originalfilename = $tempname . ".CEL";


	 }else {
          $name = "";
        $namearray = explode(" ", $_POST['filename']);
        for($i = 0; $i < sizeof($namearray); $i++) {
            $name = $name . $namearray[$i]; 
        } 
       
        $originalfilename = $name;
	  
	 }*/
	 ignore_user_abort(true);
	 
	 $outputdata = "http://mmlab.cs.clemson.edu/881/" .  $subpath . "mydata.csv";
	 $log = "http://mmlab.cs.clemson.edu/881/" .  $logpath;
	 
	 if (($bgcorrect == "mas") && ($normalization == "constant") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess1.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "constant") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess2.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "constant") && ($summarization == "mas") ) { 
	 $command = "sh preprocess3.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "constant") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess4.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "mas") && ($normalization == "constant") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess5.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 	 if (($bgcorrect == "mas") && ($normalization == "contrasts") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess6.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "contrasts") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess7.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "contrasts") && ($summarization == "mas") ) { 
	 $command = "sh preprocess8.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "contrasts") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess9.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "mas") && ($normalization == "contrasts") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess10.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 
	 
	 	 if (($bgcorrect == "mas") && ($normalization == "invariantset") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess11.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "invariantset") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess12.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "invariantset") && ($summarization == "mas") ) { 
	 $command = "sh preprocess13.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "invariantset") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess14.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "mas") && ($normalization == "invariantset") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess15.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 	 	 if (($bgcorrect == "mas") && ($normalization == "loess") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess16.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "loess") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess17.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "loess") && ($summarization == "mas") ) { 
	 $command = "sh preprocess18.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "loess") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess19.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "mas") && ($normalization == "loess") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess20.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	  	 	 if (($bgcorrect == "mas") && ($normalization == "methods") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess21.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "methods") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess22.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "methods") && ($summarization == "mas") ) { 
	 $command = "sh preprocess23.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "methods") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess24.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "mas") && ($normalization == "methods") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess25.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 
	 	  	 	 if (($bgcorrect == "mas") && ($normalization == "qspline") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess26.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "qspline") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess27.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "qspline") && ($summarization == "mas") ) { 
	 $command = "sh preprocess28.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "qspline") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess29.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "mas") && ($normalization == "qspline") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess30.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 
	 
	  if (($bgcorrect == "mas") && ($normalization == "quantiles") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess31.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "quantiles") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess32.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "quantiles") && ($summarization == "mas") ) { 
	 $command = "sh preprocess33.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "quantiles") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess34.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "mas") && ($normalization == "quantiles") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess35.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 	  if (($bgcorrect == "mas") && ($normalization == "quantiles.robust") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess36.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "quantiles.robust") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess37.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "quantiles.robust") && ($summarization == "mas") ) { 
	 $command = "sh preprocess38.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "quantiles.robust") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess39.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "mas") && ($normalization == "quantiles.robust") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess40.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	   if (($bgcorrect == "mas") && ($normalization == "yBatch.methods") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess41.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "yBatch.methods") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess42.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "yBatch.methods") && ($summarization == "mas") ) { 
	 $command = "sh preprocess43.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "mas") && ($normalization == "yBatch.methods") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess44.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "mas") && ($normalization == "yBatch.methods") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess45.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 	 if (($bgcorrect == "rma") && ($normalization == "constant") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess46.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "constant") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess47.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "constant") && ($summarization == "mas") ) { 
	 $command = "sh preprocess48.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "constant") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess49.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "rma") && ($normalization == "constant") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess50.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 	 if (($bgcorrect == "rma") && ($normalization == "contrasts") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess51.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "contrasts") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess52.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "contrasts") && ($summarization == "mas") ) { 
	 $command = "sh preprocess53.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "contrasts") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess54.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "rma") && ($normalization == "contrasts") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess55.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 
	 
	 	 if (($bgcorrect == "rma") && ($normalization == "invariantset") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess56.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "invariantset") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess57.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "invariantset") && ($summarization == "mas") ) { 
	 $command = "sh preprocess58.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "invariantset") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess59.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "rma") && ($normalization == "invariantset") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess60.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 	 	 if (($bgcorrect == "rma") && ($normalization == "loess") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess61.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "loess") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess62.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "loess") && ($summarization == "mas") ) { 
	 $command = "sh preprocess63.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "loess") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess64.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "rma") && ($normalization == "loess") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess65.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	  	 	 if (($bgcorrect == "rma") && ($normalization == "methods") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess66.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "methods") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess67.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "methods") && ($summarization == "mas") ) { 
	 $command = "sh preprocess68.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "methods") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess69.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "rma") && ($normalization == "methods") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess70.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 
	 	  	 	 if (($bgcorrect == "rma") && ($normalization == "qspline") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess71.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "qspline") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess72.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "qspline") && ($summarization == "mas") ) { 
	 $command = "sh preprocess73.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "qspline") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess74.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "rma") && ($normalization == "qspline") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess75.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 
	 
	  if (($bgcorrect == "rma") && ($normalization == "quantiles") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess76.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "quantiles") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess77.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "quantiles") && ($summarization == "mas") ) { 
	 $command = "sh preprocess78.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "quantiles") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess79.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "rma") && ($normalization == "quantiles") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess80.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 	  if (($bgcorrect == "rma") && ($normalization == "quantiles.robust") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess81.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "quantiles.robust") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess82.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "quantiles.robust") && ($summarization == "mas") ) { 
	 $command = "sh preprocess83.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "quantiles.robust") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess84.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "rma") && ($normalization == "quantiles.robust") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess85.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	   if (($bgcorrect == "rma") && ($normalization == "yBatch.methods") && ($summarization == "avgdiff") ) { 
	 $command = "sh preprocess86.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "yBatch.methods") && ($summarization == "liwong") ) { 
	 $command = "sh preprocess87.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "yBatch.methods") && ($summarization == "mas") ) { 
	 $command = "sh preprocess88.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 if (($bgcorrect == "rma") && ($normalization == "yBatch.methods") && ($summarization == "medianpolish") ) { 
	 $command = "sh preprocess89.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	  if (($bgcorrect == "rma") && ($normalization == "yBatch.methods") && ($summarization == "playerout") ) { 
	 $command = "sh preprocess90.sh $subpath $bgcorrect $normalization $summarization $email $outputdata $log $logpath > $logpath 2>&1 &";
	 }
	 
	 
	 
	 
	 
     shell_exec($command); 
	// unset($_SESSION['filename']);
	 unset($_SESSION['email']);
	 unset($_SESSION['bgcorrect']);
	 unset($_SESSION['normalization']);
	 unset($_SESSION['summarization']);
	 require_once "index.php";
	 echo "
	 <script type='text/JavaScript'>
     alert('The file is sent to processing! The result will be sent to you via email in a few minutes.');
     </script>";
	 
	

  
//}
	
	  
	
	   
	




 


?>





