<?php 
include_once (dirname(__FILE__) . "/../XHTML/header.txt"); 
include_once (dirname(__FILE__) . "/Lib/preProcess.php"); 

include_once (dirname(__FILE__) . "/Lib/geneCompareMultiple.php"); 
include_once (dirname(__FILE__) . "/Lib/geneCluster.php"); 

//display
include_once (dirname(__FILE__) . "/Lib/Display/geneCompareMultiple.php");
include_once (dirname(__FILE__) . "/Lib/Display/geneCluster.php"); 


include_once (dirname(__FILE__) . "/Lib/email.php"); 
include_once (dirname(__FILE__) . "/Lib/time.php"); 



//For clustering, there are three steps:
//step 1: clustering
//step 2: get sequence for display for not intersection between two nodes
//step 3: change the 3D array for display
//step 4: display only (function is in another file)





//let programs run forever
set_time_limit(0);
ignore_user_abort(true);



//construct a temp file name to avoid name collision
//example: acc1000phpVEqZrN

$IP = getenv ("REMOTE_ADDR");


$targetPath = dirname(__FILE__) . "/Temp/";
$targetPath .= $IP;
$targetPath .= basename ($_FILES['uploadedFile']['name']);
$targetPath .= basename ($_FILES['uploadedFile']['tmp_name']);

//echo $targetPath;

if (!move_uploaded_file ($_FILES['uploadedFile']['tmp_name'], $targetPath)) {
  echo "\n<p>There was an error of uploading the file, please try again.</p>";
  printHtmlEnd ();
  exit (); 
 }


$genes = file_get_contents ($targetPath);

$method = $_POST['method'];

$deltaDecreasement = $_POST['deltaDecreasement'];



$ontology = $_POST['ontology'];

$species = $_POST['species'];
//case sensitive
if ($species == "All") {
  echo "You must choose one valid species.";
  return;
}

$dataSources = $_POST['dataSources'];
$evidenceCodes = $_POST["evidenceCodes"];

$emailAddress = "";
$description = "";


//email is the checkbox
$flagScreenOrfile = 0;
if (isset ($_POST["email"])){

  $flagScreenOrfile = 1;

  $emailAddress = $_POST["emailAddress"];
  $description = $_POST['description'];

  //display online
  echo "\n<SCRIPT LANGUAGE=\"JavaScript\">";
  echo "alert (\" File is uploaded. You may close your browser now. The results will be mailed to you.\")";
  echo "</SCRIPT>\n";
  flush ();
 } 
 else{
   $emailAddress = "no";
 }



//connect to the database
$link = connectToMySQL ();


$geneSymbols = array ();

preProcessUploaded ($genes, $geneSymbols);


//genes with synonyms and unknown token
//id => symbols
$genesComparison = array ();

//symbols only
$genesUnknown = array ();


replaceSynonyms ($geneSymbols, $ontology, $species, $dataSources, $evidenceCodes, $genesComparison, $genesUnknown);

//display the unknown genes
$size = sizeof ($genesUnknown);

if ($size != 0) {
  echo "\n</p>The following genes are not annotated in our database using the current filter:</p>";
  
  display1DArray ($fileHandleResult, $genesUnknown);
  //echo "</b><br><br>";
 }


$geneIds = array_keys ($genesComparison);

//print_r($geneIds);



//2D associative array
$similarityTable = array ();
compareGenesMultipleStatistics ($geneIds, $ontology, $dataSources, $evidenceCodes, $method, $similarityTable);


//3D array
$thresholds = array ();
$clusterTree = array ();


cluster ($similarityTable, $deltaDecreasement, $thresholds, $clusterTree);



//display 3D array


//before sequences
//displayClusterTree($genesComparison, $thresholds, $clusterTree, 0, $fileHandle);
 

$sequence = array();
getSequence ($clusterTree, $sequence);
//echo "The correct sequence are:<p>";

//print_r($sequence);

$clusterTreeConsistent = array();
changeClusterTreeToConsistent ($sequence, $clusterTree, $clusterTreeConsistent);


//0 is false, display online
//1 is true, send via email

//print_r($thresholds);



//print_r($genesComparison);
//print_r($clusterTreeConsistent);

$currentStamp = microtime_float ();
$fileNameResult = "$currentStamp.html";
$fileHandleResult = fopen ("./Temp/" . $fileNameResult, "a");

if (isset( $_POST["email"])) {

  $emailAddress = $_POST["emailAddress"];
  $description = $_POST['description'];

  //append $similarityTable to $fileHandle
    displayGeneMultipleSimilarities (1, $fileHandleResult, $similarityTable);
    displayClusterTree ($genesComparison, $thresholds, $clusterTreeConsistent, 1, $fileHandleResult);
//  displayGOCompareMultiple ($similarityTable, 1, $fileHandleResult);

//echo $fileHandleResult;
//echo $fileNameResult;

  //send email
  //sendEmail ($emailAddress, $description, $fileHandleResult);
  sendEmail ($emailAddress, $description, $fileNameResult);
 }
 else{
   $emailAddress = "no";
//   displayGOCompareMultiple ($similarityTable, 0, $fileHandleResult);
     displayGeneMultipleSimilarities (0, $fileHandleResult, $similarityTable);
     displayClusterTree ($genesComparison, $thresholds, $clusterTreeConsistent, 0, $fileHandleResult);
 }



//final 
mysql_close ($link);
unlink ($targetPath);


//using geneCluster to save characters
getClientInformation ("geneCluster", $emailAddress);

include("../XHTML/footer.txt"); 
?>

