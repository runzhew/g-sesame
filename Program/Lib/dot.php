<?php
include_once ("GOOne.php"); 

/*
 Improvement

 1. Do not use getDAG2, try to call basic functions
 2. Colors can be defined universially 
 3. All generage dot file can be merged?
 4. All functions can be merged?  

 GO
 term1 vs. term1
 term1 vs. term2 done

 gene
 terms1 vs. terms1 done
 terms1 vs. terms2 done



*/



//input term is the format GO:0005739
//combine two DAGs together
//call by GOComparisionTwo

//output is as follow

/*
 digraph G { edge [dir=back]; node [fontsize=9]; 
 3728 [label="mitochondrion \n GO:0005739 " ]
 3728 [shape=box]
 3728 [fillcolor=lightcyan]
 3728 [style=filled]
 16854 [label="cytoplasmic part \n GO:0044444 " ]
 16854 [fillcolor=yellow2]
 16854 [style=filled]
 3727 [label="cytoplasm \n GO:0005737 " ]
 3727 [fillcolor=yellow2]
 3727 [style=filled]
 16834 [label="intracellular part \n GO:0044424 " ]
 16834 [fillcolor=yellow2]
 16834 [style=filled]
 16873 [label="cell part \n GO:0044464 " ]
 16873 [fillcolor=yellow2]
 16873 [style=filled]
 3619 [label="cell \n GO:0005623 " ]
 3619 [fillcolor=yellow2]
 3619 [style=filled]
 3571 [label="cellular_component \n GO:0005575 " ]
 3571 [fillcolor=yellow2]
 3571 [style=filled]
 3618 [label="intracellular \n GO:0005622 " ]
 3618 [fillcolor=yellow2]
 3618 [style=filled]
 16307 [label="intracellular membrane-bound organelle \n GO:0043231 " ]
 16307 [fillcolor=yellow2]
 16307 [style=filled]
 16305 [label="intracellular organelle \n GO:0043229 " ]
 16305 [fillcolor=yellow2]
 16305 [style=filled]
 16302 [label="organelle \n GO:0043226 " ]
 16302 [fillcolor=yellow2]
 16302 [style=filled]
 16303 [label="membrane-bound organelle \n GO:0043227 " ]
 16303 [fillcolor=yellow2]
 16303 [style=filled]
 3763 [label="peroxisome \n GO:0005777 " ]
 3763 [shape=box]
 3763 [fillcolor=orange]
 3763 [style=filled]
 15664 [label="microbody \n GO:0042579 " ]
 15664 [fillcolor=orange]
 15664 [style=filled]



 "15664" -> "3763";
 "16307" -> "15664";
 "16854" -> "15664";
 "16834" -> "16854";
 "3727" -> "16854" [style = dashed];
 "16834" -> "3727";
 "16873" -> "16834";
 "3618" -> "16834" [style = dashed];
 "3571" -> "16873";
 "3619" -> "16873" [style = dashed];
 "3571" -> "3619";
 "16873" -> "3618";
 "16303" -> "16307";
 "16305" -> "16307";
 "16302" -> "16305";
 "16834" -> "16305";
 "3571" -> "16302";
 "16302" -> "16303";
 "16307" -> "3728";
 "16854" -> "3728";
 }
*/


//merge two DAGs without change original DAGs
function mergeTwoDAGs (&$DAG1, &$DAG2) {

  // print_r($DAG1);
  //print_r($DAG2);

  foreach($DAG2 as $vertex1 => $edges) {
    foreach($edges as $vertex2 => $relationship) {
      $DAG1[$vertex1][$vertex2] = $relationship;
    }
  }
  //print_r($DAG1); 
}


//input ids
//output dot file
//written AUgust 18
function getDotDAGFromTwoGOs($id1, $id2, &$imageFileName) {

  $DAG = array ();
  $DAG2 = array ();

  getDAG($id1, $DAG);
  getDAG($id2, $DAG2);
  
  mergeTwoDAGs ($DAG, $DAG2);


  //the following code using graph notations
  //output dot file
  $tempfile = trim(shell_exec ("mktemp /var/www/html/rain/Program/Temp/GOdot.XXXXXX"));

  $temp = fopen ($tempfile, 'w') or die("could not open temporary file $tempfile");

  fwrite ($temp, "digraph G { edge [dir=back]; node [fontsize=9]; \n");


  //walk through graph twice to make the structure of program simple

  //vertex
  //1 is not included in vertexs
  //  fwrite ($temp, "1 [label=\"ROOT\"];\n"); 
  
  foreach ($DAG as $vertex1 => $idTemp1) {
    
    $name = getTermName ($vertex1);
    $acc = getTermAcc ($vertex1);
    
    //second \n is for the dot source file clean
    //a space must precede "["
    fwrite ($temp, "$vertex1 [label=\"$name \\n $acc\"];\n");


    //two bottom nodes
    if ($vertex1 == $id1 && $vertex1 == $id2) {
      fwrite ($temp, "$vertex1 [fillcolor=grey]\n");
      fwrite ($temp, "$vertex1 [style=filled];\n");
    }
    else {
      if ($vertex1 == $id1) {
	fwrite ($temp, "$vertex1 [fillcolor=lightcyan];\n");
	fwrite ($temp, "$vertex1 [style=filled];\n");
      }
      //if exist in both DAGs
      if ($vertex1 == $id2) {
	fwrite ($temp, "$vertex1 [fillcolor=orange];\n");
	fwrite ($temp, "$vertex1 [style=filled];\n");      
      }
    }

  }
  

  //edges
  foreach($DAG as $vertex1 => $edges) {
    foreach($edges as $vertex2 => $relationship) {

	if(!array_key_exists($vertex2,$DAG))
{
	$name = getTermName($vertex2);
	$acc = getTermAcc($vertex2);
	fwrite($temp, "$vertex2 [label=\"$name \\n $acc\"];\n");

}






      if ($relationship == 1) {
	fwrite ($temp, "\"$vertex2\" -> \"$vertex1\";\n");
      }
      else {//8
	fwrite ($temp, "\"$vertex2\" -> \"$vertex1\" [style = dashed];\n");
      }      
    }
  }




  fwrite ($temp, "}\n");
  fclose ($temp);

  passthru ("/usr/bin/dot -Tpng $tempfile -o $tempfile.png");
  //passthru ("/usr/bin/dot -Tps2 $tempfile -o $tempfile.ps");
    
  //transfer it outside
  $imageFileName = "$tempfile.png";
 
}





//input are two termids groups, each group is related with one gene with one ontology
//output all the terms and their ancestors
//This function is called by geneTop simimarity hyperlink (geneComparisonTwo.php)

function getDotDAGFromTwoTerms ($terms1, $terms2, &$imageFileName) {


  $terms = array ();
  $terms = array_merge ($terms1, $terms2);
 

  $terms = array_unique ($terms);

  $DAG = array ();
  foreach ($terms as $id1) {   
    getDAG3($id1, $DAG1);
    mergeTwoDAGs ($DAG, $DAG1);    
  }



  //the following code using graph notations
  //output dot file
  $tempfile = trim (shell_exec ("mktemp ./Temp/GOdot.XXXXXX"));

  $temp = fopen ($tempfile, 'w') or die ("could not open temporary file $tempfile");

  fwrite ($temp, "digraph G { edge [dir=back]; node [fontsize=9]; \n");


  //walk through graph twice to make the structure of program simple

  //vertex
  //1 is not included in vertexs
  fwrite ($temp, "1 [label=\"All\"];\n");
  fwrite ($temp, "2570 [label=\"Molecular function\"];\n");
  
  foreach ($DAG as $vertex1 => $idTemp1) {
    
    $name = getTermName ($vertex1);
    $acc = getTermAcc ($vertex1);
    
    //second \n is for the dot source file clean
    //a space must precede "["
    fwrite ($temp, "$vertex1 [label=\"$name \\n $acc\"];\n");

    
    //both in two genes
    if (in_array ($vertex1, $terms1) && in_array ($vertex1, $terms2)) {
      
      //filled with colors
      fwrite ($temp, "$vertex1 [fillcolor=grey]\n");
      //fwrite($temp, "$vertex1 [fillcolor=yellow2];\n");
      fwrite ($temp, "$vertex1 [style=filled];\n");
    }    
    else{
      //only belongs to one gene
      if (in_array ($vertex1, $terms1)) {
	  
	//filled with colors
	//fwrite($temp, "$vertexId [fillcolor=greenyellow]\n");
	fwrite ($temp, "$vertex1 [fillcolor=lightcyan];\n");
	fwrite ($temp, "$vertex1 [style=filled];\n");      
      }
      if (in_array($vertex1, $terms2)) {
	//filled with colors
	//fwrite($temp, "$vertexId [fillcolor=greenyellow]\n");
	fwrite ($temp, "$vertex1 [fillcolor=orange];\n");
	fwrite ($temp, "$vertex1 [style=filled];\n");   
      }
    }
  
  }
  //edges
  foreach($DAG as $vertex1 => $edges) {
    foreach($edges as $vertex2 => $relationship) {

        if(!array_key_exists($vertex2,$DAG))
{
        $name = getTermName($vertex2);
        $acc = getTermAcc($vertex2);
        fwrite($temp, "$vertex2 [label=\"$name \\n $acc\"];\n");

}
    









  if ($relationship == 1) {
	fwrite ($temp, "\"$vertex2\" -> \"$vertex1\";\n");
      }
      else {//8
	fwrite ($temp, "\"$vertex2\" -> \"$vertex1\" [style = dashed];\n");
      }      
    }
  }


  fwrite ($temp, "}\n");
  fclose ($temp);

  passthru ("/usr/bin/dot -Tpng $tempfile -o $tempfile.png");
  
  //If we need to addinto paper, this one is necessary.
  //  passthru ("/usr/bin/dot -Tps2 $tempfile -o $tempfile.ps");
    
  //transfer it outside
  $imageFileName = "$tempfile.png";
 
  //  shell_exec("rm -f $tempfile");
  //shell_exec("rm -f $tempfile.png");   

  

}



?>

