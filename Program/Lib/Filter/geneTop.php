<?php

/*
 //this inner select is very very slow
SELECT DISTINCT gene_product.id as gene FROM gene_product, term WHERE gene_product.type_id IN ( SELECT id FROM term WHERE acc = 'gene' );

SELECT gene_product.id FROM gene_product, term WHERE gene_product.type_id IN ( SELECT id FROM term WHERE acc = 'gene' );



The All selection is:
7597 rows in set (16.86 sec)


SELECT DISTINCT gene_product.id as gene FROM gene_product, term, association WHERE gene_product.type_id = 22720 AND association.gene_product_id = gene_product.id AND association.term_id = term.id AND term.id IN ( 3728,3730,3797,2631,4442,4570,4792 ) AND gene_product.species_id IN (183843, 65171, 55496, 93565, 147186, 122968, 37503, 99698, 74685, 80326, 141661, 46840, 4014, 168394, 188897, 167961, 29635, 58866, 201084, 224608, 187829, 102855, 196385, 184244, 138072, 155661, 184320, 204218, 194587, 159620, 185515, 227923, 88935, 134583, 1868, 193395, 48274, 4768, 198597, 32366, 149446, 230763, 120623, 48202, 50668, 185395, 12717, 97048, 128029, 10700, 212040, 7015, 88307, 188793, 157890, 204764, 63549, 217105, 64711, 181728, 80753, 92001, 24102, 18287, 50417, 11586, 61706, 49614, 186394, 196357, 21399, 178999, 47916, 23509, 163332, 226339, 61392, 4158, 20020, 167812, 232338, 108326, 80, 169808, 19829, 228726, 155871, 224623, 222850, 208613, 166754, 218124, 63296, 141450, 160364, 60875, 88497, 155770, 42020, 153074, 118195, 172277, 112303, 158142, 140266, 29487) ;

7597 rows in set (4 sec)


//more general one

SELECT DISTINCT gene_product.id as gene FROM gene_product, term, association WHERE gene_product.type_id IN ( SELECT id FROM term WHERE acc = 'gene' ) AND association.gene_product_id = gene_product.id AND association.term_id = term.id AND term.id IN ( 3728,3730,3797,2631,4442,4570,4792 ) AND gene_product.species_id IN (183843, 65171, 55496, 93565, 147186, 122968, 37503, 99698, 74685, 80326, 141661, 46840, 4014, 168394, 188897, 167961, 29635, 58866, 201084, 224608, 187829, 102855, 196385, 184244, 138072, 155661, 184320, 204218, 194587, 159620, 185515, 227923, 88935, 134583, 1868, 193395, 48274, 4768, 198597, 32366, 149446, 230763, 120623, 48202, 50668, 185395, 12717, 97048, 128029, 10700, 212040, 7015, 88307, 188793, 157890, 204764, 63549, 217105, 64711, 181728, 80753, 92001, 24102, 18287, 50417, 11586, 61706, 49614, 186394, 196357, 21399, 178999, 47916, 23509, 163332, 226339, 61392, 4158, 20020, 167812, 232338, 108326, 80, 169808, 19829, 228726, 155871, 224623, 222850, 208613, 166754, 218124, 63296, 141450, 160364, 60875, 88497, 155770, 42020, 153074, 118195, 172277, 112303, 158142, 140266, 29487) ;


7597 rows in set (5.57 sec)

*/




//called by geneTop in function getGenesFromTerms
//here &$species, &$dataSources, &$evidenceCodes are number 2
function constructFilterToGetGenes( &$ontologies, &$species, &$dataSources, &$evidenceCodes, &$terms3, $searchLimitation ){

  /*
   $sqlString = "SELECT DISTINCT gene_product.id as geneId, gene_product.symbol AS gene, term.name
   FROM gene_product, term, association";
  */


  $sqlString = "SELECT DISTINCT gene_product.symbol, gene_product.full_name, species.species, db.name, evidence.code FROM gene_product, term, association, species, db, evidence ";

  
  //longest
  //$sqlString = "SELECT DISTINCT gene_product.symbol, gene_product.full_name, species.common_name, db.name, evidence.code FROM gene_product, term, association, species, db, evidence, dbxref, evidence_dbxref ";
  
  
  $sqlCondition = "WHERE gene_product.type_id IN ( SELECT id FROM term WHERE acc = 'gene' ) AND ";
  $sqlCondition .= "association.gene_product_id = gene_product.id AND ";
  $sqlCondition .= "association.term_id = term.id AND ";

  //  $sqlCondition .= "gene_product.species_id = species.id AND ";
  //  $sqlCondition .= "association.source_db_id = db.id AND ";

  //first way to query evidence
  //$sqlCondition .= "association.id = evidence.association_id AND ";

  //first way to query evidence
  //  $sqlCondition .= " ( ( association.id = evidence.association_id ) OR ";
  //query another way
  //$sqlCondition .= " ( gene_product.dbxref_id = dbxref.id AND dbxref.id = evidence_dbxref.dbxref_id AND evidence_dbxref.evidence_id = evidence.id AND evidence.association_id = association.id AND association.gene_product_id = gene_product.id ) ) AND ";
  
  
  $sqlCondition .= "term.id IN ( $terms3 ) ";
  
  
  if ( $ontologies[0] != "All" ){
    
    //table 
    //no need to add since $ontologies is in term table
    //condition
    $sqlCondition .= " AND " . constructSQLCondition( "term", "term_type", $ontologies );    

  }
   


  if ( $species[0] == "All" ){
    $sqlCondition .= " AND gene_product.species_id IN (183843, 65171, 55496, 93565, 147186, 122968, 37503, 99698, 74685, 80326, 141661, 46840, 4014, 168394, 188897, 167961, 29635, 58866, 201084, 224608, 187829, 102855, 196385, 184244, 138072, 155661, 184320, 204218, 194587, 159620, 185515, 227923, 88935, 134583, 1868, 193395, 48274, 4768, 198597, 32366, 149446, 230763, 120623, 48202, 50668, 185395, 12717, 97048, 128029, 10700, 212040, 7015, 88307, 188793, 157890, 204764, 63549, 217105, 64711, 181728, 80753, 92001, 24102, 18287, 50417, 11586, 61706, 49614, 186394, 196357, 21399, 178999, 47916, 23509, 163332, 226339, 61392, 4158, 20020, 167812, 232338, 108326, 80, 169808, 19829, 228726, 155871, 224623, 222850, 208613, 166754, 218124, 63296, 141450, 160364, 60875, 88497, 155770, 42020, 153074, 118195, 172277, 112303, 158142, 140266, 29487) ";
  }
  else{
    //string and values
    $sqlCondition .= " AND " . constructSQLCondition( "species", "ncbi_taxa_id", $species );    
  }

   
  if ( $dataSources[0] != "All" ){
    //string and values
    $sqlCondition .= " AND " . constructSQLCondition( "db", "name", $dataSources );
    
  }
  
  
  if ( $evidenceCodes[0] != "All" ){         
    //string and values
    $sqlCondition .= " AND " . constructSQLCondition( "evidence", "code", $evidenceCodes );

  }


  $sqlString = $sqlString . $sqlCondition;

  if( $searchLimitation == 0 ){    
    $sqlString .= ";"; 
  }
  else{
    $sqlString .= " LIMIT $searchLimitation;"; 
  }


  return $sqlString;
					 
}







//called by geneTop in function getGenesFromTerms
//here &$species, &$dataSources, &$evidenceCodes are number 2
function filterToGetGenesDetailsSimpleFromIds( $geneIds, &$ontologies, &$species, &$dataSources, &$evidenceCodes, $searchLimitation ){

  $sqlString = "SELECT DISTINCT gene_product.id, gene_product.symbol, gene_product.full_name, species.genus, species.species FROM gene_product, species ";
  $sqlCondition = "WHERE gene_product.species_id = species.id AND gene_product.id IN ( $geneIds );";
  
  $sqlString .= $sqlCondition;

  return $sqlString;
					 
}




//called by geneTop in function getGenesFromTerms
//here &$species, &$dataSources, &$evidenceCodes are number 2
function constructFilterToGetGenesDetails( $geneIds, &$ontologies, &$species, &$dataSources, &$evidenceCodes, $searchLimitation ){

  $sqlString = "SELECT DISTINCT gene_product.id, gene_product.symbol, gene_product.full_name, species.species, db.name, evidence.code FROM gene_product, term, association, species, db, evidence ";

  
  //longest
  //$sqlString = "SELECT DISTINCT gene_product.symbol, gene_product.full_name, species.common_name, db.name, evidence.code FROM gene_product, term, association, species, db, evidence, dbxref, evidence_dbxref ";
  
  
  $sqlCondition = "WHERE gene_product.type_id IN ( SELECT id FROM term WHERE acc = 'gene' ) AND ";
  $sqlCondition .= "association.gene_product_id = gene_product.id AND ";
  //$sqlCondition .= "gene_product.species_id = species.id AND ";
  $sqlCondition .= "association.source_db_id = db.id AND ";

  //shorter way to query evidence
  //$sqlCondition .= "association.id = evidence.association_id AND ";


  //long way
  //first way to query evidence
  //  $sqlCondition .= " ( ( association.id = evidence.association_id ) OR ";
  //query another way
  //$sqlCondition .= " ( gene_product.dbxref_id = dbxref.id AND dbxref.id = evidence_dbxref.dbxref_id AND evidence_dbxref.evidence_id = evidence.id AND evidence.association_id = association.id AND association.gene_product_id = gene_product.id ) ) AND ";

  $sqlCondition .= "gene_product.id IN ( $geneIds ) ";
  
  
  if ( $ontologies[0] != "All" ){
    
    //table 
    //no need to add since $ontologies is in term table
    //condition
    $sqlCondition .= " AND " . constructSQLCondition( "term", "term_type", $ontologies );    

  }
   


  if ( $species[0] == "All" ){
    $sqlCondition .= " AND gene_product.species_id IN (183843, 65171, 55496, 93565, 147186, 122968, 37503, 99698, 74685, 80326, 141661, 46840, 4014, 168394, 188897, 167961, 29635, 58866, 201084, 224608, 187829, 102855, 196385, 184244, 138072, 155661, 184320, 204218, 194587, 159620, 185515, 227923, 88935, 134583, 1868, 193395, 48274, 4768, 198597, 32366, 149446, 230763, 120623, 48202, 50668, 185395, 12717, 97048, 128029, 10700, 212040, 7015, 88307, 188793, 157890, 204764, 63549, 217105, 64711, 181728, 80753, 92001, 24102, 18287, 50417, 11586, 61706, 49614, 186394, 196357, 21399, 178999, 47916, 23509, 163332, 226339, 61392, 4158, 20020, 167812, 232338, 108326, 80, 169808, 19829, 228726, 155871, 224623, 222850, 208613, 166754, 218124, 63296, 141450, 160364, 60875, 88497, 155770, 42020, 153074, 118195, 172277, 112303, 158142, 140266, 29487) ";
  }
  else{
    //string and values
    $sqlCondition .= " AND " . constructSQLCondition( "species", "ncbi_taxa_id", $species );    
  }

   
  if ( $dataSources[0] != "All" ){
    //string and values
    $sqlCondition .= " AND " . constructSQLCondition( "db", "name", $dataSources );
    
  }
  
  
  if ( $evidenceCodes[0] != "All" ){         
    //string and values
    $sqlCondition .= " AND " . constructSQLCondition( "evidence", "code", $evidenceCodes );

  }


  $sqlString = $sqlString . $sqlCondition;

  if( $searchLimitation == 0 ){    
    $sqlString .= ";"; 
  }
  else{
    // $sqlString .= " LIMIT $searchLimitation;"; 
    $sqlString .= " ;"; 

  }


  return $sqlString;
					 
}


//called by geneOne in function 
//here &$species, &$dataSources, &$evidenceCodes are number 2
function constructFilterToGetSpecies( $geneId, &$species ){

  $sqlString = "SELECT DISTINCT species.species FROM gene_product, species ";

  $sqlCondition = "WHERE gene_product.species_id = species.id AND ";
  $sqlCondition .= "gene_product.id = $geneId AND ";
  

  if ( $species[0] == "All" ){
    $sqlCondition .= "gene_product.species_id IN (183843, 65171, 55496, 93565, 147186, 122968, 37503, 99698, 74685, 80326, 141661, 46840, 4014, 168394, 188897, 167961, 29635, 58866, 201084, 224608, 187829, 102855, 196385, 184244, 138072, 155661, 184320, 204218, 194587, 159620, 185515, 227923, 88935, 134583, 1868, 193395, 48274, 4768, 198597, 32366, 149446, 230763, 120623, 48202, 50668, 185395, 12717, 97048, 128029, 10700, 212040, 7015, 88307, 188793, 157890, 204764, 63549, 217105, 64711, 181728, 80753, 92001, 24102, 18287, 50417, 11586, 61706, 49614, 186394, 196357, 21399, 178999, 47916, 23509, 163332, 226339, 61392, 4158, 20020, 167812, 232338, 108326, 80, 169808, 19829, 228726, 155871, 224623, 222850, 208613, 166754, 218124, 63296, 141450, 160364, 60875, 88497, 155770, 42020, 153074, 118195, 172277, 112303, 158142, 140266, 29487)";
  }
  else{
    //string and values
    $sqlCondition .= constructSQLCondition( "species", "ncbi_taxa_id", $species );    
  }

  $sqlString = $sqlString . $sqlCondition . ";";
  return $sqlString;					 
}




//called by geneOne in function 
//here &$species, &$dataSources, &$evidenceCodes are number 2
function constructFilterToGetDataSources( $geneId, &$dataSources ){

  $sqlString = "SELECT DISTINCT db.name FROM association, db ";

  $sqlCondition = "WHERE association.source_db_id = db.id AND ";
  $sqlCondition .= "association.gene_product_id = $geneId ";
  

  if ( $dataSources[0] != "All" ){

    //string and values
    $sqlCondition .= "AND" . constructSQLCondition( "db", "name", $dataSources );    
  }

  $sqlString = $sqlString . $sqlCondition . ";";
  return $sqlString;					 
}



//called by geneOne in function 
//here &$species, &$dataSources, &$evidenceCodes are number 2
function constructFilterToGetEvidenceCodes( $geneId, &$evidenceCodes ){

  $sqlString = "SELECT DISTINCT db.name FROM association, db ";

  $sqlCondition = "WHERE association.source_db_id = db.id AND ";
  $sqlCondition .= "association.gene_product_id = $geneId ";
  

  if ( $evidenceCodes[0] != "All" ){

    //string and values
    $sqlCondition .= "AND" . constructSQLCondition( "evidence", "id", $evidenceCodes );    
  }

  $sqlString = $sqlString . $sqlCondition . ";";
  return $sqlString;					 
}




?>
