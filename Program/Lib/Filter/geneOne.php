<?php

  //filter is a, for example

  //field is some ncbi_taxa_id, for example

  //an example

  //
  //(t.term_type = 'biological_process' OR t.term_type = 'cellular_component' OR t.term_type = 'molecular_function') 

  //AND (s.ncbi_taxa_id = '63363' OR s.ncbi_taxa_id = '2234')
  // AND (d.name = 'CGD' OR d.name = 'DDB' OR d.name = 'FB') 
  //AND (e.code = 'IGI' OR e.code = 'IEP' OR e.code = 'NAS' OR e.code = 'IPI' OR e.code = 'ND')

  //$filter is s, d, e
  //$field is the column name in tables in database
  //$fieldValues from web that the user select

function constructSQLCondition ($filter, $field, &$fieldValues) {
	
  $orCondition = "($filter.$field = '$fieldValues[0]'";

  //echo " In 3  size is $size <p>";

  //count function maybe not correct, use sizeof funtion
  //   $size = count($filterValues);

		
  $size = sizeof ($fieldValues);
  //not from 0
  $index = 1;
	 
  while ($index < $size) {
    $orCondition .= " OR $filter.$field = '$fieldValues[$index]'";
	   
    $index ++;
  }


  $orCondition .= ")";

  return $orCondition;
}





/*input one id
//output related names of ids
//one can get one specie
*/
function filterGetSpecieFromGeneId ($id) {

  $sqlString = "SELECT species.species FROM gene_product JOIN species ";

  $sqlCondition = "WHERE gene_product.species_id = species.id AND ";
  $sqlCondition .= "gene_product.id = $id;";

  $sqlString = $sqlString . $sqlCondition . ";";
  return $sqlString;	
}


/*input one id
//output related names of ids
//one can get one specie
*/
function filterGetGenusSpecieFromGeneId ($id) {

  $sqlString = "SELECT species.genus, species.species FROM gene_product JOIN species ";

  $sqlCondition = "WHERE gene_product.species_id = species.id AND ";
  $sqlCondition .= "gene_product.id = $id;";

  $sqlString = $sqlString . $sqlCondition . ";";
  return $sqlString;	
}







//up to caller to determine what information they need
//called by getTermsFromGene in geneOne.php


//input gene id, sqlString
//input a gene id
//output the related term id
function filterToGetTermsFromGene ($gene, $ontology, &$species, &$dataSources, &$evidenceCodes) {

  $sqlString = "SELECT DISTINCT t.id, t.acc, t.name, d.name, e.code ";
  $sqlString .= "FROM term as t JOIN evidence AS e JOIN association AS a JOIN db AS d INNER JOIN gene_product AS g ";
  $sqlString .= "ON a.gene_product_id = g.id ";
  $sqlString .= "WHERE g.id = $gene ";
  $sqlString .= "AND e.association_id = a.id ";
  $sqlString .= "AND a.source_db_id = d.id ";
  $sqlString .= "AND a.term_id = t.id ";

  //here "All" and "all" are different
  

  $sqlString .= " AND term_type = $ontology";

   
  //here only one spcies
  if ($species[0] != "All") {
    //string and values
    $sqlString .= " AND g.species_id = $species" ;
  }
   
  if ($dataSources[0] != "All") {
    //string and values
    $sqlString .= " AND " . constructSQLCondition("d", "name", $dataSources);
  }


  if ($evidenceCodes[0] != "All") {
    //string and values
    $sqlString .= " AND " . constructSQLCondition("e", "code", $evidenceCodes);

  }

  $sqlString .= ";";

  return $sqlString;
					 
}




////////////////////////////////
//genes -> terms
////////////////////////////////
//0. from gene id to term id
//1. from gene id to term name 
//2. from gene symbol to term id
//3. from gene symbol to term all, 


//0
//input a gene id
//output the related term id
//called by getTermIdsFromGeneId in geneOne.php



/*
 example see geneOne.sql

 1. one $ontology
 2. no species is needed
 3. multiple $dataSources, $evidenceCodes

*/


function filterToGetTermIdsFromGeneId ($gene, $ontology, $dataSources, $evidenceCodes) {

  $sqlString = "SELECT DISTINCT t.id";

  $sqlStringFrom = "\n FROM association AS a";
  $sqlStringFrom .= "\n INNER JOIN gene_product AS g ON a.gene_product_id = g.id";
  $sqlStringFrom .= "\n INNER JOIN term as t ON a.term_id = t.id";

  $sqlStringFrom .= "\n INNER JOIN db AS d ON a.source_db_id = d.id";
  $sqlStringFrom .= "\n INNER JOIN evidence AS e ON a.id = e.association_id";

  //have to be one ontology
  $sqlStringWhere = "\n WHERE g.id = $gene";

  $sqlStringWhere .= "\n AND t.term_type = \"$ontology\"";


  //IEA: Inferred from Electronic Annotation
  /*

  SELECT COUNT(code) FROM evidence WHERE code = 'IEA';

  count(code) |
  +-------------+
  |    18618783 | 
  +-------------+
  1 row in set (1 min 5.43 sec)

  mysql> select count(id) from evidence;                            
  +-----------+
  | count(id) |
  +-----------+
  |  19349780 | 
  +-----------+
  1 row in set (0.00 se

  */


  $sqlStringWhere .= "\n AND e.code <> \"IEA\"";

  
  if ($dataSources[0] != "All") {
    $sqlStringWhere .= "\n AND " . constructSQLCondition("d", "name", $dataSources);
  }


  if ($evidenceCodes[0] != "All") {
    $sqlStringWhere .= "\n AND " . constructSQLCondition("e", "code", $evidenceCodes);
  }


  $sqlString .= $sqlStringFrom . $sqlStringWhere . ";";

  //echo $sqlString;
  return $sqlString;
}






//called by  geneCompareTwo
function filterToGetGeneIdsFromGeneSymbol ($symbol, $ontologies, $species, $dataSources, $evidenceCodes) {


  //DISTINCT have to be used
  $sqlString = "SELECT DISTINCT g.id";


  $sqlStringFrom = "\n FROM association AS a";
  $sqlStringFrom .= "\n INNER JOIN gene_product AS g ON a.gene_product_id = g.id";
  $sqlStringFrom .= "\n INNER JOIN species as s ON g.species_id = s.id";
  $sqlStringFrom .= "\n INNER JOIN db AS d ON a.source_db_id = d.id";
  $sqlStringFrom .= "\n INNER JOIN evidence AS e ON a.id = e.association_id";

  $sqlStringWhere = "\n WHERE g.symbol = \"$symbol\"";
  //$sqlStringWhere .= "\n AND t.acc = 'gene'";


  //this variable maybe useless according to filter
  $sqlCondition = "\n AND ";

  if ($ontologies[0] != "All") {
    
    //table 
    //no need to add since $ontologies is in term table

    //condition
    $sqlCondition .= constructSQLCondition("term", "term_type", $ontologies);
	
	//comment by Lin
	//I think the statement should be
	//$sqlStringWhere .= "\n AND " . constructSQLCondition("term", "term_type", $ontologies);
	//because $sqlCondition seems to be useless in the concatness of $sqlString     
  }
   


  //here using $species or $species[0] need to consider carefully
  if ($species != "All") {

    // print_r($species);
    $sqlStringWhere .= "\n AND s.ncbi_taxa_id = $species";
  }

   
  if ($dataSources[0] != "All") {
    $sqlStringWhere .= "\n AND " . constructSQLCondition("d", "name", $dataSources);    
  }
  
  
  if ($evidenceCodes[0] != "All") {         
    $sqlStringWhere .= "\n AND " . constructSQLCondition("evidence", "code", $evidenceCodes);
  }


  $sqlString .= $sqlStringFrom . $sqlStringWhere . ";";


  return $sqlString;
					 
}




//annotation
//input a gene id and terms which annotate this gene id,

//output the related GO terms SQL

//called by getComparisonTwo for display

function filterToGetAssociation ($gene, &$terms) {

  $termsString  = implode (",", $terms) ;

  $sqlString = "SELECT DISTINCT t.id, d.name, e.code ";
  $sqlString .= " FROM term as t JOIN evidence AS e JOIN association AS a JOIN db AS d INNER JOIN gene_product AS g ";
  $sqlString .= " ON a.gene_product_id = g.id ";
  $sqlString .= " WHERE ";
  $sqlString .= " g.id = $gene ";
  $sqlString .= " AND e.association_id = a.id";
  $sqlString .= " AND a.source_db_id = d.id";
  $sqlString .= " AND a.term_id IN ($termsString); ";
  

  return $sqlString;					 
}




//called by geneTop in function getGenesFromTerms (very top)
//here &$species, &$dataSources, &$evidenceCodes are number 2
//do not change $terms since other function still use it
function filterToGetGeneIdsFromTermIds ($ontology, $species, $dataSources, $evidenceCodes, $terms) {

  //string now
  $terms3 = implode (',', $terms);

  $sqlString = "SELECT DISTINCT gene_product.id as gene FROM gene_product INNER JOIN term INNER JOIN association";
  
 
  //for mmlab
  // $sqlCondition = " WHERE gene_product.type_id = 22720";

  $sqlCondition = " WHERE association.gene_product_id = gene_product.id";
  $sqlCondition .= " AND association.term_id = term.id";
  $sqlCondition .= " AND term.id IN ($terms3)";
  $sqlCondition .= " AND term.term_type = \"$ontology\"";

  

  //first way to query evidence
  //  $sqlCondition .= " ((association.id = evidence.association_id) OR ";
  //query another way
  //$sqlCondition .= " (gene_product.dbxref_id = dbxref.id AND dbxref.id = evidence_dbxref.dbxref_id AND evidence_dbxref.evidence_id = evidence.id AND evidence.association_id = association.id AND association.gene_product_id = gene_product.id)) AND ";
  

/*
  if ($species[0] != "All") {
    $sqlCondition .= " AND gene_product.species_id = $species ";
  }
*/
   
  if ($dataSources[0] != "All") {

    //table 
    $sqlString .= ", db ";
    
    //string and values
    $sqlCondition .= " AND association.source_db_id = db.id";
    $sqlCondition .= " AND " . constructSQLCondition("db", "name", $dataSources);
    
  }
  
  
  if ($evidenceCodes[0] != "All") {         

    //table 
    $sqlString .= ", evidence ";
    
    //string and values
    $sqlCondition .= " AND association.source_db_id = evidence.id";
    $sqlCondition .= " AND " . constructSQLCondition("evidence", "code", $evidenceCodes);
  }


  $sqlString = $sqlString . $sqlCondition;
  $sqlString .= ";"; 

  return $sqlString;					 
}





//called by geneOne in function 
//here &$species, &$dataSources, &$evidenceCodes are number 2
function filterToGetDataSources ($geneId, &$dataSources) {

  $sqlString = "SELECT DISTINCT db.name FROM association JOIN db ";

  $sqlCondition = "WHERE association.source_db_id = db.id AND ";
  $sqlCondition .= "association.gene_product_id = $geneId ";
  

  if ($dataSources[0] != "All") {

    //string and values
    $sqlCondition .= "AND" . constructSQLCondition ("db", "name", $dataSources);    
  }

  $sqlString = $sqlString . $sqlCondition . ";";
  return $sqlString;					 
}




//called by geneOne in function 
//here &$species, &$dataSources, &$evidenceCodes are number 2
function filterToGetEvidenceCodes ($geneId, &$evidenceCodes) {

  $sqlString = "SELECT DISTINCT db.name FROM association JOIN db ";

  $sqlCondition = "WHERE association.source_db_id = db.id AND ";
  $sqlCondition .= "association.gene_product_id = $geneId ";
  

  if ($evidenceCodes[0] != "All") {
    //string and values
    $sqlCondition .= "AND" . constructSQLCondition ("evidence", "id", $evidenceCodes);    
  }

  $sqlString = $sqlString . $sqlCondition . ";";
  return $sqlString;					 
}



?>
