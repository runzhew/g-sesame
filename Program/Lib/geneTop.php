<?php

include_once (dirname(__FILE__) . "/geneCompareTwo.php"); 
include_once (dirname(__FILE__) . "/geneCompareTwoCaches.php"); 

//this correct, but it has to consider who include this file
//include_once ("./Lib/Display/geneTop.php");


include_once ("time.php"); 


/**
//this function is the main function of gene Top
//if there is only one gene id is found for a given gene symbol
//or
//by a web page, geneTop2.php if multiple gene ids are found for a given gene symbol
**/
function searchTopGenesFromId ($geneCore, $searchNumber, $ontology, $species1, $dataSources1, $evidenceCodes1, $species2, $dataSources2, $evidenceCodes2, $isA, $partOf) {


  //session insertion
  $_SESSION['gene1'] = $geneCore;

  //from $gene
  $terms = array ();  
  
  $genesHistory = array ();
  $genesOldBoundary = array ();
  //$genesNewBoundary = array ();

  $termsHistory = array ();
  $termsOldBoundary = array ();
  //$termsNewBoundary = array ();


  //results, using indexed arraies both parallel
  //one more entry as C++ STL does
  $geneTopNs = array ();
  $geneTopNs = array_pad ($geneTopNs, $searchNumber + 1, 0);

  $similarityTopNs = array ();
  $similarityTopNs = array_pad ($similarityTopNs, $searchNumber + 1, 0);
  //  print_r ($geneTopNs);



  $sMax = 0;


  //1D for display

  $symbols = array ();
  $fullNames = array ();

  //2D for display
  $genuses = array ();

  $dataSources = array ();
  $evidenceCodes = array ();

  $numberGenes = 0;


  //caches and statistics
  $termSemanticValuesCache = array ();
  $termSemanticHit = 0;
  $termSemanticMiss = 0;

  $termSemanticSumCache = array ();

  //2D
  $twoGOSimilarityCache = array ();
  $twoGOSimilarityHit = 0;
  $twoGOSimilarityMiss = 0;



  //time
  $time1 = microtime_float ();


  getTermIdsFromGeneId ($geneCore, $ontology, $dataSources1, $evidenceCodes1, $terms);


  $size = sizeof ($terms); 
  if ($size == 0) {
    $symbol = getGeneSymbolFromGeneId ($geneCore);
    //echo __FILE__ . " " . __FUNCTION__ ;
    echo "<p>No GO terms annotate $symbol using the current filter.";

    //no need to continue
    return;
  }

  //print_r ($terms);

  $genesOldBoundary[] = $geneCore;

  $termsHistory = $terms;
  $termsOldBoundary = $termsHistory;


 // echo __FILE__ . " " . __FUNCTION__ ;
//  echo "<p>History is ";
 // print_r ($termsHistory);
  
  $step = 0;
  $flag = 1;

  //here the whole size of array is has $searchNumber + 1 entries, the last one is a dummy one
  //index $searchNumber - 1 is the last valid one
  while ($sMax >= $similarityTopNs[$searchNumber - 1] && $flag == 1) {
    
    /////////////////////////////////////////////////
    //odd step

    if ($step % 2 == 1) {
 
      $termsNewBoundary = array();


      foreach ($termsOldBoundary as $term2){

	$termsNewBoundaryParents2 = array ();
	$termsNewBoundaryChildern2 = array ();

     //Added by Lin
	 //The first parameter should be $term2?
	 //finished
	 
	//check if arrive root or bottoms???
	getParents ($term, $termsNewBoundaryParents2);
	getChildren ($term, $termsNewBoundaryChildern2);
	$termsNewBoundary = array_merge ($termsNewBoundary, $termsNewBoundaryParents2, $termsNewBoundaryChildern2);
      }
      //exit ();

      $termsNewBoundary = array_unique ($termsNewBoundary);
      $termsNewBoundary = array_diff ($termsNewBoundary, $termsHistory, $termsOldBoundary);

      $termsHistory = array_merge ($termsHistory, $termsOldBoundary);      
      $termsOldBoundary = $termsNewBoundary;
      
    }//end of odd steps


    else { //even step
      $genesNewBoundary = array();
      getGeneIdsFromTermIds ($termsOldBoundary, $ontology, $species2, $dataSources2, $evidenceCodes2, $genesNewBoundary);

      // print_r ($genesNewBoundary);

      $genesNewBoundary = array_diff ($genesNewBoundary, $genesHistory, $genesOldBoundary);

      // echo "<P>" . __FILE__ . " " . __FUNCTION__ . " " . __LINE__ . "</P>";


      $numberGenes = sizeof ($genesNewBoundary);
      if ($numberGenes == 0) {
        // echo "<P>" . __FILE__ . " " . __FUNCTION__ . " " . __LINE__ . "</P>";
        echo "<P>No new gene ids found";
        return;
      }

      $genesHistory = array_merge ($genesHistory, $genesOldBoundary);
      // echo "<P>" . __FILE__ . " " . __FUNCTION__ . " " . __LINE__ . "</P>";
      // echo "<p>Size of the history is " . sizeof ($genesHistory);

      if (sizeof ($genesHisotry) > 2000) {
        //echo __FILE__ . " " . __FUNCTION__ . " " . __LINE__;
        //echo "<p>Size of the history is " . sizeof ($geneHistory);
        break;
      }

      $genesOldBoundary = $genesNewBoundary;
      
      $similarities = array ();

      //try to insert into $similaritiesTopNs
      $flag = 0;

      foreach ($genesNewBoundary as $gene2) {

	$terms2 = array ();

	getTermIdsFromGeneId ($gene2, $ontology, $dataSources2, $evidenceCodes2, $terms2);

	// This function is just change the number and the same interface
	//compareTwoGenesFromTerms2 no cache 1,815.529 seconds.
	//compareTwoGenesFromTerms3 one cache, GO cache 56 seconds
	//compareTwoGenesFromTerms4 one cache, TwoGO caches 208 seconds
	//compareTwoGenesFromTerms5 two caches 36 seconds

//        echo "<P>" . __FILE__ . " " . __FUNCTION__ . " " . __LINE__ . "</P>";
  	$similarity = compareTwoGenesFromTerms2 ($terms, $terms2, $isA, $partOf, $termSemanticValuesCache, $termSemanticHit, $termSemanticMiss, $termSemanticSumCache, $twoGOSimilarityCache, $twoGOSimilarityHit, $twoGOSimilarityMiss);
//  	$similarity = 1.0;
	
 //       echo "<P>" . __FILE__ . " " . __FUNCTION__ . " " . __LINE__ . "</P>";
	
	//shift the current found genes, last index is $searchNumber - 1
	//the index here is very subtle, check carefully before change
	$index = $searchNumber - 1;
	while ($index >= 0 && $similarity > $similarityTopNs[$index]) {

	  //parallel
	  $geneTopNs[$index + 1] = $geneTopNs[$index];
	  $similarityTopNs[$index + 1] = $similarityTopNs[$index];
	  
	  $index--;
	}//finish shift
	
	//Now what index points is > = similarity or an invalid one, -1
	//insert if $index < $searchNumber
	if ($index != $searchNumber - 1){
	  $geneTopNs[$index + 1] = $gene2;
	  $similarityTopNs[$index + 1] = $similarity;    	  

	  //ever insert???? useful???
	  $flag = 1;
	}//if      
      }//try to insert

    }//end of even step

    $step ++;
  }//while

  displayGeneTop ($geneCore, $geneTopNs, $similarityTopNs);
  //results
  //print_r ($geneTopNs);
  //print_r ($similarityTopNs);


  $time2 = microtime_float ();
  echo "\n<p>Search time: " . number_format ( ($time2 - $time1), 3) . " seconds.</p>";
  return;
}





/**

//The reason to design this functiion although it is simple is that 
//if there is an exit, there is no need to include ("../XHTML/footer.html") many times.
//the same reason as geneCompareTwo
**/
function searchTopGenesFromSymbol ($symbol, $searchNumber, $ontology, $species1, $dataSources1, $evidenceCodes1, $species2, $dataSources2, $evidenceCodes2, $isA, $partOf) {

  $geneIds = array ();
  
  $number = getGeneIdsFromGeneSymbol ($symbol, $ontology, $species1, $dataSources1, $evidenceCodes1, $geneIds);
  
  if ($number == 0) {
    echo  "No gene ids found from $symbol.";
    return;
  }
  
  if ($number > 1) {
    //this function will output a new web page
    // hyperlinks for seperated gene ids  
    displayGeneIds ($geneIds);
    return;
  }
  

  //symbol to id is 1:1, then going on
  
  $geneId = $geneIds[0];  
  
    //echo  "Begin to search.";
  searchTopGenesFromId ($geneId, $searchNumber, $ontology, $species1, $dataSources1, $evidenceCodes1, $species2, $dataSources2, $evidenceCodes2, $isA, $partOf);
}
?>
