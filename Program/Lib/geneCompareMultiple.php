<?php

include_once (dirname(__FILE__) . "/GOCompareTwoStatistics.php");
include_once (dirname(__FILE__) . "/geneCompareTwo.php");
include_once (dirname(__FILE__) . "/geneCompareTwoStatistics.php");

//input $genes is an indexed array of ids
//output is the associatiative array
//values are saved upper triangle ( above diaginal )

function compareGenesMultiple ($genes, $ontology, 
                               $dataSources, $evidenceCodes, 
                               $isA, $partOf, 
                               &$similarityTable) {

  $size = sizeof ($genes);

  for ($index1 = 0; $index1 < $size - 1; $index1++) {    
    for ($index2 = $index1 + 1; $index2 < $size; $index2++) {

      $gene1 = $genes[$index1];
      $gene2 = $genes[$index2];
      
      
      //dummy one, for each specific pair of genes
      $similarities = array();
      compareTwoGeneIds ($gene1, $gene2, $ontology, 
                         $dataSources, $evidenceCodes, 
                         $dataSources, $evidenceCodes, 
                         $isA, $partOf, 
                         $result, $similarities);
      
      if ($gene1 < $gene2){
	$similarityTable[$gene1][$gene2] = $result; 
      }
      else{
	$similarityTable[$gene2][$gene1] = $result; 
      }

    } //for 
  } //for 

  return 0;
}//end of function 




/*
 -----------------------
 statisticas functions
 -----------------------
 The following functions are related with statistical methods, such as Jiang's, Lin's, and Resnik's

 Option 1: 
 All the functions have same name with "Statistics" to show they have different parameters

 Options 2:
 Using different file, such as geneCompareTwoStatistics.php, with the same function name;

 The problem is there is same function names may cause problems


*/


function compareGenesMultipleStatistics ($genes, $ontology, 
                                         $dataSources, $evidenceCodes, 
                                         $method, &$similarityTable) {

  $size = sizeof ($genes);

  for ($index1 = 0; $index1 < $size - 1; $index1++) {    
    for ($index2 = $index1 + 1; $index2 < $size; $index2++) {

      $gene1 = $genes[$index1];
      $gene2 = $genes[$index2];


      $similarities = array();


      //cache
      $termFrequencies = array();
      $termSemanticHit = 0;
      $termSemanticMiss = 0;
      
      $produceCountAll = getProductCountAll();
      
      
      //same filters, twice
      compareTwoGeneIdsStatistics ($gene1, $gene2, 
                                   $ontology, 
                                   $dataSources, $evidenceCodes, 
                                   $dataSources, $evidenceCodes, 
                                   $termFrequencies, $produceCountAll, $method,
                                   $result, $similarities);
      
      
      if ($gene1 < $gene2){
        $similarityTable[$gene1][$gene2] = $result;
      }
      else{
        $similarityTable[$gene2][$gene1] = $result; 
      }
      
      
    } //for 
  } //for
    
  return 0;
}//end of function 


?>
