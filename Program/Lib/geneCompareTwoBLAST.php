<?php 
include_once ("synonym.php"); 
include_once ("GOCompareTwo.php"); 
include_once ("geneOne.php"); 
include_once ("Display/geneCompareTwo.php"); 
 


function writeSequence( $sequence ){

  $fileName = trim (shell_exec ("mktemp ./Temp/sequence.XXXXXX"));
  $temp = fopen ($fileName, 'w') or die("could not open temporary file $fileName");
  fwrite ($temp, $sequence);
  fclose ($temp);

  return $fileName;
}



/*

gene symbol 104K_THEAN id is 2321477

gene symbol 104K_THEPA id is 1415011


line is 
(
    [0] => 
    [1] => Score
    [2] => =
    [3] => 
    [4] => 912
    [5] => bits
    [6] => (2357),
    [7] => Expect
    [8] => =
    [9] => 0.0

)





*/


function compareTwoGeneIdsBLAST ($id1, $id2) {


  //the second parameter is reserved for future use
  $sequence1 = getSequenceFromGeneId ($id1, 1);
  $sequence2 = getSequenceFromGeneId ($id2, 1);

  if (strlen($sequence1) == 0 || strlen($sequence2) == 0) {
    return 0;
  }


  $symbol1 = getGeneSymbolFromGeneId ($id1);
  $symbol2 = getGeneSymbolFromGeneId ($id2);

  //echo "\n<p> sequence1: $symbol1, $id1 " . $sequence1 . "</p>";
  //echo "\n<p> sequence2: $symbol2, $id2 " . $sequence2 . "</p>";

  $fileName1 = writeSequence ($sequence1);
  $fileName2 = writeSequence ($sequence2);

  $fileNameResult = trim (shell_exec ("mktemp ./Temp/sequenceResult.XXXXXX"));
  //$temp = fopen ($fileNameResult, 'w') or die("could not open temporary file $fileNameResult");



  //here -f F to let BLAST global alignments??

  //gene type_id = 25790
  //$command = "/usr/local/bin/blast/bin/bl2seq -i $fileName1 -j $fileName2 -p blastn -F F -o $fileNameResult";

  //protein type_id = 25794
  $command = "/usr/local/bin/blast/bin/bl2seq -i $fileName1 -j $fileName2 -p blastp -F F -o $fileNameResult";

  //$command = "/usr/local/bin/blast/bin/bl2seq -i $fileName1 -j $fileName2 -p blastp";

  //echo $command;


  passthru ($command);
  //exec($command);
  //shell_exec($command);
  //system($command);


  $fileHandle = fopen ($fileNameResult, "r");

  
  while (!feof ($fileHandle)) {

    $line = fgets ($fileHandle);


    //check different cases

    if (ereg('No hits found', $line)){
      fclose ($fileHandle);
      //      echo "<p>No hits found.</p>";
      return 0;
    }


    //if find the first 'Score', stops

    if (ereg ('Score', $line)) {

      //may use strtok and a loop
      $words = split (' +', $line);

      //explode only accepts string, not pattern
      //$words = explode (' *', $line);


      //print_r ($words);

      fclose ($fileHandle);

      //return bits
      return $words[3];
    }

  }
  

  fclose ($fileHandle);

  echo "<p>The BLAST file is wrong.</p>";
  exit();
  return 0;
}




?>
