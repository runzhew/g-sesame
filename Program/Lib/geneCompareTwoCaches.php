<?php 

include_once( "synonym.php" ); 

include_once( "GOComparisonTwo.php" ); 
include_once( "geneOne.php" ); 


include_once( "Display/geneComparisonTwo.php" ); 
 




//the following functions: compareTwoGenesFromTerms2, 3, 4, 5 are same interface called by geneTop

//This function is called by geneTop without any caches
function compareTwoGenesFromTerms2( &$terms1, &$terms2, $isA, $partOf, &$termSemanticValuesCache, &$termSemanticHit, &$termSemanticMiss, &$termSemanticSumCache, &$twoGOSimilaritiesCache, &$twoGOhit, &$twoGOmiss ){


  $size1 = sizeof( $terms1 );
  $size2 = sizeof( $terms2 );
  
  if ( $size2 == 0 && $display ){
    echo "<p>No GO term is found for gene " . $gene2 . "</p>";   
  }
  
  if( $size1 == 0 || $size2 == 0 ){
    return 0;
  }
  
  
  
  //the following program will calculate two genes according to their terms
  
  //2D
  $GOTermComparisionTable = array();

  //term in term1 with all terms in terms
  //find the similarity of term in term1 with all terms in term2

  $similarities1 = array();

  for( $index1 = 0; $index1 < $size1; $index1++ ){
 
    $term1 = $terms1[$index1];

    $GOTermComparisonOneRow = array();
    
    $similarityMax = 0;

    for( $index2 = 0; $index2 < $size2; $index2++ ){

      $term2 = $terms2[$index2];

      if( $term1 == $term2 ){
	$similarity = 1; 
      }
      else{

	//no caches
	$similarity = compareTwoGOIds( $term1, $term2, $isA, $partOf );
      }
      //save the maximum value
      if ( $similarity > $similarityMax ){

	$similarities1[$index1] = $similarity;
	$similarityMax =  $similarity;

      }

      $GOTermComparisonOneRow[] = $similarity;

    }

    $GOTermComparisonTable[] = $GOTermComparisonOneRow;
  }



  //find the similarity of term in term2 with all terms in term1

  $similarities2 = array();

  for( $index2 = 0; $index2 < $size2; $index2++ ){

    $similarityMax = 0;

    for( $index1 = 0; $index1 < $size1; $index1++ ){
  
      $similarity = $GOTermComparisonTable[$index1][$index2];

      //save the maxium value
      if ( $similarity > $similarityMax ){
	$similarities2[$index2] = $similarity;
	$similarityMax =  $similarity;
      }

    }//for
  }//for


  //do calculation
  $sum1 = array_sum( $similarities1 );
  $sum2 = array_sum( $similarities2 );

  //  print_r( $similarities1 );
  //print_r( $similarities2 );

  $similarityResult =  ( $sum1 + $sum2 )/( $size1 + $size2 );
 
  return $similarityResult;

}






//This function is called by geneTop with only GO cache

function compareTwoGenesFromTerms3( &$terms1, &$terms2, $isA, $partOf, &$termSemanticValuesCache, &$termSemanticHit, &$termSemanticMiss, &$termSemanticSumCache, &$twoGOSimilaritiesCache, &$twoGOhit, &$twoGOmiss ){


  $size1 = sizeof( $terms1 );
  $size2 = sizeof( $terms2 );
  
  if ( $size2 == 0 && $display ){
    echo "<p>No GO term is found for gene " . $gene2 . "</p>";   
  }
  
  if( $size1 == 0 || $size2 == 0 ){
    return 0;
  }
  
  
  
  //the following program will calculate two genes according to their terms
  
  //2D
  $GOTermComparisionTable = array();

  //term in term1 with all terms in terms
  //find the similarity of term in term1 with all terms in term2

  $similarities1 = array();

  for( $index1 = 0; $index1 < $size1; $index1++ ){
 
    $term1 = $terms1[$index1];

    $GOTermComparisonOneRow = array();
    
    $similarityMax = 0;

    for( $index2 = 0; $index2 < $size2; $index2++ ){

      $term2 = $terms2[$index2];


      //look for in cache

      
      $term11 = $term1;
      $term12 = $term2;
      
      //echo "<p>$termIndex";
      
      if( $term1 == $term2 ){
	$similarity = 1; 
      }
      else{
	$similarity = compareTwoGOIds2( $term1, $term2, $isA, $partOf, $termSemanticValuesCache, $termSemanticHit, $termSemanticMiss, $termSemanticSumCache );
	         
      }
      //save the maximum value
      if ( $similarity > $similarityMax ){

	$similarities1[$index1] = $similarity;
	$similarityMax =  $similarity;

      }

      $GOTermComparisonOneRow[] = $similarity;

    }

    $GOTermComparisonTable[] = $GOTermComparisonOneRow;
  }



  //find the similarity of term in term2 with all terms in term1

  $similarities2 = array();

  for( $index2 = 0; $index2 < $size2; $index2++ ){

    $similarityMax = 0;

    for( $index1 = 0; $index1 < $size1; $index1++ ){
  
      $similarity = $GOTermComparisonTable[$index1][$index2];

      //save the maxium value
      if ( $similarity > $similarityMax ){
	$similarities2[$index2] = $similarity;
	$similarityMax =  $similarity;
      }

    }//for
  }//for


  //do calculation
  $sum1 = array_sum( $similarities1 );
  $sum2 = array_sum( $similarities2 );

  //  print_r( $similarities1 );
  //print_r( $similarities2 );

  $similarityResult =  ( $sum1 + $sum2 )/( $size1 + $size2 );

  return $similarityResult;
}






//This function is called by geneTop with one TWOGO cache
function compareTwoGenesFromTerms4( &$terms1, &$terms2, $isA, $partOf, &$termSemanticValuesCache, &$termSemanticHit, &$termSemanticMiss, &$termSemanticSumCache, &$twoGOSimilaritiesCache, &$twoGOhit, &$twoGOmiss ){


  $size1 = sizeof( $terms1 );
  $size2 = sizeof( $terms2 );
  
  if ( $size2 == 0 && $display ){
    echo "<p>No GO term is found for gene " . $gene2 . "</p>";   
  }
  
  if( $size1 == 0 || $size2 == 0 ){
    return 0;
  }
  
  
  
  //the following program will calculate two genes according to their terms
  
  //2D
  $GOTermComparisionTable = array();

  //term in term1 with all terms in terms
  //find the similarity of term in term1 with all terms in term2

  $similarities1 = array();

  for( $index1 = 0; $index1 < $size1; $index1++ ){
 
    $term1 = $terms1[$index1];

    $GOTermComparisonOneRow = array();
    
    $similarityMax = 0;

    for( $index2 = 0; $index2 < $size2; $index2++ ){

      $term2 = $terms2[$index2];


      //look for in cache

      
      $term11 = $term1;
      $term12 = $term2;
      
      //echo "<p>$termIndex";
      
      if( $term1 == $term2 ){
	$similarity = 1; 
      }
      else{
	
	//switch if $term1 > $term2
	if( $term1 > $term2 ) {
	  $term11 = $term2;
	  $term12 = $term1;
	}
	
	
	//if( array_key_exists( $termIndex, $twoGOSimilaritiesCache ) ){
	//if( isset( $twoGOSimilaritiesCache[$termIndex] ) ){
	
	//if( array_key_exists( $term1, $twoGOSimilaritiesCache ) && array_key_exists( $term2, $twoGOSimilaritiesCache[$term1] ) ){
	if( isset( $twoGOSimilaritiesCache[$term11][$term12] ) ){
	  
	  //$similarity = $twoGOSimilaritiesCache[$termIndex];
	  
	  $similarity = $twoGOSimilaritiesCache[$term11][$term12];
	  $twoGOhit ++;
	}
	else{  //calculate

	  $similarity = compareTwoGOIds( $term1, $term2, $isA, $partOf );
	  //$similarity = compareTwoGOIds2( $term1, $term2, $isA, $partOf, $termSemanticValuesCache, $termSemanticHit, $termSemanticMiss, $termSemanticSumCache );
	  
	  //$twoGOSimilaritiesCache[$termIndex] = $similarity;
	  $twoGOSimilaritiesCache[$term11][$term12] = $similarity;

	  $twoGOmiss ++;
	}
         
      }
      //save the maximum value
      if ( $similarity > $similarityMax ){

	$similarities1[$index1] = $similarity;
	$similarityMax =  $similarity;

      }

      $GOTermComparisonOneRow[] = $similarity;

    }

    $GOTermComparisonTable[] = $GOTermComparisonOneRow;
  }



  //find the similarity of term in term2 with all terms in term1

  $similarities2 = array();

  for( $index2 = 0; $index2 < $size2; $index2++ ){

    $similarityMax = 0;

    for( $index1 = 0; $index1 < $size1; $index1++ ){
  
      $similarity = $GOTermComparisonTable[$index1][$index2];

      //save the maxium value
      if ( $similarity > $similarityMax ){
	$similarities2[$index2] = $similarity;
	$similarityMax =  $similarity;
      }

    }//for
  }//for


  //do calculation
  $sum1 = array_sum( $similarities1 );
  $sum2 = array_sum( $similarities2 );

  //  print_r( $similarities1 );
  //print_r( $similarities2 );

  $similarityResult =  ( $sum1 + $sum2 )/( $size1 + $size2 );
 
  return $similarityResult;

}




//This function is called by geneTop with two caches
//input are two term ids arrays related with two gene ids

//$terms1 of gene1 are related with left column, which is given by the caller
//$terms2 are related with top row
//all of them are ids

//$display is a bool variable, if it is true, it is calleb by geneComparisonTwo2, 
//otherwise, it is called by geneComparisonMultiple


//three caches are used and this function consumes one, $twoGOSimilaritiesCache
//if missed, it will call compareTwoGOTerms2( $term1, $term2, $isA, $partOf, $termSemanticValuesCache, $termSemanticHit, $termSemanticMiss, $termSemanticSumCache );
//with two more caches

//changethe first one into terms array ( already unique )





function compareTwoGenesFromTerms5( &$terms1, &$terms2, $isA, $partOf, &$termSemanticValuesCache, &$termSemanticHit, &$termSemanticMiss, &$termSemanticSumCache, &$twoGOSimilaritiesCache, &$twoGOhit, &$twoGOmiss ){


  $size1 = sizeof( $terms1 );
  $size2 = sizeof( $terms2 );
  
  if ( $size2 == 0 && $display ){
    echo "<p>No GO term is found for gene " . $gene2 . "</p>";   
  }
  
  if( $size1 == 0 || $size2 == 0 ){
    return 0;
  }
  
  
  
  //the following program will calculate two genes according to their terms
  
  //2D
  $GOTermComparisionTable = array();

  //term in term1 with all terms in terms
  //find the similarity of term in term1 with all terms in term2

  $similarities1 = array();

  for( $index1 = 0; $index1 < $size1; $index1++ ){
 
    $term1 = $terms1[$index1];

    $GOTermComparisonOneRow = array();
    
    $similarityMax = 0;

    for( $index2 = 0; $index2 < $size2; $index2++ ){

      $term2 = $terms2[$index2];


      //look for in cache

      
      $term11 = $term1;
      $term12 = $term2;
      
      //echo "<p>$termIndex";
      
      if( $term1 == $term2 ){
	$similarity = 1; 
      }
      else{
	
	//switch if $term1 > $term2
	if( $term1 > $term2 ) {
	  $term11 = $term2;
	  $term12 = $term1;
	}
	
	
	//if( array_key_exists( $termIndex, $twoGOSimilaritiesCache ) ){
	//if( isset( $twoGOSimilaritiesCache[$termIndex] ) ){
	
	//if( array_key_exists( $term1, $twoGOSimilaritiesCache ) && array_key_exists( $term2, $twoGOSimilaritiesCache[$term1] ) ){
	if( isset( $twoGOSimilaritiesCache[$term11][$term12] ) ){
	  
	  //$similarity = $twoGOSimilaritiesCache[$termIndex];
	  
	  $similarity = $twoGOSimilaritiesCache[$term11][$term12];
	  $twoGOhit ++;
	}
	else{  //calculate
	  
	  $similarity = compareTwoGOIds2( $term1, $term2, $isA, $partOf, $termSemanticValuesCache, $termSemanticHit, $termSemanticMiss, $termSemanticSumCache );
	  
	  //$twoGOSimilaritiesCache[$termIndex] = $similarity;
	  $twoGOSimilaritiesCache[$term11][$term12] = $similarity;

	  $twoGOmiss ++;
	}
         
      }
      //save the maximum value
      if ( $similarity > $similarityMax ){

	$similarities1[$index1] = $similarity;
	$similarityMax =  $similarity;

      }

      $GOTermComparisonOneRow[] = $similarity;

    }

    $GOTermComparisonTable[] = $GOTermComparisonOneRow;
  }



  //find the similarity of term in term2 with all terms in term1

  $similarities2 = array();

  for( $index2 = 0; $index2 < $size2; $index2++ ){

    $similarityMax = 0;

    for( $index1 = 0; $index1 < $size1; $index1++ ){
  
      $similarity = $GOTermComparisonTable[$index1][$index2];

      //save the maxium value
      if ( $similarity > $similarityMax ){
	$similarities2[$index2] = $similarity;
	$similarityMax =  $similarity;
      }

    }//for
  }//for


  //do calculation
  $sum1 = array_sum( $similarities1 );
  $sum2 = array_sum( $similarities2 );

  //  print_r( $similarities1 );
  //print_r( $similarities2 );

  $similarityResult =  ( $sum1 + $sum2 )/( $size1 + $size2 );

  return $similarityResult;
}



?>
