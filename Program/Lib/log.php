<?php
  //Name:
  //Project: Gene Comparison
  //Date: 6/23/07
  //This file contain all of the functions for logs
  //remember to change ./Log by
  //chmod -R 777 Log

  //$program
  //$emailAddress cannot be the reference
  //since they are strings (constant)
function getClientInformation ($program, $emailAddress) {

 

  //  $fileName = dirname(__FILE__) "./Log/log.txt";

  //Here we use month at the unit
  //if log file is much huge based on month, use date ("Y.m.d") instead
  $fileName = getcwd () . "/Log/log." . date ("Y.m") . ".txt";


  $fileHandle = fopen ($fileName, "a");

  if (!$fileHandle) { 
    echo "Cannot open file $fileName."; 
  }
 
  //REMOTE_ADDR
  //The IP address of the remote host making the request.
  $IP = getenv ("REMOTE_ADDR");

  //echo $IP;
  $host = gethostbyaddr ($IP);

  //echo $host;
  $log = "\n" . date ("m/d/Y H:i:s");
  $log .= "\t" . $IP;
  $log .= "\t" . $host;
  $log .= "\t" . $program;
  $log .= "\t" . $emailAddress;
  
  
  // echo $log;
  fwrite ($fileHandle, $log);
  
  fclose ($fileHandle);
  
} 
?>

