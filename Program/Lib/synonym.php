
<?php
  
  //This function replace the synonyms into main table name
  //return a replaced synonym, 

  //if the same, not found 
  //one by one

function searchOneSynonym ($synonymOrUnknown, &$newSymbol) {
  
  $numberSynonym = 0;
  $numberUnknown = 0;
 
 
  //for the SQL
  //'' and "" work
  //`` does not work
	
  $sql = "SELECT gene_product.symbol ";
  $sql .= "FROM gene_product, gene_product_synonym ";
  $sql .= "WHERE gene_product.id = gene_product_synonym.gene_product_id ";
  $sql .= "AND gene_product_synonym.product_synonym ='$synonymOrUnknown';";
  
  //echo" synonym from: $sql ";
				
  $result = mysql_query ($sql);
	
  if ($oneRow = mysql_fetch_array($result)) {
    
    $newSymbol = $oneRow['symbol'];
    
    //echo "Find one $tempSymbol <br>" ;
  }
  else {   
    echo "\n<p>No synonym found for $synonymOrUnknown.</p>";
    $newSymbol = $synonymOrUnknown;  
  }
  
  return array ("0", $synonymOrUnknown);
}





//call by clusterGenes()

//This function replace the synonyms 
//return an array of three arrays, one for replaced synonym, one is not found
//$synonymOrUnknown is an array
/*
for the input gene symbols
case 1: it exists in database

case 2: it exists in database as a synonym
original symbol is to display

synonym is to compare
$genesComparison is id => symbol or synonym (both form users' input), id for comparison

$genesUnknown
case 3: it does not exist in the database, just the symbols


Input: symbols
Output: 
*/


function replaceSynonyms (&$genes, &$ontologies, &$species, &$dataSources, &$evidenceCodes, &$genesComparison, &$genesUnknown) {

  foreach ($genes as $symbol) {


    //temp array to hold possible gene ids
    $geneIds1 = array();
    
    $numberIds = getGeneIdsFromGeneSymbol($symbol, $ontologies, $species, $dataSources, $evidenceCodes, $geneIds1);
    
    
    //only the first one ????????????
	//The default is the first ID
    if ($numberIds >= 1) {
      $genesComparison[$geneIds1[0]] = $symbol;
      continue;
    }
    
    //if not found, search from synonym table    
    $sqlString = "SELECT gene_product.id ";
    $sqlString .= "FROM gene_product, gene_product_synonym ";
    $sqlString .= "WHERE gene_product.id = gene_product_synonym.gene_product_id ";
    $sqlString .= "AND gene_product_synonym.product_synonym ='$symbol';";
    
    $result = mysql_query($sqlString);	

    //if in main gene_product table
    if (list($id) = mysql_fetch_array ($result)) {
      $genesComparison[$id] = $symbol;
    }
    else{
      //echo "<br>Not find one: " . $token;
      $genesUnknown[] = $symbol;
    }
  }//for		 
  return;
}	 

?>
