<?php 
include_once ("synonym.php"); 
include_once ("GOCompareMultiple.php"); 
include_once ("geneOne.php"); 
include_once ("Display/geneCompareTwo.php"); 

/*
//contents
//compare two groups of terms -> compare two ids -> compare two symbols
//called by   geneTop            geneCompareMultiple       geneCompareTwo



//input are two terms arrays related with two gene ids

//if the caller needs to know the details of $GOTermComparisonTable, call this function
//otherwise, call compareTwoGeneIds, which will provide a dummy $GOTermComparisonTable
//to call this function

//$terms1 of gene1 are related with left column, which is given by the caller  
//$similaritiesMaxRow is 1D array related all rows

//$terms2 are related with top row
//$similaritiesMaxColumn is 1D array related all columns

//function return integer which is about the status
//which is the same as exit() in C
//if 0, success
//otherwise, error.





//this function is called 
//by geneCompareTwo3.php or
//by compareTwoGeneSymbols
//transfer id and symbol is to prevent synonyms


//this function will output tables (in display.php) and a hyperlink
//for the hyperlink, it only need send gene2 id since gene1 id has sent by session
//This is not consistant but to match geneTop.
//geneTop has source gene id, id1, and many other id2s.


//output:
//0.34

//Conveat: $dataSources and $evidenceCodes maybe used partially since there maybe not exist terms with some of them 

There is no need to check if sizeof(terms1) == 0
When changing from symbol to id, it has check implicitely
*/
function compareTwoGeneIds ($id1, $id2, $ontology, 
                            $dataSources1, $evidenceCodes1, 
                            $dataSources2, $evidenceCodes2, 
                            $isA, $partOf, 
                            &$result, &$termComparisonTable) {

  $terms1 = array();
  getTermIdsFromGeneId ($id1, $ontology, $dataSources1, $evidenceCodes1, $terms1);


  $terms2 = array();
  getTermIdsFromGeneId ($id2, $ontology, $dataSources2, $evidenceCodes2, $terms2);


  //no caches

  //return the similarity
  //$status = compareTwoGeneIdsFromTermIds ($terms1, $terms2, $isA, $partOf, $result, $termComparisonTable);

  $status = compareGOMultiple ($terms1, $terms2, 
                               $isA, $partOf, 
                               $result, $termComparisonTable);

  return $status;
}



//This is the main function of two gene symbols compare

function compareTwoGeneSymbols ($symbol1, $symbol2, $ontology, 
				$species1, $dataSources1, $evidenceCodes1, 
				$species2, $dataSources2, $evidenceCodes2, 
				$isA, $partOf, &$result) {


  /*
  echo "<br><br>Printed from /var/www/html/G-SESAME/Program/Lib/geneCompareTwo.php</br>";
  echo "<br>_SERVER['DOCUMENT_ROOT'] is: " . $_SERVER['DOCUMENT_ROOT'];
  echo "<br> getcwd  is: " . getcwd();
  echo "<br> dirname(__FILE__) is: " . dirname(__FILE__);
  */

  $geneIds1 = array ();
  $number1 = getGeneIdsFromGeneSymbol ($symbol1, $ontology, $species1, $dataSources1, $evidenceCodes1, $geneIds1);
    
  //can be merged into getGeneIdsFromGeneSymbol

  if ($number1 == 0) {

    searchOneSynonym ($symbol1, $synonym1);
        
    if ($symbol1 != $synonym1) {
      echo "\n<p>A synonym of gene " . $symbol1 . " is found: " . $synonym1 . ".</p>"; 
      $number1 = getGeneIdsFromGeneSymbol ($synonym1, $ontology, 
					   $species1, $dataSources1, $evidenceCodes1, $geneIds1);

      if ($number1 == 0) { 
        echo "\n<p>No gene is found for gene synonym: " . $symbol1;
        return;
      }
    }
    else {
      echo "\n<p>No gene is found for gene: " . $symbol1 . " based on the current filter.</p>";      
      return;
    }
  }
  
  echo "\n<p>Gene id found for gene number: " . $number1 . " based on the current filter.</p>";      

  $geneIds2 = array ();
  $number2 = getGeneIdsFromGeneSymbol ($symbol2, $ontology, 
				       $species2, $dataSources2, $evidenceCodes2, $geneIds2);

  if ($number2 == 0) {
       
    searchOneSynonym ($symbol2, $synonym2);
    
    if ($symbol2 != $synonym2) {
      echo "\n<p>A synonym of gene " . $symbol2 . " is found: " . $synonym2 . ".</p>"; 
      $number2 = getGeneIdsFromGeneSymbol ($synonym2, $ontology, $species2, $dataSources2, $evidenceCodes2, $geneIds2);

      if ($number2 == 0) { 
        echo "\n<p>No gene is found for gene synonym: " . $symbol2. ".</p>";
        return;
      }
    }
    else {
      echo "\n<p>No gene is found for gene: " . $symbol2 . " based on the current filter.</p>";      
      return;
    }
  }

  if ($number1 > 1 || $number2 > 1) {    
    //output geneComparisionTwo2.php with geneComparisionTwo3.php hyperlinks
    displayIdsFromTwoGeneSymbols ($geneIds1, $geneIds2);
    return;
  }



  //the following codes are the same as geneCompareTwo3.php
  $termComparisonTable = array();
 
  //no need species since gene id can defer to it
  compareTwoGeneIds ($geneIds1[0], $geneIds2[0], $ontology, $dataSources1, $evidenceCodes1, $dataSources2, $evidenceCodes2, $isA, $partOf, $result, $termComparisonTable);
  

  //the following is to display

  //options 1: using inputs geneid + user's input (evidence codes and data source) 
  //to getTermAll, including valid evidence codes and data source


  //options 2: using inputs geneid + terms to get evidence codes and data sources
  //better, since no need distinguish the ontology now
  //getTermsAllFromGeneId 

 
  //print_r ($result);
  //print_r ($termComparisonTable);

  displayGeneCompareTwo ($geneIds1[0], $geneIds2[0], $result, $termComparisonTable);
   
  return 0;
}


?>
