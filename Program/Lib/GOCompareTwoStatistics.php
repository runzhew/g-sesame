<?php

//Name:
//Project: Gene Ontology Comparison
//Date: 8/11/07
//This file contain all of the function which use for GOSearch.php page. 
//Creating the functions seperate from the actual site helps with the organization of the 
//source code.



/*
 The following functions are related with statistical methods, such as Jiang's, Lin's, and Resnik's
 
 
 In the future, some functions should be put into GOOne.php
*/

include_once ("GOOne.php");

function getProductCountAll() {
  //get all product_count
  $sqlString = "SELECT SUM(product_count)";
  /* $sqlString .= " FROM gene_product_count INNER JOIN term ON (term.id = gene_product_count.term_id) where term.term_type = '$ontology'"; */
  $sqlString .= "FROM gene_product_count";

  $result = mysql_query ($sqlString);

  if (!$result) {
    echo "\n<p>Error performing query from getting gene_product_count All" . mysql_error() . "</p>";
    exit();
  }

  $row = mysql_fetch_array ($result);

  return $row[0];
}




function getProductCountOntology($ontology) {
  //get all product_count
  $sqlString = "SELECT SUM(product_count)";
  $sqlString .= " FROM gene_product_count INNER JOIN term ON (term.id = gene_product_count.term_id) where term.term_type = '$ontology'";

  $result = mysql_query ($sqlString);

  if (!$result) {
    echo "\n<p>Error performing query from getting gene_product_count All" . mysql_error() . "</p>";
    exit();
  }

  $row = mysql_fetch_array ($result);

  return $row[0];
}



  /*
   SELECT SUM(product_count)
   FROM gene_product_count
   WHERE term_id = 19130;

   Here there is no space between SUM and (product_count)


   This function called by getFrequency() twice,
   a term itself have no much meaning as it maybe 0,
   but all its childern may have many annotations
  */



function getProductCount ($id) {
  //get all product_count
  $sqlString = "SELECT SUM(product_count)";
  $sqlString .= " FROM gene_product_count";
  $sqlString .= " WHERE term_id = $id;";


  //echo "\n<br>" . $sqlString;

  $result = mysql_query ($sqlString);

  if (!$result) {
    echo "\n<p>Error performing query from getting gene_product_count " . mysql_error() . "</p>";
    exit();
  }

  $row = mysql_fetch_array ($result);

  return $row[0];
}


function getProductCount_pre ($id) {
  //get all product_count
  $sqlString = "SELECT count";
  $sqlString .= " FROM pre_count";
  $sqlString .= " WHERE term_id = $id;";


  //echo "\n<br>" . $sqlString;

  $result = mysql_query ($sqlString);

  if (!$result) {
    echo "\n<p>Error performing query from getting pre_count " . mysql_error() . "</p>";
    exit();
  }

  $row = mysql_fetch_array ($result);

  return $row[0];
}





function getFrequency ($id) {

  $sum = getProductCount_pre ($id);

  $children = array();
  getDescendants ( $id, $children );

  foreach ( $children as $child ) {
    $sum += getProductCount_pre ($child);
  }

  return $sum;
}


function getFrequency_pre ($id) {
  //get all product_count
  $sqlString = "SELECT count";
  $sqlString .= " FROM pre_frequency";
  $sqlString .= " WHERE term_id = $id;";


  //echo "\n<br>" . $sqlString;

  $result = mysql_query ($sqlString);

  if (!$result) {
    echo "\n<p>Error performing query from getting pre_frequency " . mysql_error() . "</p>";
    exit();
  }

  $row = mysql_fetch_array ($result);

  return $row[0];
}




//lowest common ancestor
//Finding the shared parent of two nodes
//see example from http://amigo.geneontology.org/dev/sql/doc/example-queries.html


/*
 SELECT tp.id, tp.acc, p1.distance + p2.distance AS total_distance,                   
 p1.distance AS d1,   
 p2.distance AS d2                 
 FROM term AS t1                   
 INNER JOIN graph_path AS p1 ON (t1.id=p1.term2_id)   
 INNER JOIN term AS tp       ON (p1.term1_id=tp.id)                  
 INNER JOIN graph_path AS p2 ON (tp.id=p2.term1_id)                 
 INNER JOIN term AS t2       ON (t2.id=p2.term2_id)      
 WHERE  t1.id = 3907 AND t2.id = 3942  ORDER BY total_distance;
   
 +-------+------------+----------------+----+----+
 | id    | acc        | total_distance | d1 | d2 |
 +-------+------------+----------------+----+----+
 | 18390 | GO:0043231 |              3 |  1 |  2 | 
 | 19130 | GO:0044444 |              3 |  1 |  2 | 
 | 18388 | GO:0043229 |              5 |  2 |  3 | 
 | 18386 | GO:0043227 |              5 |  2 |  3 | 
 | 19110 | GO:0044424 |              5 |  2 |  3 | 
 |  3906 | GO:0005737 |              5 |  2 |  3 | 
 | 19110 | GO:0044424 |              6 |  3 |  3 | 
 | 19110 | GO:0044424 |              6 |  2 |  4 | 
 | 19149 | GO:0044464 |              7 |  3 |  4 | 
 |  3803 | GO:0005622 |              7 |  3 |  4 | 
 | 18385 | GO:0043226 |              7 |  3 |  4 | 
 | 19110 | GO:0044424 |              7 |  3 |  4 | 
 |  3803 | GO:0005622 |              8 |  3 |  5 | 
 |  3803 | GO:0005622 |              8 |  4 |  4 | 
 | 19149 | GO:0044464 |              8 |  4 |  4 | 
 | 19149 | GO:0044464 |              8 |  3 |  5 | 
 | 19149 | GO:0044464 |              9 |  4 |  5 | 
 |  3803 | GO:0005622 |              9 |  4 |  5 | 
 |  3804 | GO:0005623 |              9 |  4 |  5 | 
 | 19149 | GO:0044464 |              9 |  5 |  4 | 
 ... 
 +-------+------------+----------------+----+----+
 64 rows in set (0.00 sec)
*/


function getFrequencyLCA ($id1, $id2, &$termFrequencies) {


  $sqlString = "SELECT tp.id,  p1.distance + p2.distance AS total_distance, ";   
  $sqlString .= " p1.distance AS d1, "; 
  $sqlString .= " p2.distance AS d2 ";
  $sqlString .= " FROM term AS t1 ";
  $sqlString .= " INNER JOIN graph_path AS p1 ON (t1.id=p1.term2_id) ";
  $sqlString .= " INNER JOIN term AS tp       ON (p1.term1_id=tp.id) ";  
  $sqlString .= " INNER JOIN graph_path AS p2 ON (tp.id=p2.term1_id) ";  
  $sqlString .= " INNER JOIN term AS t2       ON (t2.id=p2.term2_id) "; 
  $sqlString .= " WHERE t1.id = $id1 AND t2.id = $id2 ORDER BY total_distance;";


  //echo $sqlString;
  //exit;

  // writing the query to select the term
  $result = mysql_query ($sqlString);

  if (!$result) {
    echo "\n<p>No result or error of performing query from getting LCA " . mysql_error() ."</p>";
    exit();
  }



  //fine the maximum one  
  $row = mysql_fetch_array ($result);
  $id = $row["id"];
  
  //the first row is the LCA
  $distanceShortest= $row["total_distance"];


  if (array_key_exists($id, $termFrequencies)) {
    $frequency = $termFrequencies[$id];      
  }
  else{
    $frequency = getFrequency_pre ($id);
    //buffered
    $termFrequencies[$id] = $frequency;
  }

  $frequencyMax = $frequency;

  //echo "<br>total Distance----------";

  //look for the max $geneFrequencyMax
  //if first condition satisfied, then check the $row
    
  while (list ($idTemp, $distanceTemp, $distance1, $distance2) = mysql_fetch_array ($result)) {
    

    //there are maybe multiple LCA (by distance), as the information contents increasemonotically,
    //the parents of LCA will not considered 

    if ($distanceTemp > $distanceShortest){
      break;
    }
    
    if (array_key_exists($idTemp, $termFrequencies)) {
      $frequency = $termFrequencies[$idTemp];      
    }
    else{
      $frequency = getFrequency_pre ($idTemp);
      //buffered
      $termFrequencies[$idTemp] = $frequency;
    }

    //maybe need updated according to $geneFrequency
    if ($frequencyMax < $frequency) {
      $id = $idTemp;
      $frequencyMax = $frequency;
    }
  }


  //id is no need to be returned
  return $frequencyMax;
}




//This is the main function of Two GO Terms similarity
//no use of  $isA, $partOf
function compareTwoGOIdsJiang ($id1, $id2, &$termFrequencies, $produceCountAll) {

  //echo $produceCountAll;


  if (array_key_exists($id1, $termFrequencies)) {
    $p1 = $termFrequencies[$id1] / $produceCountAll;
  }
  else{
    $frequency =getFrequency_pre ($id1);

    //buffer
    $termFrequencies[$id1] = $frequency;
    $p1 = $frequency / $produceCountAll;
  }

  if (array_key_exists($id2, $termFrequencies)) {
    $p2 = $termFrequencies[$id2] / $produceCountAll;
  }
  else{
    $frequency = getFrequency_pre ($id2);
    $termFrequencies[$id2] = $frequency;
    $p2 = $frequency / $produceCountAll;
  }



  //should be cached also
  $pLCA = getFrequencyLCA ($id1, $id2, $termFrequencies) / $produceCountAll;


  //echo $pLCA;
  //echo "<br>" . $p1;
  //echo "<br>" . $p2;


  $sum = log ($p1, 10) + log ($p2, 10);

  if ($sum == 0) {
    return 0;
  }

  $similarity = 1 / (1 + 2 * log ($pLCA, 10) - $sum);

  return $similarity;
}



//This is the main function of Two GO Terms similarity
//no use of  $isA, $partOf
function compareTwoGOIdsLin ($id1, $id2, &$termFrequencies, $produceCountAll) {


  if (array_key_exists($id1, $termFrequencies)) {
    $p1 = $termFrequencies[$id1] / $produceCountAll;
  }
  else{
    $frequency = getFrequency_pre ($id1);

    //buffer
    $termFrequencies[$id1] = $frequency;
    $p1 = $frequency / $produceCountAll;
  }

  if (array_key_exists($id2, $termFrequencies)) {
    $p2 = $termFrequencies[$id2] / $produceCountAll;
  }
  else{
    $frequency = getFrequency_pre ($id2);
    $termFrequencies[$id2] = $frequency;
    $p2 = $frequency / $produceCountAll;
  }



  //should be cached also
  $pLCA = getFrequencyLCA ($id1, $id2, $termFrequencies) / $produceCountAll;

 
  $sum = log ($p1, 10) + log ($p2, 10);

  if ($sum == 0) {
    return 0;
  }

  return 2 * log ($pLCA, 10) / $sum;
}



//This is the main function of Two GO Terms similarity
//no use of  $isA, $partOf
function compareTwoGOIdsResnik ($id1, $id2, &$termFrequencies, $produceCountAll) {
  $pLCA = getFrequencyLCA ($id1, $id2, $termFrequencies) / $produceCountAll;
  return -log ($pLCA, 10);
}










?>

