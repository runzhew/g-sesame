<?php

  //Date: 8/11/08


  //random walk methods

  //no use of  $isA, $partOf
function chooseOneParentRandomly ($id) {
  
  $parents = getAllParents ($id);


  $size = sizeof ($parents);

  if ($size == 0) {
    echo "\nNo parent exist.";
    exit();
  }

  //here rand is the number that is inclusive,
  $randomIndex = rand (0, $size - 1);

  $parent = $parents[$randomIndex];

  return $parent;
}




//get all nodes from id itself to the root, the chain.


function getAncestorChainRandomly ($id) {

  $ancestors = array();
  $ancestors[] = $id;

  $parent = chooseOneParentRandomly ($id);

  //1 is the all root
  while ($parent != 1) {

    $ancestors[] = $parent;
    $son = $parent;
    $parent = chooseOneParentRandomly ($son);
  }

  //root
  $ancestors[] = 1;

  return $ancestors;
}




//for this double loop, it's easy to use return 
//instead of continue coding after double for loop
function getRendezvous ($ancestors1, $ancestors2) {

  $pairIndexes = array();
  for ($index1 = 0; $index1 < sizeof ($ancestors1); $index1++) {

    for ($index2 = 0; $index2 < sizeof ($ancestors2); $index2++) {

      if ($ancestors1[$index1] == $ancestors2[$index2]) {
	$pairIndexes[] = $index1;
	$pairIndexes[] = $index2;

	return $pairIndexes;
      }

    }
  }//outter for
}



//main function
function compareTwoGOIdsRandomWalk ($id1, $id2, $isA, $partOf) {

  //initializing 
  $distance = 0;
  $height = 0;

  for ($index = 0; $index < 5; $index++) {


    //one time
    $ancestors1 = getAncestorChainRandomly ($id1);
    $ancestors2 = getAncestorChainRandomly ($id2);

    //print_r ($ancestors1);
    //print_r ($ancestors2);


    //$indexes is a 2 entries array 
    $indexes = getRendezvous ($ancestors1, $ancestors2);


    //  print_r ($indexes);


    //heights related with first one
    //sizeof ($ancestors1) is the number of nodes, distance + 1

    $height1 = sizeof ($ancestors1) - $indexes[0] - 1;
    $height2 = sizeof ($ancestors2) - $indexes[1] - 1;

 

    //accumulating them
    $distance += $indexes[0] + $indexes[1];
    $height += ($height1 + $height2 ) /2;
  }

  $distance = $distance / $index;
  $height = $height / $index;

  /*
   if ($distance == 0) {
   return 1;
   }

  


   if ($height == 0) {
   return 0;
   }
  */
  
  //initial model
  //return 1 / $distance;
  
  //after considering distance to the root
  //using average of two heights

  //echo "Distance is " . $distance;
  //echo "<p>";
  //echo $height;

  //  return  $height / ($distance + 1);

  //same notation as in the dissertation
  //a normorization function
  $rou = 0.8;

  //  echo "Pow is " . pow ($rou, $distance);


  //model 1, the most simple one
  //it is sensitive to the $rou

  //this model is good, not it does not reflect the further two from the root,
  //the differnece between two term is less
  //  return pow ($rou, $distance);

  //model 2:

  //This model is also good enough.  
  //It impacts the value dramatically, so the step should be set to 0.001
  //return pow ($rou, $distance) / $height;


  //model 3:then it is not sensitive to the $rou
  return pow ($rou, $distance / $height);
  
  //model 4: if the height impacts too much, use minus instead
  //return pow ($rou, $distance - $height);
  
}

?>

