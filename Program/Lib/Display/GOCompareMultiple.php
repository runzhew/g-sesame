<?php


  //include_once( "displayGOComparisionTwo.php" );
  //include_once ($_SERVER['DOCUMENT_ROOT'] . "/G-SESAME/Program/Lib/GOOne.php"); 
include_once (dirname(__FILE__) . "/../GOOne.php"); 
include_once (dirname(__FILE__) . "/display.php"); 


/*
Precondition, size1 and size2 > 0
All GO term ids are valid in the database
//called by GOMultipleComparison

*/

function displayGOCompareMultiple ($similarityTable, $flag, $fileHandle, $result) {


  $ids1 = array_keys ($similarityTable);

  $ids2 = array_keys ($similarityTable[$ids1[0]]);
  $size2 = sizeof ($ids2);


  //print_r ($ids1);
  //print_r ($ids2);
  

  outputScreenOrFile ($flag, $fileHandle, "<h2>Semantic similarity of multiple GO term sets results</h2>");
   outputScreenOrFile ($flag, $fileHandle, "<div class=\"emphasis1\">Semantic similarity of two GO term sets is: <div class=\"emphasis2\">" . number_format ($result, 3) . "</div></div>");
  outputScreenOrFile ($flag, $fileHandle, "<h3> Similarity Table</h3>");
  outputScreenOrFile ($flag, $fileHandle, "\n<table border=1 bordercolor=black >");

  //first line
  outputScreenOrFile ($flag, $fileHandle, "\n<tr BGcolor = #FF9900><td> &nbsp; </td>");

  for( $index2 = 0; $index2 < $size2; $index2++) {    

    $term = getTermAcc ($ids2[$index2]);
    outputScreenOrFile ($flag, $fileHandle, "<th>$term</th>");
  }

  outputScreenOrFile ($flag, $fileHandle, "</tr>");

  foreach ($similarityTable as $id1 => $ids1) {

    //first column
    $term = getTermAcc ($id1);
    outputScreenOrFile ($flag, $fileHandle, "<tr><th BGcolor = #FF9900>" . $term . "</th>");

    foreach( $ids1 as $id2 => $similarity ) {
      //the last number control the width of the numerical
      outputScreenOrFile ($flag, $fileHandle, "<td center>" . substr ($similarity, 0, 5 ) . "</td>");
    }

    outputScreenOrFile ($flag, $fileHandle, " </tr>");
  }

  outputScreenOrFile ($flag, $fileHandle, "</table>");
  return;
  
}





?>
