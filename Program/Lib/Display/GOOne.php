<?php
 

include_once( "displayGOOne" );
include_once( "displayGOComparisionTwo" );


//called by GOMultipleComparison
function printSimilarityTable( $fileHandle, $genesDisplay1, $genesDisplay2, $similarityTable ){

  $size1  = sizeof( $genesDisplay1 );
  $size2  = sizeof( $genesDisplay2 );

  fwrite( $fileHandle, "<h4> Similarity Table</h4>" );

  fwrite( $fileHandle, "<table border=1 bordercolor=black >" );

  //first line
  fwrite( $fileHandle, "<tr BGcolor = #FF9900><td> &nbsp; </td>" );

  for( $index1 = 0; $index1 < $size2; $index1++ ){    
    fwrite( $fileHandle, "<th>$genesDisplay2[$index1]</th>" );
  }

  fwrite( $fileHandle, "</tr>" );

  for( $index1 = 0; $index1 < $size1; $index1++ ){

    //first column
    fwrite( $fileHandle, "<tr><th BGcolor = #FF9900>" . $genesDisplay1[$index1] . "</th>" );

    for( $index2 = 0; $index2 < $size2; $index2++ ){    


      //the last number control the width of the numerical
      fwrite( $fileHandle, "<td center>" . substr( $similarityTable[$index1][$index2], 0, 5 ) . "</td>" );
      
    }

    fwrite( $fileHandle, " </tr>" );
  }

  fwrite( $fileHandle, "</table>" );
  return;
  
}


?>
