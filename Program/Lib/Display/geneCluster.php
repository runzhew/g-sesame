<?php

include_once("display.php"); 


//0 is false, display online
//1 is true, send via email


//two rows for the biggest table

//$genes is id => symbol or synonym (from user's input)
function displayClusterTree (&$genes, &$thresholds, &$clusterTree, $flag, $fileHandle) {

  outputScreenOrFile ($flag, $fileHandle, "\n<h3>Gene Clustering Result</h3>");
  
  //print out the table 
  //table 0

  outputScreenOrFile ($flag, $fileHandle, "\n<table border=1 bordercolor=black>");

  $size0 = sizeof ($clusterTree);

  //threshold
  outputScreenOrFile ($flag, $fileHandle, "\n<tr>");
  outputScreenOrFile ($flag, $fileHandle,  "\n<th>Threshold</th>");

  outputScreenOrFile ($flag, $fileHandle, "<th>");
  outputScreenOrFile ($flag, $fileHandle, "Initial");
  outputScreenOrFile ($flag, $fileHandle, "</th>");
  
  
  foreach ($thresholds as $threshold) {
    outputScreenOrFile ($flag, $fileHandle, "<th>");
    outputScreenOrFile ($flag, $fileHandle, number_format ($threshold, 3));
    outputScreenOrFile ($flag, $fileHandle, "</th>");
  }

  outputScreenOrFile ($flag, $fileHandle, "\n</tr>");


  outputScreenOrFile ($flag, $fileHandle, "\n<tr>");
  outputScreenOrFile ($flag, $fileHandle, "\n<td><b>Clustering <br /> Result </b></td>");


  for ($level = 0; $level < $size0; $level++) {

    //one entry
    //each entry is one table
    outputScreenOrFile ($flag, $fileHandle, "<td>");

    foreach ($clusterTree[$level] as $oneLeaf) {
      
      $size2 = sizeof ($oneLeaf);

      outputScreenOrFile ($flag, $fileHandle, "\n<table border=1 bgcolor = #FF9900 > <tr><td>");

      for ($row2 = 0; $row2 < $size2; $row2++) {
   
	$temp5 = $oneLeaf[$row2];
		
	outputScreenOrFile ($flag, $fileHandle, " $genes[$temp5]");
	outputScreenOrFile ($flag, $fileHandle, "<br />");

      }//for 2
  
      //table1
      outputScreenOrFile ($flag, $fileHandle, "</td></tr></table>");
      outputScreenOrFile ($flag, $fileHandle, "<br />");

    }//for 1



    //end of one entry
    outputScreenOrFile ($flag, $fileHandle, "</td>");

  }//for 0
  
  outputScreenOrFile ($flag, $fileHandle, "</tr>");
  
  //table0
  outputScreenOrFile ($flag, $fileHandle, "\n</table>");

}


?>
