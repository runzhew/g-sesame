<?php
include_once( "geneOne.php" );

////////////////////////////////
//Top gene functions
////////////////////////////////
function displayFilterOntology( &$ontologies ){

  foreach( $ontologies as $ontology ){
    echo $ontology . " ";
  }
  
}




////////////////////////////////
//geneTop
////////////////////////////////
//this function is called by gentTop2.php if there are more then one gene ids are found


function displayGenesIds( $geneIds ){

  echo "<ul> <font color='#ff3300'>
       <li>The input gene symbol correspnonds to the following genes in different species.</li>
        <li> To continue the search, Please click on the intended source gene.
        </font>
      </ul>";


  echo "<table border=1 width=\"800\">";

  echo "<tr>";
  echo "<th width=\"50\">Index</th><th width=\"100\">Symbol</th><th width=\"350\">Full Name</th><th width=\"150\">Data Source</th><th width=\"250\">Species</th>";
  echo "</tr>";

  $index = 1;
  foreach( $geneIds as $id ){

    $sqlString = "SELECT symbol, dbxref_id, species_id, full_name FROM gene_product WHERE id = $id;";
    //echo $sqlString;

    $result = mysql_query( $sqlString );

    list( $symbol, $dbxrefId, $sepciesId, $fullName ) = mysql_fetch_array( $result );
    
    
    $sqlString = "SELECT xref_dbname FROM dbxref WHERE id = $dbxrefId;";
    $result = mysql_query( $sqlString );
    list( $dbname ) = mysql_fetch_array( $result );
    
    $sqlString = "SELECT genus, species FROM species WHERE id = $sepciesId;";
    $result = mysql_query( $sqlString );
    list( $genus, $specie ) = mysql_fetch_array( $result );
    
    echo "\n<tr>";
    echo "<td>". $index . "</td>";
    echo "<td><a href=geneTop3.php?gene1=$id target=_blank>" . $symbol . "</a></td>";
    echo "<td>". $fullName . "</td>";
    echo "<td>" . $dbname . "</td>";
    echo "<td>" . $genus . " " . $specie . "</td>";

    echo "</tr>";

    $index ++;
  }

  echo "</table>";
  
}



//input  genecore, topNs, similarities


function displayGeneTop( $gene1, &$geneTopNs, &$similarityTopNs ){

  $size = sizeof( $similarityTopNs ) - 1;
  echo "<h3>Top $size similar genes:</h3>( <b>Note:</b> Click on a gene symbol to display its annotation information. Click on the similarity value to visualize the annotation similarity/difference between this gene and the source gene.) <br/><br/>";
  
  echo "<table border=\"1\">";
  
  echo "<tr>";   
  echo "<th>";
  echo "Rank";
  echo "</th>";

  echo "<th>";
  echo "Gene";
  echo "</th>";

  /*
   echo "<th>";
   echo "Full Name";
   echo "</th>";

   echo "<th>";
   echo "Genus Species";
   echo "</th>";
  */
      
  echo "<th>";
  echo "Similarity";
  echo "</th></tr>";



  for( $index = 0; $index < $size; $index ++ ){

    //id
    $gene = $geneTopNs[$index];
    $similarity = $similarityTopNs[$index];

    echo "<tr>";
    echo "<td>";
    echo $index + 1;
    echo "</td>\n";
    
    echo "<td> <a href='geneOne2.php?gene2=$gene' target='_blank'>";
    $symbol = getGeneSymbolFromGeneId ($gene);
    echo $symbol;
    echo "</a></td>\n";

    /*
     echo "<td> ";
     if( sizeof( $fullNames[$gene]) == 0 || $fullNames[$gene] == ""{
     echo "None";
     }
     else{
     echo ( $fullNames[$gene] );
     }
     echo "</td>\n";


     echo "<td> ";
     if( sizeof( $genuses[$gene]) == 0 || $fullNames[$gene] == ""{
     echo "None";
     }
     else{
     echo $genuses[$gene] . $species[$gene];
     }
     echo "</td>\n";
    */

    echo "<td> <a href='geneCompareTwo3.php?id1=$gene1&id2=$gene' target='_blank'>";
    echo ( number_format( $similarity, 3 ) );
    echo "</a></td>\n";
    echo "</tr>\n";
     
  }
  echo "</table>";

}
  
?>
