<?php

include_once ("display.php"); 

//called by geneClustering  


/*
Upper triangle

Array
(
    [82793] => Array
        (
            [82806] => 0.96236073471846
            [82812] => 0.96236073471846
            [82818] => 0.96236073471846
            [83804] => 0.89367073069478
            [82822] => 0.61644021270961
       )

    [82806] => Array
        (
            [82812] => 1
            [82818] => 1
            [83804] => 0.93130999597632
            [82822] => 0.57628535177967
       )

    [82812] => Array
        (
            [82818] => 1
            [83804] => 0.93130999597632
            [82822] => 0.57628535177967
       )

    [82818] => Array
        (
            [83804] => 0.93130999597632
            [82822] => 0.57628535177967
       )

    [82822] => Array
        (
            [83804] => 0.56653530052331
       )

)


*/



function displayGeneMultipleSimilarities ($flag, $fileHandle, $similarityTable) {


  //check the data structure above to know the reason
  //print_r( $similarityTable);

  $genes1 = array_keys ($similarityTable);
  $genes2 = array_keys ($similarityTable[ $genes1[0]]);
  $genes3 = array_merge ($genes1, $genes2);
  $genes = array_unique ($genes3);

  //echo "Here ";
  //print_r($genes1);
  //print_r($genes2);
  //print_r($genes3);

  outputScreenOrFile ($flag, $fileHandle, "<h3> Similarity Table</h3>");
  
  outputScreenOrFile ($flag, $fileHandle, "\n<table border = \"1\">");
  
  //first line
  outputScreenOrFile ($flag, $fileHandle, "\n<tr center BGcolor = #FF9900> <td> &nbsp; </td>");


  //first line  
  foreach ($genes as $index1 => $id) {    
    $symbol = getGeneSymbolFromGeneId ($id); 
    outputScreenOrFile ($flag, $fileHandle, "<td><b> $symbol </b></td>");
  }
  outputScreenOrFile ($flag, $fileHandle, "</tr>");


  foreach ($genes as $index1 => $id1) {
    $symbol1 = getGeneSymbolFromGeneId($id1);
    

    //first column
    outputScreenOrFile ($flag, $fileHandle, "\n<tr><td BGcolor = #FF9900><b>" . $symbol1 . "</b></td>");
    

    foreach($genes as $index2 => $id2){

      $symbol2 = getGeneSymbolFromGeneId($id2);


      //have to use index to show upper triangle      
      if ($index1 < $index2) {
	if ($id1 < $id2) {
	  outputScreenOrFile ($flag, $fileHandle, "<td>" . substr($similarityTable[$id1][$id2], 0, 5) . "</td>");
	}
	else {
	  outputScreenOrFile ($flag, $fileHandle, "<td>" . substr($similarityTable[$id2][$id1], 0, 5) . "</td>");
	}
      }
      else {
	outputScreenOrFile($flag, $fileHandle, "<td> &nbsp; </td>");
      }	
    } 
    outputScreenOrFile ($flag, $fileHandle, " </tr>");
      
  }

  outputScreenOrFile ($flag, $fileHandle, "</table>");
  return;

}

?>
