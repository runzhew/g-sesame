<?php

include_once (getcwd() . "/Lib/association.php");
include_once (getcwd() . "/Lib/species.php");
include("../tpl/head.html");
include("../tpl/header.html");
include("../tpl/sidebar.html");

/*
//thmml
echo "<br>_SERVER['DOCUMENT_ROOT'] is: " . $_SERVER['DOCUMENT_ROOT'];

//Program
echo "<br> getcwd  is: " . getcwd();


//current file
echo "<br> dirname(__FILE__) is: " . dirname(__FILE__);

*/


  //one ontology
  //refer to html result for detail data structure

function displayOneGO ($geneId, &$terms) {
     
  $dataSources = array();
  $evidenceCodes = array();
  
  //getAssociation($geneId, $terms, $dataSources, $evidenceCodes);
  
 


  echo "\n<table border=\"1\">";
  
  echo "\n<tr> <th>GO</th> <th>Name</th> <th>Data Source</th><th>Evidence Code</th> </tr>";
 
  foreach ($terms as $id) {
    
    echo "\n<tr>";

    echo "<td>";
    echo getTermAcc ($id);
    echo "</td>\n";

    echo "<td> ";
    echo getTermName ($id);
    echo "</td>\n";
    
    echo "<td>";


    $result = getDBNameFromTermId ($id, $dataSources);

    if (!$result) {
      echo "&nbsp; ";
    }
    else{

      list($dataSource) = mysql_fetch_array($result);
      echo $dataSource;

      while(list($dataSource) = mysql_fetch_array($result)) {
	echo ", " . $dataSource;
      }    

    }
    echo "</td>\n";
    


    echo "<td>";   

    $result = getEvidenceCodeFromTermId($id, $evidenceCodes);
    if(!$result) {
      echo "&nbsp; ";
    }
    else{
      list($evidenceCode) = mysql_fetch_array($result);
      echo $evidenceCode;
      
      while(list($evidenceCode) = mysql_fetch_array($result)) {
	echo ", " . $evidenceCode;
      }    
    }
    echo "</td>\n";
    echo "</tr>";
  }
  
  echo "</table>";
}





//the table contains [id1][id2] -> similarity

function displaySimilarityTable ($similarityTable) {

  /*
  echo "<br><br>Printed from /var/www/html/G-SESAME/Program/Lib/Display/geneCompareTwo.php</br>";
  echo "<br>_SERVER['DOCUMENT_ROOT'] is: " . $_SERVER['DOCUMENT_ROOT'];
  echo "<br> getcwd  is: " . getcwd();
  echo "<br> dirname(__FILE__) is: " . dirname(__FILE__);
  */

  //ids
  $terms1 = array();
  $terms2 = array();

  $terms1 = array_keys ($similarityTable);

  //any one can be used to obtain $terms2
  list ($term1, $row) = each ($similarityTable);
  $terms2 = array_keys ($row);
  
  

  echo "\n<h3> Similarities of the associated GO terms:</h3>";

  echo "<table border=1 bordercolor=black >";

  //first line
  echo "<tr BGcolor = \"#FF9900\"><td> &nbsp; </td>";
  foreach ($terms2 as $id2) {          
    $term = getTermAcc ($id2);
    echo "<th>$term</th>";    
  }
  echo "</tr>";
  

  
  foreach ($terms1 as $id1) {

    //first column 

    $term = getTermAcc ($id1);
    echo "\n<tr><th BGcolor = \"#FF9900\">" . $term . "</th>";
    
    foreach ($terms2 as $id2) {    
      
      //the last number control the width of the numerical
      echo "<td>" . substr($similarityTable[$id1][$id2], 0, 5) . "</td>";
      
    }

    echo "</tr>";
  }

  echo "</table>";
  return;  
}


/*
 From these arguments, can get everything

*/


function displayGeneCompareTwo ($geneId1, $geneId2, $result, $TermComparisonTable) {

  $symbol1 = getGeneSymbolFromGeneId ($geneId1);
  $symbol2 = getGeneSymbolFromGeneId ($geneId2);


  echo "<h2>Functional similarity of two genes</h2>"; 
  echo "<p><b>Semantic similarity between " . $symbol1 . 
    " and " . $symbol2 . " is <div class=\"emphasis2\">" . number_format ($result, 3) . "</div></b></p>"  ;
  //We assume if the value is too small, then there is no need to display then.  
  //As only dispaly 3 digits above, here we use 3 zeros.
  if ($result <= 0.0009) {
    return;
  }

  $terms1 = array();
  $terms2 = array();

  echo "<h3>Associated GO term information:</h3>";

  echo "\n<table>";

  echo "\n<tr>";
  echo "<th>";
  echo $symbol1;
  echo " from ";
  echo getGenusSpeciesFromGeneId ($geneId1);
  echo "</th>"; 

  echo "<th>";
  echo $symbol2;
  echo " from ";
  echo getGenusSpeciesFromGeneId ($geneId2);
  echo "</th>"; 
  echo "</tr>";

  
  //details, time consuming

  $terms1 = array_keys ($TermComparisonTable);
  //any one can be used to obtain $terms2
  $term1 = $terms1[0];
  $terms2 = array_keys ($TermComparisonTable[$term1]);


  echo "\n<tr valign=top>";
  echo "<td>";
  displayOneGO ($geneId1, $terms1);
  echo "</td>";
  
  echo "<td>";
  displayOneGO ($geneId2, $terms2);
  echo "</td>";
  echo "</tr>"; 



  echo "\n<tr>";
  
  echo "<td>";
  displaySimilarityTable ($TermComparisonTable);
  echo "</td>";
  
  
  
  
  echo "</tr>"; 

  echo "</table>";

  echo "\n<h3>Visualization of the annotation information:</h3>";
  echo "<ul>";


  echo "\n<li><div class=\"colorCyan\"><b>Cyan nodes</b></div>&nbsp are GO terms annotating the gene $symbol1.</li>\n";
  echo "\n<li><div class=\"colorOrange\"><b>Orange nodes</b></div>&nbsp are GO terms annotating the gene $symbol2.</li>\n";
  echo "\n<li><div class=\"colorGrey\"><b>Grey nodes</b></div>&nbsp are GO terms annotating both genes.</li>\n";
  echo "</ul>";
  
  echo "<p>Click the following icon to view a larger annotation graph.</p>\n";
  getDotDAGFromTwoTerms ($terms1, $terms2, $imageFileName); 
  
  echo "<table>";
  echo "<tr>";
  echo "<td>";
  //generate dot files    
  
  //print_r($terms1);
  //print_r($terms2);
  
  
  
  echo "\n<a href=\"$imageFileName\" target=\"_blank\" />";
  echo "<img src=\"$imageFileName\" height=\"150\", width=\"150\" />";
  echo "</a>";    
  echo "</td>";
  echo "</tr>";
  echo "</table>";
  
}






//the following functions are related with geneCompareTwo3.php

//input are ids and name for radio name (HTML)

//output radio buttons 
//output geneComparisionTwo2.php with geneComparisionTwo3.php radio buttons

function displayIdsRadio ($ids, $name) {


  echo "<table border=\"1\">";
  echo "\n<tr>";
  echo "<th>Index</th><th>Symbol</th><th>Full Name</th><th>Data Source</th><th>Species</th>";
  echo "</tr>";


  $index = 1;
  foreach ($ids as $id) {
    
    $sqlString = "SELECT symbol, species_id, dbxref_id, full_name FROM gene_product WHERE id = $id;";
    //echo $sqlString;
    
    $result = mysql_query ($sqlString);
    
    list ($symbol, $sepciesId, $dbxrefId, $fullName) = mysql_fetch_array ($result);
    

    $genus = getGenus ($sepciesId);
    $specie = getSpecies ($sepciesId);
    $dbname = getDatabaseName ($dbxrefId);

    
    echo "\n<tr>";
    echo "<td>". $index . "</td>";

    if ($index == 1) {
      echo "<td><input type=radio value=$id name=\"$name\" checked>" . $symbol . "</a></td>";
    }
    else{
      echo "<td><input type=radio value=$id name=\"$name\">" . $symbol . "</a></td>";
    }

    echo "<td>". $fullName . "</td>";
    echo "<td>" . $dbname . "</td>";
    echo "<td>" . $genus . " " . $specie . "</td>";

    echo "</tr>";

    $index ++;
  }

  echo "\n</table>";
}



//output radio buttons 
//output geneComparisionTwo2.php with geneComparisionTwo3.php radio buttons

function displayIdsFromTwoGeneSymbols ($ids1, $ids2) {

  echo "<div class=\"col-sm-9 col-md-9 section\" role=\"main\" id=\"video_section\">";
  echo "\n<FORM name='form' method='get' action='geneCompareTwo3.php'>";

  echo "\n<ul>";
  echo "\n <li>The input gene symbol correspnonds to the following genes in different species.</li>";
  echo "\n <li>Please select one of the gene id from each to compare.</li>";
  echo "\n</ul>";

  echo "<p>The firstsymbol:</p>";
  displayIdsRadio ($ids1, "id1");
  echo "<p>The second symbol:</p>";
  displayIdsRadio ($ids2, "id2");

  echo "\n<input name='Submit' value='submit' type='submit'> <input name='reset' type='reset'>";
  echo "\n</form>";
  echo "<\div>";

}

?>
