<?php
function displayTermsFromGene( &$accs, &$names, &$dataSources, &$evidenceCodes ){

  echo "<table border=\"1\">" ;
  echo "<tr><th>Index</th> <th>GO</th> <th>Name</th> <th>Source</th> <th>Evidence</th> </tr>";
 
  $index = 1;

  foreach ( $accs as $id => $acc ){
  
    echo "<tr>";

    echo "<td>";
    echo $index;
    echo "</td>\n";
    
    echo "<td>";
    echo $acc;
    echo "</td>\n";

    echo "<td> ";
    echo $names[$id];
    echo "</td>\n";
    

    echo "<td>";
    reset( $dataSources[$id] );

    list( $id2, $dataSource ) = each( $dataSources[$id] );
    echo $dataSource;
    
    while( list( $id2, $dataSource ) = each( $dataSources[$id] ) ){
      echo ", " . $dataSource;
    }
    echo "</td>\n";
    
    
    echo "<td>";
    reset( $evidenceCodes[$id] );
    
    list( $id2, $evidenceCode ) = each( $evidenceCodes[$id] );
    echo $evidenceCode;
    
    while( list( $id2, $evidenceCode ) = each( $evidenceCodes[$id] ) ){
      echo ", " . $evidenceCode;
    }
    echo "</td>\n";
    
     
    $index ++;
  }
  
  echo "</table>";


  return;
}



?>
