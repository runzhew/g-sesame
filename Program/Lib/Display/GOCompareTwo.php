<?php
  /*
   Sequence of the functions

   1. GOOne
   2. GOComparisionTwo
   3. geneOne
   4. geneComparisionTwo
   5. geneComparisonMultiple
   6. geneClustering
   7. geneTop
  */


include_once( "displayGOOne" );
include_once( "displayGOComparisionTwo" );

include_once( "displaygeneOne" );
include_once( "displaygeneComparisionTwo" );
include_once( "displaygeneComparisonMultiple" );

include_once( "displaygeneClustering" );

include_once( "displaygeneTop" );



function printHtmlEnd(){
  echo "\n</body>\n</html>";
}




//call by geneClustering
function display1DArray( $fileHandle, $array ){

  $size = sizeof( $array );
  
  for( $row = 0; $row < $size; $row++ ){
    $temp4 = $array[$row];
    // echo "<br> $temp4 &nbsp;";
    fwrite( $fileHandle, "$temp4 &nbsp;" );

    //echo "$temp4 &nbsp;";
  }
}




function display2DArray( $array ){
  
  echo"<br />  ";

  $sizeRow = sizeof( $array );

  for( $row = 0; $row < $sizeRow; $row++ ){

    $sizeColumn = sizeof( $array[$row] );
  
    for( $column = 0; $column < $sizeColumn; $column++ ){
      $temp4 = $array[$row][$column];
      //      $temp4 += 1;
      echo " $temp4 &nbsp;" ;
    }

    echo "<br>";    
  }
}



//called by geneComparisonMultiple or geneClustering  

function displaySimilarityTable( $fileHandle, $genesDisplay, $similarityTable ){

  $size  = sizeof( $genesDisplay );

  fwrite( $fileHandle, "<h4> Similarity Table</h4>" );

  fwrite( $fileHandle, "\n<table border=1 bordercolor=black >" );

  //first line
  fwrite( $fileHandle, "\n<tr center BGcolor = #FF9900><td> &nbsp; </td>" );

  for( $index1 = 0; $index1 < $size; $index1++ ){    
    fwrite( $fileHandle, "<td><b> $genesDisplay[$index1] </b></td>" );
  }

  fwrite( $fileHandle, "</tr>" );

  for( $index1 = 0; $index1 < $size; $index1++ ){

    //first column
    fwrite( $fileHandle, "\n<tr><td BGcolor = #FF9900><b>" . $genesDisplay[$index1] . "</b></td>" );

    for( $index2 = 0; $index2 < $size; $index2++ ){    

      if ( $index2 <= $index1 ){
	fwrite( $fileHandle, "<td> &nbsp; </td>" );
      }
      else{
	//the last number control the width of the numerical
	fwrite( $fileHandle, "<td center>" . substr( $similarityTable[$index1][$index2], 0, 5 ) . "</td>" );

	//or use, where 1 is 1.000
	//fwrite( $fileHandle,  "<td center>" . number_format( $similarityTable[$index1][$index2], 3 ) . "</td>";


      }

    }

    fwrite( $fileHandle, " </tr>" );
  }

  fwrite( $fileHandle, "</table>" );
  return;
  
}



//call by geneclustering
//input is 3D

//two rows for the biggest table
function displayClusteringTable( $fileHandle, $genes, $thresholdForDisplay, $groupForDisplay ){

  fwrite( $fileHandle, "<h4> Gene Clustering Result</h4>" );
  
  //print out the table 
  //table 0

  fwrite( $fileHandle, "\n<table border=1 bordercolor=black>" );

  $size0 = sizeof( $groupForDisplay );

  //threshold
  fwrite( $fileHandle, "\n<tr>" );
  fwrite( $fileHandle,  "\n<th>Threshold </th>" );




  fwrite( $fileHandle, "<th>"  );
  fwrite( $fileHandle, "Initial" );
  fwrite( $fileHandle, "</th>" );
  for( $row0 = 1; $row0 < $size0; $row0++ ){
    fwrite( $fileHandle, "<th>"  );
    fwrite( $fileHandle, "$thresholdForDisplay[$row0]" );
    fwrite( $fileHandle, "</th>" );
  }

  fwrite( $fileHandle, "\n</tr>");


  fwrite( $fileHandle, "\n<tr>" );
  fwrite( $fileHandle, "\n<td><b>Clustering <br /> Result </b></td> ");

  for( $row0 = 0; $row0 < $size0; $row0++ ){

    //one entry
    //each entry is one table
    fwrite( $fileHandle, "<td>" );

    $size1 = sizeof( $groupForDisplay[$row0] );
    for( $row1 = 0; $row1 < $size1; $row1++ ){
      
      $size2 = sizeof( $groupForDisplay[$row0][$row1] );

      //fwrite( $fileHandle,  "\n<td BGcolor = #FF9900>" ;
      //table1
      //fwrite( $fileHandle,  "\n<table border=1 bordercolor=black BGcolor = #FF9900 CELLPADDING = 1 CELLSPACING = 1> <tr><td>" ;

      fwrite( $fileHandle, "\n<table border=1 bgcolor = #FF9900 > <tr><td>" );

      for( $row2 = 0; $row2 < $size2; $row2++ ){
   
	$temp5 = $groupForDisplay[$row0][$row1][$row2];

	
	fwrite( $fileHandle, " $genes[$temp5]" );
	fwrite( $fileHandle, "<br />" );



      }//for 2
  
      //table1
      fwrite( $fileHandle, "</td></tr></table>" );
      fwrite( $fileHandle, "<br />" );

    }//for 1



    //end of one entry
    fwrite( $fileHandle, "</td>" );

  }//for 0
  
  fwrite( $fileHandle, "</tr>" );
  
  //table0
  fwrite( $fileHandle, "\n</table>" );

}





//called by GOMultipleComparison
function printSimilarityTable( $fileHandle, $genesDisplay1, $genesDisplay2, $similarityTable ){

  $size1  = sizeof( $genesDisplay1 );
  $size2  = sizeof( $genesDisplay2 );

  fwrite( $fileHandle, "<h4> Similarity Table</h4>" );

  fwrite( $fileHandle, "<table border=1 bordercolor=black >" );

  //first line
  fwrite( $fileHandle, "<tr BGcolor = #FF9900><td> &nbsp; </td>" );

  for( $index1 = 0; $index1 < $size2; $index1++ ){    
    fwrite( $fileHandle, "<th>$genesDisplay2[$index1]</th>" );
  }

  fwrite( $fileHandle, "</tr>" );

  for( $index1 = 0; $index1 < $size1; $index1++ ){

    //first column
    fwrite( $fileHandle, "<tr><th BGcolor = #FF9900>" . $genesDisplay1[$index1] . "</th>" );

    for( $index2 = 0; $index2 < $size2; $index2++ ){    


      //the last number control the width of the numerical
      fwrite( $fileHandle, "<td center>" . substr( $similarityTable[$index1][$index2], 0, 5 ) . "</td>" );
      
    }

    fwrite( $fileHandle, " </tr>" );
  }

  fwrite( $fileHandle, "</table>" );
  return;
  
}


function outputTermsFromGene( &$accs, &$names, &$species, &$dataSources, &$evidenceCodes ){

  echo "<table border=1 bordercolor=black >" ;
  echo "<tr><th>Index</th> <th>GO</th> <th>Name</th> <th>Species</th> <th>Source</th> <th>Evidence</th> </tr>";
 
  $index = 1;

  foreach ( $accs as $id => $acc ){
  
    echo "<tr>";

    echo "<td>";
    echo $index;
    echo "</td>\n";
    
    echo "<td>";
    echo $acc;
    echo "</td>\n";

    echo "<td> ";
    echo $names[$id];
    echo "</td>\n";
    

    echo "<td>";
    reset( $species[$id] );

    list( $id2, $specie ) = each( $species[$id] );
    echo $specie;
    
    while( list( $id2, $specie ) = each( $species[$id] ) ){
      echo ", " . $specie;
    }
    echo "</td>\n";



    echo "<td>";
    reset( $dataSources[$id] );

    list( $id2, $dataSource ) = each( $dataSources[$id] );
    echo $dataSource;
    
    while( list( $id2, $dataSource ) = each( $dataSources[$id] ) ){
      echo ", " . $dataSource;
    }
    echo "</td>\n";
    
    
    echo "<td>";
    reset( $evidenceCodes[$id] );
    
    list( $id2, $evidenceCode ) = each( $evidenceCodes[$id] );
    echo $evidenceCode;
    
    while( list( $id2, $evidenceCode ) = each( $evidenceCodes[$id] ) ){
      echo ", " . $evidenceCode;
    }
    echo "</td>\n";
    
     
    $index ++;
  }
  
  echo "</table>";


  return;
}

?>
