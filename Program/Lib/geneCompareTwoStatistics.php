<?php 
include_once (dirname(__FILE__) . "/synonym.php"); 
include_once (dirname(__FILE__) . "/GOCompareTwo.php"); 
include_once (dirname(__FILE__) . "/geneOne.php"); 
include_once (dirname(__FILE__) . "/Display/geneCompareTwo.php"); 
 

/*
 -----------------------
 statisticas functions
 -----------------------
 The following functions are related with statistical methods, such as Jiang's, Lin's, and Resnik's

 Option 1: 
 All the functions have same name with "Statistics" to show they have different parameters

 Options 2:
 Using different files, such as geneCompareTwoStatistics.php, with the same function name;

 The problem is there is same function names may cause problems

 Currently, both options are used


//this function is called 
//by geneCompareTwo3.php or
//by compareTwoGeneSymbols
//transfer id and symbol is to prevent synonyms


//this function will output tables (in display.php) and a hyperlink
//for the hyperlink, it only need send gene2 id since gene1 id has sent by session
//This is not consistant but to match geneTop.
//geneTop has source gene id, id1, and many other id2s.


//output:
//BP, CC ro MF of $similarities = 0.34 for example

//Conveat: $dataSources and $evidenceCodes maybe used partially since there maybe not exist terms with some of them 

//using cache


There is no need to check if sizeof(terms1) == 0
When changing from symbol to id, it has check implicitely

*/

function compareTwoGeneIdsStatistics ($id1, $id2, $ontology, 
                                      $dataSources1, $evidenceCodes1, 
                                      $dataSources2, $evidenceCodes2, 
                                      $termFrequencies, $produceCountAll, $method, 
                                      &$result, &$termComparisonTable) {


  //print_r ($ontologies);

  //echo "Another: ";
  //print_r ($dataSources1);

  //echo $method;


  $terms1 = array();
  getTermIdsFromGeneId ($id1, $ontology, $dataSources1, $evidenceCodes1, $terms1);

  /*
  if (sizeof($terms1) == 0){

    $symbol1 = getGeneSymbolFromGeneId ($id1);
    echo "<p>No result for $symbol1</p>";

    return 1;
  }
  */

  $terms2 = array();
  getTermIdsFromGeneId ($id2, $ontology, $dataSources2, $evidenceCodes2, $terms2);


  //no caches
  //2D
  //return the similarity
  $status = compareGOMultipleStatistics ($terms1, $terms2, 
                                         $termFrequencies, $produceCountAll, $method, 
                                         $result, $termComparisonTable);


  //print_r ($similarities);
  return $status;
}






//This is the main function of two gene symbols compare
//This function can only be called on time by geneComparetwo2.php
//so, it need to be pulled up in the future

function compareTwoGeneSymbolsStatistics ($symbol1, $symbol2, $ontologies, 
	$species1, $dataSources1, $evidenceCodes1, 
	$species2, $dataSources2, $evidenceCodes2, $method) {

  $geneIds1 = array();
  $number1 = getGeneIdsFromGeneSymbol ($symbol1, $ontologies, $species1, $dataSources1, $evidenceCodes1, $geneIds1);
    
  //can be merged into getGeneIdsFromGeneSymbol

  if ($number1 == 0) {

    searchOneSynonym ($symbol1, $synonym1);
        
    if ($symbol1 != $synonym1) {
      echo "\n<p>A synonym of gene " . $symbol1 . " is found: " . $synonym1 . ".</p>"; 
      $number1 = getGeneIdsFromGeneSymbol ($synonym1, $ontologies, $species1, $dataSources1, $evidenceCodes1, $geneIds1);

      if ($number1 == 0) { 
	echo "\n<p>No gene is found for gene: " . $symbol1;
	return;
      }
    }
    else {
      echo "\n<p>No gene is found for gene: " . $symbol1 . " based on the current filter.</p>";      
      return;
    }
  }
  

  $geneIds2 = array();
  $number2 = getGeneIdsFromGeneSymbol ($symbol2, $ontologies, $species2, $dataSources2, $evidenceCodes2, $geneIds2);

  if ($number2 == 0) {
       
    searchOneSynonym ($symbol2, $synonym2);
    
    if ($symbol2 != $synonym2) {
      echo "\n<p>A synonym of gene " . $symbol2 . " is found: " . $synonym2 . ".</p>"; 
      $number2 = getGeneIdsFromGeneSymbol ($synonym2, $ontologies, $species2, $dataSources2, $evidenceCodes2, $geneIds2);

      if ($number2 == 0) { 
	echo "\n<p>No gene is found for gene: " . $symbol2;
	return;
      }
    }
    else {
      echo "\n<p>No gene is found for gene: " . $symbol2 . " based on the current filter.</p>";      
      return;
    }
  }



  //check the flowchart for details
  if ($number1 > 1 || $number2 > 1) {    
    //output geneComparisionTwo2.php with geneComparisionTwo3.php hyperlinks
    //geneComparisionTwo3.php will be called
    displayIdsFromTwoGeneSymbols ($geneIds1, $geneIds2);
    return;
  }


  //the following codes are the same as geneCompareTwo3.php

  //3D

  $similarities = array();
  $similarities = array_pad ($similarities, 3, 0);

  $TermComparisonTable = array();
  $TermComparisonTable = array_pad ($TermComparisonTable, 3, array());

 
  //no need species since gene id can defer to it
  $TermComparisonTable = compareTwoGeneIdsStatistics ($geneIds1[0], $geneIds2[0], $ontologies, $dataSources1, $evidenceCodes1, $dataSources2, $evidenceCodes2, $method, $similarities);
  



  //the following is to display

  //options 1: using inputs geneid + user's input (evidence codes and data source) 
  //to getTermAll, including valid evidence codes and data source


  //options 2: using inputs geneid + terms to get evidence codes and data sources
  //better, since no need distinguish the ontology now
  //getTermsAllFromGeneId 

  //print_r( $similarities );
  displayGeneCompareTwo ($geneIds1[0], $geneIds2[0], $similarities, $TermComparisonTable);
   
  return;
}


function writeSequence( $sequence ){

  $fileName = trim (shell_exec ("mktemp ./Temp/sequence.XXXXXX"));
  $temp = fopen ($fileName, 'w') or die("could not open temporary file $fileName");
  fwrite ($temp, $sequence);
  fclose ($temp);

  return $fileName;
}

?>
