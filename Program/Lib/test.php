<?php

include_once('database.php');
include_once('GOCompareTwoStatistics.php');

$link = connectToMySQL();

function dataExist($id)
{
     $sql_string = "SELECT count FROM pre_count where term_id = $id";
     $result = mysql_query($sql_string);

     if (!$result) 
	  die( "\n<p>Error performing query whether term exists " . mysql_error() . $sql_string ."</p>");
     if (mysql_num_rows ($result) == 0) 
	  return false;
     $row = mysql_fetch_array ($result);
     return $row[0];

}

function insertIntoDatabase_count()
{
     mysql_query("DROP TABLE IF EXISTS pre_count");
     $sql = "CREATE TABLE pre_count ( term_id INT, count BIGINT";
     $sql .= ", PRIMARY KEY (term_id))";
     if (!mysql_query($sql))
	  die("Table Creation Error!" .mysql_error());
     
     $sql_string = "SELECT distinct term_id";
     $sql_string .= " FROM gene_product_count";
     $result = mysql_query($sql_string);
     if (!$result) 
	  die("\n<p>Error performing query select distinct from gene_product_count " . mysql_error() . $sql_string ."</p>");
     while ($row = mysql_fetch_array($result)) {
	  $id = $row[0];
	  /* if (($tmp = dataExist($id)) === false) { /\* if did not find id in the pre_count table, insert into it *\/ */
	  if (!mysql_query("INSERT INTO pre_count VALUES ($id, ". getProductCount($id). ")"))
	       die(mysql_error() ."<br/>");    
     }
}


function insertIntoDatabase_frequency()
{
     mysql_query("DROP TABLE IF EXISTS pre_frequency");
     $sql = "CREATE TABLE pre_frequency ( term_id INT, count BIGINT";
     $sql .= ", PRIMARY KEY (term_id))";
     if (!mysql_query($sql))
	  die("Table Creation Error!" .mysql_error());
     
     $sql_string = "SELECT term_id";
     $sql_string .= " FROM pre_count";
     $result = mysql_query($sql_string);
     if (!$result) 
	  die("\n<p>Error performing query select distinct from gene_product_count " . mysql_error() . $sql_string ."</p>");
     while ($row = mysql_fetch_array($result)) {
	  $id = $row[0];
	  /* if (($tmp = dataExist($id)) === false) { /\* if did not find id in the pre_count table, insert into it *\/ */
	  if (!mysql_query("INSERT INTO pre_frequency VALUES ($id, ". getFrequency($id). ")"))
	       die(mysql_error() ."<br/>");    
     }
}

insertIntoDatabase_frequency();

?>
