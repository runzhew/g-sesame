<?php


  /*



  This function is the pure mathmatics program
  */





function getCorrelation ($xs, $ys) {

  //sizes are the same
  $size = sizeof ($ys);

  $sum1 = array_sum ($xs);
  $sum2 = array_sum ($ys);


  $average1 = $sum1 / $size;
  $average2 = $sum2 / $size;


  $numerator = 0;

  $denominator1 = 0;
  $denominator2 = 0;

  for ($index = 0; $index < $size; $index ++) {

    $numerator += ($xs[$index] - $average1) * ($ys[$index] - $average2);


    $denominator1 += ($xs[$index] - $average1) * ($xs[$index] - $average1);
    $denominator2 += ($ys[$index] - $average2) * ($ys[$index] - $average2);

    if ($denominator1 == 0 || $denominator2 == 0) {
      return 0;
    }

  }

  return $numerator / sqrt ($denominator1 * $denominator2); 
}



/*
 In the future,  all xs = 0 should be removed

 $xs are similaritiesBLAST, or Microarray
  
 $ys is the 2D, which contain three ontologies

*/


function getCorrelationThreeOntologies ($method, $xs, $ys) {
  //echo "<p>ys </p>";
  //print_r ($ys);


  $count = sizeof ($xs);
  //calculate correlation

  //filter out all three aspects


  $similaritiesBP = array();
  $similaritiesCC = array();
  $similaritiesMF = array();




  for ($index = 0; $index < $count; $index++) {
    $similaritiesBP[$index] = $ys[$index][0];
    $similaritiesCC[$index] = $ys[$index][1];
    $similaritiesMF[$index] = $ys[$index][2];
  }




  //echo "\n<p>All valid sequences:  </p>";
  //print_r ($xsValid);

  //echo "<p>ys </p>";
  //print_r ($similaritiesBP);


  //html speical code
  // &amp; will be display as &

  $correlationBP = getCorrelation ($xs, $similaritiesBP);
  echo "\n<p>$method & " . number_format ($correlationBP, 3) . " & ";


  $correlationCC = getCorrelation ($xs, $similaritiesCC);
  echo number_format ($correlationCC, 3). " & ";


  $correlationMF = getCorrelation ($xs, $similaritiesMF);
  echo number_format ($correlationMF, 3) . "\\\\</p>";
  return;
}

/*
 creating array and return as reference

*/

function initializeFeeding(&$similarities, &$ontologies, &$dataSources1, &$evidenceCodes1, &$dataSources2, &$evidenceCodes2) {


  $similarities = array();
  $ontologies = array();   
  $dataSources1 = array();
  $evidenceCodes1 = array();
  $dataSources2 = array();
  $evidenceCodes2 = array();

  $ontologies[] = "All";  


  //BLAST alpha, beta, gamma
  $dataSources1[] = "All";
  $dataSources2[] = "All";


  //Microarray
  //$dataSources1[] = "SGD";
  //$dataSources2[] = "SGD";



  $evidenceCodes1[] = "All"; 
  $evidenceCodes2[] = "All"; 

  return;
}






/*
 Input:
 $method, 4 methods, only to determin maximum number or printing, it is the scale now
 $ontology = 0 ,1, or 2, BP, CC, or MF
 $similaritiesMicroarray, 
 $similaritiesResnik  //3 ontology inner part, so $ontology is necessory as this function only can calculate one ontology


*/

function getCorrelationDouble ($method, $ontology, $similaritiesMicroarray, $similaritiesResnik) {

  //calculate method vs. correlation, result correlatonMore
  $maxResnik = 20; 


  //partition according to similarity values of method
  $similaritiesMicroarray3 = array();
  $similaritiesResnik3 = array();


  for ($index = 0; $index < $maxResnik; $index++) {

    foreach ($similaritiesResnik as $gene1 => $oneLine) {
      foreach ($oneLine as $gene2 => $similarity) {

	if ($method * $similarity[$ontology] >= $index && $method * $similarity[$ontology] < $index + 1) {
	  $similaritiesMicroarray3[$index][$gene1][$gene2] = $similaritiesMicroarray[$gene1][$gene2];
	  //three ontologies assignment
	  $similaritiesResnik3[$index][$gene1][$gene2] = $method * $similarity[$ontology];
	}
      
      }
    }


  }


  //print_r ($similaritiesMicroarray3);
  //print_r ($similaritiesResnik3);
  //exit();



  //calculate total

  $similaritiesCorrelation = array();
  $similaritiesResnik5 = array();

  for ($index = 0; $index < $maxResnik; $index++) {

    if (!array_key_exists ($index, $similaritiesResnik3)) {
      //echo $index . "No such index ";
      continue;
    }


    //Adapter
    //from assocaitve array to index array to call function getCorrelationThreeOntologies

    $similaritiesMicroarray4 = array();
    $similaritiesResnik4 = array();
  

    foreach ($similaritiesMicroarray3[$index] as $gene1 => $oneLine) {
      foreach ($oneLine as $gene2 => $similarity) {
	$similaritiesMicroarray4[] = $similarity;
	$similaritiesResnik4[] = $similaritiesResnik3[$index][$gene1][$gene2];
      }
    }


    //print_r ($similaritiesMicroarray2);
    //echo "\n<p>$similaritiesG_SESAME </p>";
    //print_r ($similaritiesG_SESAME2);
    //echo "\n<p> $index: correlation is ";
    //  getCorrelationThreeOntologies ("Resnik", $similaritiesMicroarray4, $similaritiesResnik4);

    $singleCorrelation = getCorrelation ($similaritiesMicroarray4, $similaritiesResnik4);

    $similaritiesCorrelation[] = $singleCorrelation;
    $similaritiesResnik5[] = $index;

    //echo "\n<p> </p>";
  }



  $finalCorrelation = getCorrelation ($similaritiesCorrelation, $similaritiesResnik5);
  // echo "\n<p>Final correlation is " . number_format ($finalCorrelation, 3). " </p>";

  return $finalCorrelation;

}

?>
