<?php
include_once ("geneCompareTwoRandomWalk.php");


//input $genes is an indexed array of ids
//output is the associatiative array
//values are saved upper triangle ( above diaginal )



function compareGenesMultipleRandomWalk ($genes, $ontologies, $dataSources, $evidenceCodes, $isA, $partOf, &$similarityTable) {

  $size = sizeof ($genes);

  for ($index1 = 0; $index1 < $size - 1; $index1++) {    
    for ($index2 = $index1 + 1; $index2 < $size; $index2++) {

      $gene1 = $genes[$index1];
      $gene2 = $genes[$index2];


      $similarities = array();
      //use same $dataSources, $evidenceCodes
      compareTwoGeneIdsRandomWalk ($gene1, $gene2, $ontologies, $dataSources, $evidenceCodes, $dataSources, $evidenceCodes, $isA, $partOf, $similarities);
      
      $numberNoneZero = 0;

      for ($index = 0; $index < 3; $index++) {
	if ($similarities[$index] != 0) {
	  $numberNoneZero ++;
	}
      }
  

      //output of $similarities is {0, 0, 0.1} or all zeros
      if ($numberNoneZero == 0) {
	$similarity = 0;
      }
      else {
	$similarity = array_sum ($similarities) / $numberNoneZero;
      }

     
      if ($gene1 < $gene2){
	$similarityTable[$gene1][$gene2] = $similarity; 
      }
      else{
	$similarityTable[$gene2][$gene1] = $similarity; 
      }

    }
  }

  return;
}//end of function 



//input $genes is an indexed array of ids
//output is the associatiative array
//values are saved upper triangle ( above diaginal )



function compareGenesMultipleThreeOntologies ($genes, $ontologies, $dataSources, $evidenceCodes, $isA, $partOf, &$similarityTable) {

  $size = sizeof ($genes);

  for ($index1 = 0; $index1 < $size - 1; $index1++) {    
    for ($index2 = $index1 + 1; $index2 < $size; $index2++) {

      $gene1 = $genes[$index1];
      $gene2 = $genes[$index2];


      $similarities = array();


      compareTwoGeneIds ($gene1, $gene2, $ontologies, $dataSources, $evidenceCodes, $dataSources, $evidenceCodes, $isA, $partOf, $similarities);
      
      if ($gene1 < $gene2){
	$similarityTable[$gene1][$gene2] = $similarities; 
      }
      else{
	$similarityTable[$gene2][$gene1] = $similarities; 
      }

    }
  }

  return;
}//end of function 




?>
