<?php

  //the sequence of functions are arranged by sub to main sequence


  //input two arrays and check if there is merge happens
  //rturn 0 or 1 and if 1, then a new 1D array is the merged of $rowArray and $columnArray,
function twoArrayMerge (&$similarityTable, $threshold, &$rowArray, &$columnArray) {

  //print_r($rowArray);
  //print_r($columnArray);


  foreach ($rowArray as $gene1) {
    foreach ($columnArray as $gene2) {
      
      //find it and then merge and then return
      if ($gene1 <= $gene2) {

	if ($similarityTable[$gene1][$gene2] >= $threshold) {
	  return 1;
	}
      }
      else{
	if ($similarityTable[$gene2][$gene1] >= $threshold) {
	  return 1;
	}
      }
    }//for column
  }//for row

  return 0;
}




//input a 2D array and check if there is merge happens 
//between any two of the 1D array
//return 0 or 1
//if 1, then the 2D array is updated

//the algorithm will compare two 1D array one by one
//if merge happens, the merged array *replace* two original two arraies
//the algorithm begin from beginning

//$oneThresholdInputArray is the input array
//if merge happens, it is also the output array

function oneThresholdMerge (&$similarityTable, $threshold, &$oneThresholdInputArray) {


  //to check if this $theashold is low enough
  $isMergeEverHappens = 0;


  $size = sizeof($oneThresholdInputArray);

  $row = 0;
  //compare one by one, not row and column
  while ($row < $size - 1) {


    //define this variable before for loop
    $isTwoArrayMerged = 0;

    for ($column = $row + 1; $column < $size; $column++) {

      $isTwoArrayMerged = twoArrayMerge($similarityTable, $threshold, $oneThresholdInputArray[$row], $oneThresholdInputArray[$column]);

      if ($isTwoArrayMerged == 1) {

	//for upper function uses to tell if there is a merge happens
	$isMergeEverHappens = 1;

	//echo "merge happens\n";
	//delete one and merge it into another one
	//note that numeric keys in input are not preserved.

	//print_r($oneThresholdInputArray);
	$columnArray = array_splice($oneThresholdInputArray, $column, 1);

	//here $columnArray would be an array (new one) 
	//whose elements are from the elements from $oneThresholdInputArray (an array)
	/* for example
	 Array  //new synthesized array
	 (
	 //one element (an array, what we need) from the original array
	 [0] => Array
	 (
	 [0] => 2295686
	)
	 
	)
	*/


	//print_r($columnArray);
	//print_r($oneThresholdInputArray);
	$oneThresholdInputArray[$row] = array_merge($oneThresholdInputArray[$row], $columnArray[0]);
	//print_r($oneThresholdInputArray);

	//start to begin to search of possible merging 
	break;
      }// if

    }//for

    //if merged happens, start from the beginning to merge by resetting $row.
    if ($isTwoArrayMerged == 1) {
      $row = 0;
      $size = sizeof($oneThresholdInputArray);
      $isTwoArrayMerged = 0;
    }
    else{
      $row ++;
    }

  }//while
  
  return $isMergeEverHappens;  
}



//This is the main function of cluster
//$genes is only for display, no use for cluster


//$similarityTable is a gene1 <= gene2 associate array


/*
Array
(
    [82793] => Array
        (
            [82806] => 0.96236073471846
            [82812] => 0.96236073471846
            [82818] => 0.96236073471846
            [83804] => 0.89367073069478
            [82822] => 0.61644021270961
       )

    [82806] => Array
        (
            [82812] => 1
            [82818] => 1
            [83804] => 0.93130999597632
            [82822] => 0.57628535177967
       )

    [82812] => Array
        (
            [82818] => 1
            [83804] => 0.93130999597632
            [82822] => 0.57628535177967
       )

    [82818] => Array
        (
            [83804] => 0.93130999597632
            [82822] => 0.57628535177967
       )

    [82822] => Array
        (
            [83804] => 0.56653530052331
       )

)


*/

 //output threshold
//$threshold is a dicimal number, not good candidate to be an index or assoicate array
//so two arraies are used


function cluster ($similarityTable, $deltaDecreasement, &$thresholds, &$clusterTree) {

  
  $size = sizeof ($similarityTable);


  //initialize $clusterTree by inserting arraies from 0 to size - 1


  //this code is the same as displayGeneMultipleSimilarities to extract unique gene ids from $similarityTable 
  //check the data structure above for details

  $genes1 = array_keys ($similarityTable);
  $genes2 = array_keys ($similarityTable[ $genes1[0]]);
  $genes3 = array_merge ($genes1, $genes2);
  $genes = array_unique ($genes3);
  
  $index = 0;
  foreach ($genes as $temp => $id) {
    //3D
    $clusterTree[0][$index] = array($id);
    $index ++;
  }
  
  
  //get max in $similarityTable
  //only for Jiang, Lin's methods is useful
  $similarityMax = 0;
  foreach ($similarityTable as $row) {
    foreach ($row as $similarity) {      
      if ($similarityMax < $similarity) {
	$similarityMax = $similarity;
      }
    }
  }
  
  $startTheashold = 0;

  if ($similarityMax < 1 ) {
    $startTheashold = 1.0;
  }
  else{
    $startTheashold = $similarityMax;
  }
  

  //  $threshold = $startTheashold - $deltaDecrease;
  //some case, similarity is 1 between two different genes, so $threshold starts from $startTheashold
  //not from $startTheashold - $deltaDecrease
  $threshold = $startTheashold;



  //This is a 2D array
  $oneThresholdInputArray = $clusterTree[0];

  $isMergeEverHappens = 0;

  //echo "  One merge happen under the threshold $threshold;";
  while ($threshold >= 0) {

 
    $isMergeEverHappens = oneThresholdMerge ($similarityTable, $threshold, $oneThresholdInputArray);
    
    //if $isMergeEverHappens, then $oneThresholdInputArray has been changed into another level
    if ($isMergeEverHappens == 1) {

      $clusterTree[] = $oneThresholdInputArray;      
      $thresholds[] = $threshold;
      $isMergeEverHappens = 0;
    }
    

    //stop if they merge before $threshold == 0
    if (sizeof ($oneThresholdInputArray) == 1) {
      return;
    }


    //prepare next iteration

    //decrease threshold for further merge
    $threshold -= $deltaDecreasement;

    //   echo "<p> threshold is " . $threshold;

    //exit;
    //prevent decimal errors
    if ($threshold < 0.0000001) {
      $threshold = 0;
    }
    
  }//$threshold loop

  return;
}













////////////////////////////////////////
//The following functions are for step 2
////////////////////////////////////////

//input a node
//return an array of index of sons ($adjacent Nodes Indexs because layer is lower) of this node

function findSonIndexs (&$clusterTree, $layer, $index, &$adjacentNodesIndexs) {


  //the leaves of the tree, then do nothing????
  if ($layer == 0) {
    return;
  }


  //current input node
  $nodeU = $clusterTree[$layer][$index];

  //lower layer
  $sonLayerSize = sizeof ($clusterTree[$layer - 1]);
 
  for ($index2 = 0; $index2 < $sonLayerSize; $index2++) {

    $nodeV = $clusterTree[$layer - 1][$index2];

    //array_intersect() returns an array containing all the values of array1 
    // that are present in all the arguments. Note that keys are preserved.
    $nodeTemp = array_intersect($nodeU, $nodeV);


    //if $nodeV is the son of $nodeU, then there is at lease one item is the same
    if (sizeof($nodeTemp) != 0) {      
      $adjacentNodesIndexs[] = $index2;
    }
  }


  return;
}



//add all sons of a node into the stacks 
function addSonIndexs (&$clusterTree, $layer, $index2, &$stackLayer, &$stackPosition) {

  //find all adjacent nodes of node ($layer, $index2) in layer $index - 1 (all sons)
  $sonIndexs = array();
  findSonIndexs($clusterTree, $layer, $index2, $sonIndexs);

  //echo "adjacent Nodes <br>";
  //print_r($adjacentNodes);


  //push into stack
  $size = sizeof ($sonIndexs);
  for ($index3 = 0; $index3 < $size; $index3++) {
    $stackLayer[] = $layer - 1;
    $stackPosition[] = $sonIndexs[$index3];   
  }

  return;
}




//this part use no recrusive DSF
//not included in CLRS book
//check wikipedia for details
/*
 Another version

 dfs(graph G) {
 list L = empty
 tree T = empty
 choose a starting vertex x
 search(x)
 while (L is not empty)
 remove edge (v, w) from end of L
 if w not yet visited{
 add (v, w) to T
 search(w)
 }
 }
   
 search(vertex v) {
 visit v
 for each edge (v, w) {
 add edge (v, w) to end of L
 }
 }




 For the fatty example, we have 7=height layers
 Array
 (
 [0] => Array
 (
 [0] => Array
 (
 [0] => 2295683
)

 [1] => Array
 (
 [0] => 2295397
)

 [2] => Array
 (
 [0] => 2295520
)

 [3] => Array
 (
 [0] => 28567
)

 [4] => Array
 (
 [0] => 2295684
)

 [5] => Array
 (
 [0] => 2295685
)

 [6] => Array
 (
 [0] => 2295686
)

 [7] => Array
 (
 [0] => 2295701
)

 [8] => Array
 (
 [0] => 2295788
)

)

 [1] => Array
 (
 [0] => Array
 (
 [0] => 2295683
 [1] => 2295686
)

 [1] => Array
 (
 [0] => 2295397
 [1] => 2295520
)

 [2] => Array
 (
 [0] => 28567
)

 [3] => Array
 (
 [0] => 2295684
)

 [4] => Array
 (
 [0] => 2295685
)

 [5] => Array
 (
 [0] => 2295701
)

 [6] => Array
 (
 [0] => 2295788
)

)

 [2] => Array
 (
 [0] => Array
 (
 [0] => 2295683
 [1] => 2295686
 [2] => 2295684
)

 [1] => Array
 (
 [0] => 2295397
 [1] => 2295520
)

 [2] => Array
 (
 [0] => 28567
)

 [3] => Array
 (
 [0] => 2295685
)

 [4] => Array
 (
 [0] => 2295701
)

 [5] => Array
 (
 [0] => 2295788
)

)

 [3] => Array
 (
 [0] => Array
 (
 [0] => 2295683
 [1] => 2295686
 [2] => 2295684
 [3] => 2295685
)

 [1] => Array
 (
 [0] => 2295397
 [1] => 2295520
)

 [2] => Array
 (
 [0] => 28567
)

 [3] => Array
 (
 [0] => 2295701
)

 [4] => Array
 (
 [0] => 2295788
)

)

 [4] => Array
 (
 [0] => Array
 (
 [0] => 2295683
 [1] => 2295686
 [2] => 2295684
 [3] => 2295685
 [4] => 2295701
)

 [1] => Array
 (
 [0] => 2295397
 [1] => 2295520
 [2] => 2295788
)

 [2] => Array
 (
 [0] => 28567
)

)

 [5] => Array
 (
 [0] => Array
 (
 [0] => 2295683
 [1] => 2295686
 [2] => 2295684
 [3] => 2295685
 [4] => 2295701
 [5] => 2295397
 [6] => 2295520
 [7] => 2295788
)

 [1] => Array
 (
 [0] => 28567
)

)

 [6] => Array
 (
 [0] => Array
 (
 [0] => 2295683
 [1] => 2295686
 [2] => 2295684
 [3] => 2295685
 [4] => 2295701
 [5] => 2295397
 [6] => 2295520
 [7] => 2295788
 [8] => 28567
)

)

)

*/


function getSequence (&$clusterTree, &$sequence) {

  //print_r($clusterTree);


  $stackLayer = array();
  $stackPosition = array();


  //height of the tree
  $height = sizeof ($clusterTree);

  //  echo $height;

  //add son nodes (adjacent nodes) into stacks


  //$height - 1 is the root of the clusterTree
  //0 is the only position(root node) of this layer
  addSonIndexs ($clusterTree, $height - 1, 0, $stackLayer, $stackPosition);


  $sizeStack = sizeof ($stackLayer);

  //echo "  <br> size is  $sizeStack ";

  while ($sizeStack != 0) {

    $layer = array_pop ($stackLayer);
    $position = array_pop ($stackPosition);


    //if it is a leave, then remember it
    if ($layer == 0) {
      $sequence[] = $clusterTree[0][$position][0];
    }



    if ($layer > 0) {
      addSonIndexs ($clusterTree, $layer, $position, $stackLayer, $stackPosition);
    }
    $sizeStack = sizeof ($stackLayer);
  }

  return;

}








////////////////////////////////////////
//The following functions are for step 3
////////////////////////////////////////
  
function changeClusterTreeToConsistent (&$sequence, &$clusterTree, &$clusterTreeConsistent) {


  //height of the tree
  $height = sizeof ($clusterTree);
  $numberElements = sizeof ($sequence);


  for ($index1 = 0; $index1 < $height; $index1++) {

    //2D
    $oneLayer = array();
    $index3 = 0;

    while ($index3 < $numberElements) {
      $oneElement = $sequence[$index3];

      //find the related node of in cluster tree
      $width = sizeof($clusterTree[$index1]);
      for ($index2 = 0; $index2 < $width; $index2++) {

	if (in_array($oneElement, $clusterTree[$index1][$index2])) {	  
	  break;
	}
	
      }

      // add one node of the tree for display
      $oneTreeNode = array();  
      $sizeTreeNode = sizeof ($clusterTree[$index1][$index2]);
      for ($index4 = 0; $index4 < $sizeTreeNode; $index4++) {
	$oneTreeNode[] = $sequence[$index3];
	$index3++;
      }

      $oneLayer[] = $oneTreeNode;


    }//while

    $clusterTreeConsistent[] = $oneLayer;

  }

  return;
}



?>
