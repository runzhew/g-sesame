<?php

//input: id, the starting (lowest) point, 

include_once ("/var/www/html/rain/Program/Lib/GOOne.php");
include_once ("/var/www/html/rain/Program/Lib/GOCompareTwoStatistics.php");

function getAncestors ($id, &$Ancestors) {

  /* $sqlString = "SELECT DISTINCT t.id */
  /*               FROM */
  /*               graph_path  */
  /*               INNER JOIN */
  /*               term2term AS r ON (r.term2_id = graph_path.term1_id) */
  /*               INNER JOIN */
  /*               term AS t ON (t.id = r.term1_id) */
  /*               WHERE t.is_root <> 1 AND graph_path.term2_id = $id;"; */

  /* // writing the query to select the term */

  /* $result = mysql_query ($sqlString); */

  /* if (!$result) { */
  /*   echo "\n<p>Error performing query from getting ancestors " . mysql_error() ."</p>"; */
  /*   echo $sqlString; */
  /*   return 1; */
  /* } */

  /* //change the graph from list (matrix) into adjacency list */
   

  /* if (mysql_num_rows ($result) == 0 ){ */
  /*   echo "\n<p>No result found from the current query to get ancestors </p>"; */
  /*   return 0; */
  /* } */
  /*   $Ancestors[] = $id; */


       $sqlString = "SELECT DISTINCT graph_path.term1_id  FROM  graph_path
                INNER JOIN
                term AS t ON (t.id = term1_id)
                WHERE t.is_root <> 1 AND graph_path.term2_id = $id;";

  // writing the query to select the term

  $result = mysql_query ($sqlString);

  if (!$result) {
    echo "\n<p>Error performing query from getting ancestors " . mysql_error() ."</p>";
    echo $sqlString;
    return 1;
  }

  //change the graph from list (matrix) into adjacency list
   

  if (mysql_num_rows ($result) == 0 ){
    echo "\n<p>No result found from the current query to get ancestors </p>";
    return 0;
  }
  while ($id1 = mysql_fetch_array ($result)) {
    $Ancestors[] = $id1[0];

  }

  return 0;
}


function getICSemanticValues ($Ancestors, &$ICSemanticValues, $produceCountAll) {
     foreach ($Ancestors as $id) {
	  $tmp = getFrequency_pre($id)/$produceCountAll;
	  if ($tmp == 0)
	       $ICSemanticValues[$id] = 0.5;
	  else if ($tmp >= 1)
	       $ICSemanticValues[$id] = 1.0;
	  else
	       $ICSemanticValues[$id] = 1/(1+exp(1/log($tmp, 10)));
     }


  return;

}
	

//get numetorator value
//input id -> value
function getIntersectionSum_AIC (&$semanticValues1, &$semanticValues2) {

  $sum = 0;
  foreach ($semanticValues1 as $vertex => $value) {
    if (array_key_exists($vertex, $semanticValues2)) {
      $sum += 2*$semanticValues1[$vertex];
    }
  }
  
  return $sum;
}



//This is the main function of Two GO Terms similarity

function compareTwoGOIdsAIC ($id1, $id2, $produceCountAll) {

  $Ancestors1 = array();
  $ICSemanticValues1 = array();
  getAncestors($id1, $Ancestors1);
  getICSemanticValues ($Ancestors1, $ICSemanticValues1, $produceCountAll) ;


  $Ancestors2 = array();
  $ICSemanticValues2 = array();

  getAncestors($id2, $Ancestors2);
  getICSemanticValues ($Ancestors2, $ICSemanticValues2, $produceCountAll);
  

  $sumIntersection = getIntersectionSum_AIC ($ICSemanticValues1, $ICSemanticValues2);

  $sum3 = array_sum ($ICSemanticValues1);
  $sum4 = array_sum ($ICSemanticValues2);


  return $sumIntersection / ($sum3 + $sum4);
}





?>

