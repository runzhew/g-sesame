<?php

//Name:
//Project: Gene Ontology Comparison
//Date: 8/11/07
//This file contain all of the function which use for GOSearch.php page. 
//Creating the functions seperate from the actual site helps with the organization of the 
//source code.



//get all semantic values of one term


/*
input: id, the starting (lowest) point, 

adjacency list

the algorithm can refer to CLRS p532
since it is an DAG and start from lowest point
there is no need to maintain the color data structure as CLRS does


Conveat
$semanticValues contain 1

*/


function getSemanticValues ($id, &$DAG, $isA, $partOf, &$semanticValues) {

  $sizeVertexs = sizeof ($DAG);

  //BFS

  //lowest descendant
  $semanticValues[$id] = 1;

  $queue[] = $id;


		 
  //breadth first search begins
  while (sizeof ($queue) != 0) {

    //dequeue from head of queue
    $vertexU = $queue[0];
    //index will not be changed when use unset($queue[0]);
    array_splice ($queue, 0, 1);

    
    //root vertex, 1, will not be processed even if it is in the queue
    //otherwise, $DAG[$vertexU] will be null
    if (!array_key_exists ($vertexU, $DAG)) {
      continue;
    }
    
    
    foreach ($DAG[$vertexU] as $vertexV => $type) {

      //generate a new one if not exist, and initialize to 0
      if (!array_key_exists($vertexV, $semanticValues)) {
	$semanticValues[$vertexV] = 0;
      }


      //check if updated $semanticValueV is needed
      $semanticValueU = $semanticValues[$vertexU];
      if ($type == 1) {
	$semanticValueV2 = $isA * $semanticValueU;
      }
      else {
	$semanticValueV2 = $partOf * $semanticValueU;
      }
      if ($semanticValueV2 > $semanticValues[$vertexV]) {
	$semanticValues[$vertexV] = $semanticValueV2;
      }
      

	  
      //enqueue
      $queue[] = $vertexV;
    }//end of all parents (more general one)
  
  
  }//while

  return;
}
	
	

//get numetorator value
//input id -> value
function getIntersectionSum (&$semanticValues1, &$semanticValues2) {

  $sum = 0;
  
  foreach ($semanticValues1 as $vertex => $value){
    if (array_key_exists($vertex, $semanticValues2)){
      $sum += $semanticValues1[$vertex];
      $sum += $semanticValues2[$vertex];
    }
  }
  
  return $sum;
}




//This is the main function of Two GO Terms similarity


function compareTwoGOIds ($id1, $id2, $isA, $partOf) {


  $DAG1 = array();

  //DAG is the adjacency list
  getDAG ($id1, $DAG1);

  //id -> value
  $semanticValues1 = array();
  getSemanticValues ($id1, $DAG1, $isA, $partOf, $semanticValues1);



  $DAG2 = array();
  getDAG ($id2, $DAG2);

  $semanticValues2 = array();
  getSemanticValues ($id2, $DAG2, $isA, $partOf, $semanticValues2);
  

  $sumIntersection = getIntersectionSum ($semanticValues1, $semanticValues2);

  $sum3 = array_sum ($semanticValues1);
  $sum4 = array_sum ($semanticValues2);

  //return $sumIntersection / ($sum3 + $sum4);

  //echo "<p> $sumIntersection </p>"; 

//  echo "<p> $sum3 </p>"; 
 // echo "<p> $sum4 </p>"; 
  
  //return $sumIntersection * $sumIntersection / ($sum3 * $sum4);
  return $sumIntersection / ($sum3 + $sum4);
}





//This function compare two GO Terms similarity with caches, index is term id, value is the semantic value
//$semanticValues1 and 2 are associative array now


//future work
//maybe use a single function in this function since two are same
function compareTwoGOIds2 ($term1, $term2, $isA, $partOf, &$termSemanticValuesCache, &$termSemanticHit, &$termSemanticMiss, &$termSemanticSumCache) {

  //have to define outside the if else
  $semanticValues2 = array();
  $semanticValues2 = array();
  $sum1 = 0;
  $sum2 = 0;

  
  //  if(array_key_exists($term1, $termSemanticValuesCache)){
  if (isset($termSemanticValuesCache[$term1])) {

    $semanticValues1 = $termSemanticValuesCache[$term1];

    $termSemanticHit++;

    $sum1 = $termSemanticSumCache[$term1];
  }
  else {
  
    $DAG1 = array();

    //from original table
    getDAG ($term1, $DAG1);

    //from graph_path2 table only
    //getDAG2($term1, $DAG1);


    getSemanticValues ($term1, $DAG1, $isA, $partOf, $semanticValues1);

    //caching
    //data structure is id -> array()
    $termSemanticValuesCache[$term1] = $semanticValues1;
    $termSemanticMiss++;
    
    
    $sum1 = array_sum ($semanticValues1);
    $termSemanticSumCache[$term1] = $sum1;
  }


    
  //if(array_key_exists($term2, $termSemanticValuesCache)){
  if (isset($termSemanticValuesCache[$term2])) {    
    $semanticValues2 = $termSemanticValuesCache[$term2];
    $termSemanticHit++;

    $sum2 = $termSemanticSumCache[$term2];
  }
  else {
    $DAG2 = array();

    getDAG ($term2, $DAG2);
    //getDAG5($term2, $DAG2);
    getSemanticValues ($term2, $DAG2, $isA, $partOf, $semanticValues2);

    //caching
    $termSemanticValuesCache[$term2] = $semanticValues2;
    $termSemanticMiss++;

    $sum2 = array_sum ($semanticValues2);
    $termSemanticSumCache[$term2] = $sum2;
  }

  

  $sumIntersection = getIntersectionSum ($semanticValues1, $semanticValues2);

  return $sumIntersection / ($sum1 + $sum2);

}

?>

