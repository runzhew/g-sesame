<?php
include_once ("dot.php"); 
include_once ("Filter/geneOne.php");




//input gene id
//output gene symbol
//this function is mainly used for display
function getGeneSymbolFromGeneId ($id) {
  
  $sqlString = "SELECT symbol FROM gene_product WHERE id = $id;";
  
  $result = mysql_query ($sqlString); 
  
  if (!$result) {
    echo "\n<p>No results for id: $id </p>";


    if (DEBUG == 1) {
      //echo "\n<p> $sqlString </p>";
      return 0;
    }  

  }
  list ($symbol) = mysql_fetch_array ($result);
  return $symbol;
}




/*
input gene symbol
//output the related gene ids

//one symbol has multiple ids
//for example
//SELECT id FROM gene_product WHERE symbol = 'ADH1';

//the following functions are related with genes themselves
*/

function getGeneIdsFromGeneSymbol ($symbol, $ontology, $species, $dataSources, $evidenceCodes, &$ids) {
  

  //genecluster will call this function

  //print_r ($species);
  $sqlString = filterToGetGeneIdsFromGeneSymbol ($symbol, $ontology, $species, $dataSources, $evidenceCodes);

  // echo $sqlString;

  $result = mysql_query ($sqlString);

  //This output shounld put outside to take care of the loop 
  if (!$result) {
    echo "No results for $symbol;";
    //echo "<p>$sqlString</p>";
    return 0;
  }
  
  $index = 0;
  while (list ($id) = mysql_fetch_array ($result)) {  
    $ids[] = $id;    
    // echo "<p> Index = " . $index . " id = " . $id;
    $index ++;
  }
  
  return $index;
}



//0. from gene id to term id
//1. from gene id to term name //missing?
//2. from gene symbol to term ids
//3. from gene symbol to term all, oldest

//0
//input gene id

//no species since it is a property of gene id
//output the related GO term unique ids
//getGOTerms1 is located in geneComparisonTwo.php
//this function use reference to transfer arraies
//this function is called by searchTopGenesFromId

function getTermIdsFromGeneId ($gene, $ontology, &$dataSources, &$evidenceCodes, &$terms) {
  
  $sqlString = filterToGetTermIdsFromGeneId ($gene, $ontology, $dataSources, $evidenceCodes);
  
  //this information is very useful for debug
  //  echo "<p>filterToGetTermIdsFromGeneId is:  $sqlString <p>";
  //exit();


  $result = mysql_query ($sqlString);
  
  if (!$result) {
    echo "<p>Error in getTermIdsFromGeneIdOneOntology: $gene </p>";
    echo "<p>$sqlString </p>";
    return;
  }
  
  while (list ($id) = mysql_fetch_array ($result)) {
    //reading one by one and put in the table
    $terms[] = $id;
  }//while

  //$terms = array_unique($terms);
  //$terms = array_values($terms);

  return 0;
}





//input gene id
//output the related GO terms all information
//called by geneOne2.php
function getTermsFromGeneId ($gene, &$ontology, 
			     &$species2, &$datasources2, &$evidenceCodes2, 
			     &$ids, &$accs, &$names, &$species, &$dataSources, &$evidenceCodes) {


  $sqlString = filterToGetTermsFromGene ($gene, $ontology, $species2, $datasources2, $evidenceCodes2);

  
  //this information is very useful for debug
  //echo " <br> $sqlString <br>";
  //exit();



  $result = mysql_query ($sqlString);
  if (!$result) {
    echo " <p> Error of $gene </p>";
    echo " <p> $sqlString </p>";
    return;
  }


  while (list ($id, $acc, $name, $dataSource, $evidenceCode) = mysql_fetch_array ($result)) {	

    if (!isset ($id, $accs) || !in_array ($id, $accs)) {
      $accs[$id] = $acc;
    }

    if (!isset ($id, $names) || !in_array ($id, $names)) {
      $names[$id] = $name;
    }


    if (!isset ($id, $dataSources[$id]) || !in_array ($dataSource, $dataSources[$id])) {
      $dataSources[$id][] = $dataSource;
    }

    if (!isset ($id, $evidenceCodes[$id]) || !in_array ($evidenceCode, $evidenceCodes[$id])) {
      $evidenceCodes[$id][] = $evidenceCode;
    }

  }//while
  
  return;
}





//0. from term ids to gene ids
//1. from term id to gene symbol
//2. from term acc to gene id
//3. from term acc to gene symbol


//0

//input, terms is 3 entries now



function getGeneIdsFromTermIds (&$terms, &$ontologies, &$species2, &$dataSources2, &$evidenceCodes2, &$genes) {
  
 
  $sqlString = filterToGetGeneIdsFromTermIds ($ontologies, $species2, $dataSources2, $evidenceCodes2, $terms);
  
  //echo " <br> $sqlString <br>";
  //exit();
  
  
  $result = mysql_query ($sqlString);
  if (!$result) {
    echo "<p>Error in getGeneIdsFromTermIds: <br>" . $sqlString;
    flush();
    return;
  }
  
  while (list ($gene) = mysql_fetch_array ($result)) { 
    $genes[] = $gene;
  }//while
  
  return;
}



//the above functions related with terms and genes
/////////////////////////////////////////////////////////////////








//input 
//output the related gene symbols of these terms
//this function is called by geneTop
function getGeneDetailsSimpleFromIds ($similaritiesTopNs, &$ontologies, &$species2, &$dataSources2, &$evidenceCodes2, $searchLimitation, &$symbols, &$fullNames, &$genuses, &$species, &$dataSources, &$evidenceCodes) {


  $geneIds = array_keys ($similaritiesTopNs);
  $geneIds2 = implode (',', $geneIds);
  
  
  //input id
  $sqlString = filterToGetGenesDetailsSimpleFromIds ($geneIds2, $ontologies, $species2, $dataSources2, $evidenceCodes2, $searchLimitation);
  
  //echo $sqlString;
  //exit();
  
  
  $result = mysql_query ($sqlString);
  
  while (list ($id, $symbol, $fullName, $genus, $specie) = mysql_fetch_array ($result)) { 
    
    if (!isset ($symbols[$id])) {
      $symbols[$id] = $symbol;	
      $fullNames[$id] = $fullName;
      $genuses[$id] = $genus; 
      $species[$id] = $specie;
    }
    
  }//while
   
  return;
}








//this is very slow
//input 
//output the related gene symbols of these terms

function getGeneDetailsFromIds ($similaritiesTopNs, &$ontologies, &$species2, &$dataSources2, &$evidenceCodes2, $searchLimitation, &$symbols, &$fullNames, &$species, &$dataSources, &$evidenceCodes) {


  
  $geneIds = array_keys ($similaritiesTopNs);
  $geneIds2 = implode (',', $geneIds);
  
  
  //input id
  $sqlString = constructFilterToGetGenesDetails ($geneIds2, $ontologies, $species2, $dataSources2, $evidenceCodes2, $searchLimitation);
  
  // echo $sqlString;
  //exit();
  
  
  $result = mysql_query($sqlString);

  
  
  while (list($id, $symbol, $fullName, $specie, $dataSource, $evidenceCode) = mysql_fetch_array($result)) { 
    
    if (!isset ($symbols[$id])) {
      $symbols[$id] = $symbol;	
      $fullNames[$id] = $fullName;
    }
    
    
    //insert related info
    if (!isset ($dataSources[$id]) || !in_array ($dataSource, $dataSources[$id])) {
      $dataSources[$id][] = $dataSource;
    }

    //insert related info
    if (!isset ($species[$id]) || !in_array ($specie, $species[$id])) {
      $species[$id][] = $specie;
    }

    //insert related info
    if (!isset ($evidenceCodes[$id]) || !in_array ($evidenceCode, $evidenceCodes[$id])) {
      $evidenceCodes[$id][] = $evidenceCode;
    }
    
    
  }//while
   
  return;
}



/*
//input one id
//output related ONE and only ONE name of id

//this function is called by geneOne2.php
*/
function getSpeciesFromGeneId ($id) {


  //input id
  $sqlString = filterGetSpecieFromGeneId ($id);
  
  //echo $sqlString;
  //exit();
  $result = mysql_query ($sqlString);

  list($name) = mysql_fetch_array ($result);
  
  return $name;
}


/*
//input one id
//output related ONE and only ONE name of id

//this function is called by geneOne2.php
*/
function getGenusSpeciesFromGeneId ($id) {


  //input id
  $sqlString = filterGetGenusSpecieFromGeneId ($id);
  
  //echo $sqlString;
  //exit();
  $result = mysql_query ($sqlString);

  list ($genus, $species) = mysql_fetch_array ($result);
  
  return $genus . " " . $species;
}



//input $dataSources2 ids
//output valid $dataSources names

function getDataSourcesFromGeneId ($gene, &$dataSources2, &$dataSources) {

  //input id
  $sqlString = filterToGetDataSources ($gene, $DataSources2);
  
  //echo $sqlString;
  //exit();
  $result = mysql_query ($sqlString);
  
  while (list($name) = mysql_fetch_array ($result)) { 
    
    $dataSources[] = $name;
    
  }//while
   
  return;

}
  


function getEvidenceCodesFromGeneId ($gene, &$evidenceCodes2, &$evidenceCodes) {

  //input id
  $sqlString = filterToGetEvidenceCodes ($gene, $evidenceCodes2);
  
  //echo $sqlString;
  //exit();
  $result = mysql_query($sqlString);
 
  while (list ($name) = mysql_fetch_array ($result)) { 
    
    $evidenceCodes[] = $name;
    
  }//while
   
  return;

}
  





/*
 The talbe of gene_product_seq
 go_associations.gene_product.id 	gene_product_id 	integer 	NOT NULL
 go_sequence.seq.id 	seq_id 	integer 	NOT NULL
 is_primary_seq 	integer 	

 If this link is for the representatibe sequence for a gene product, is_primary_seq should be set to true (=1)


 The third parameter is reserved for it for future use

*/



function getSequenceFromGeneId ($id, $isPrimary) {


  $sqlString = "SELECT seq.seq ";
  $sqlString .= "FROM gene_product ";
  $sqlString .= "INNER JOIN gene_product_seq ";
  $sqlString .= "ON gene_product.id = gene_product_seq.gene_product_id ";
  $sqlString .= "INNER JOIN seq ";
  $sqlString .= "ON gene_product_seq.seq_id = seq.id ";
  $sqlString .= "WHERE gene_product.id = $id;";

  //echo "\n<p>" . $sqlString . "</p>";


  $result = mysql_query ($sqlString);
  if (!$result) {
    echo "\n<p>There is an error when doing sequence $id.</p>";
    echo $sqlString . mysql_error();
    flush();
    return;
  }


  if (mysql_num_rows($result) == 0) {
    echo "\n<p>There is no sequences related with gene id $id.</p>";
    echo $sqlString;
    flush();
    return;
  }
  
  list ($sequence) = mysql_fetch_array ($result);
  
  return $sequence;
}



?>
