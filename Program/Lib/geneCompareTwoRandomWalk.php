<?php 
include_once ("synonym.php"); 
//include_once ("GOCompareTwo.php"); 
include_once ("GOCompareTwoRandomWalk.php"); 

include_once ("geneOne.php"); 
include_once ("Display/geneCompareTwo.php"); 
 

//contents
//compare two groups of terms -> compare two ids -> compare two symbols
//called by   geneTop            geneCompareMultiple       geneCompareTwo



//input are two terms arrays related with two gene ids

//if the caller needs to know the details of $GOTermComparisonTable, call this function
//otherwise, call compareTwoGeneIds, which will provide a dummy $GOTermComparisonTable
//to call this function


//$terms1 of gene1 are related with left column, which is given by the caller  
//$similaritiesMaxRow is 1D array related all rows

//$terms2 are related with top row
//$similaritiesMaxColumn is 1D array related all columns

function compareTwoGenesFromTermsOneOntologyRandomWalk ($terms1, $terms2, $isA, $partOf, &$TermComparisonTable) {


  $size1 = sizeof ($terms1);
  $size2 = sizeof ($terms2);
  
  
  if ($size1 == 0 || $size2 == 0) {
    return 0;
  }
  

  //term in term1 with all terms in terms
  //find the similarity of term in term1 with all terms in term2

  $similaritiesMaxRow = array();

  foreach ($terms1 as $term1) {
    
    $similarityMax = 0;

    foreach ($terms2 as $term2) {
      $similarity = compareTwoGOIdsRandomWalk ($term1, $term2, $isA, $partOf);
      
      
      
      //$similarity = compareTwoGOIds ($term1, $term2, $isA, $partOf);
      //save the maximum value
      if ($similarity > $similarityMax) {
	
	$similaritiesMaxRow[$term1] = $similarity;
	$similarityMax =  $similarity;
      }
      
      //generate the table for display by geneComparetwo
      $TermComparisonTable[$term1][$term2]  = $similarity;
    }
  }



  //find the similarity of term in term2 with all terms in term1
  $similaritiesMaxColumn = array();

  foreach ($terms2 as $term2) {

    $similarityMax = 0;
    foreach ($terms1 as $term1) {

  
      $similarity = $TermComparisonTable[$term1][$term2];

      //save the maxium value
      if ($similarity > $similarityMax) {
	$similaritiesMaxColumn[$term2] = $similarity;
	$similarityMax =  $similarity;
      }

    }//for
  }//for


  //do calculation
  $sum1 = array_sum ($similaritiesMaxRow);
  $sum2 = array_sum ($similaritiesMaxColumn);
  
  //  print_r($similarities1);

  $similarityResult = ($sum1 + $sum2) / ($size1 + $size2);

  return $similarityResult;
}






//input 

//output $similarities {BP, CC, MF}

//return $GOTermComparisonTable since geneTwo use it while others do not use it

//geneTwo (only one time O(1) is just for display)


function compareTwoGenesFromTermsThreeOntologiesRandomWalk ($terms1, $terms2, $isA, $partOf, &$similarities) {


  $TermComparisonTable = array();

  for ($index = 0; $index < 3; $index++) {

    $size1 = sizeof ($terms1[$index]);
    $size2 = sizeof ($terms2[$index]);
    
    
    if ($size1 == 0 || $size2 == 0) {
      $similarities[$index] = 0;
      continue;
    }
  
    $similarities[$index] = compareTwoGenesFromTermsOneOntologyRandomWalk ($terms1[$index], $terms2[$index], $isA, $partOf, $TermComparisonTable[$index]);
        
  }
  
  return $TermComparisonTable;
}









//this function is called 
//by geneCompareTwo3.php or
//by compareTwoGeneSymbols
//transfer id and symbol is to prevent synonyms


//this function will output tables (in display.php) and a hyperlink
//for the hyperlink, it only need send gene2 id since gene1 id has sent by session
//This is not consistant but to match geneTop.
//geneTop has source gene id, id1, and many other id2s.


//output:
//BP, CC and MF of $similarities = {0.34, 0.4, 0.6} for example

//Conveat: $dataSources and $evidenceCodes maybe used partially since there maybe not exist terms with some of them 

//using cache
function compareTwoGeneIdsRandomWalk ($id1, $id2, $ontologies, $dataSources1, $evidenceCodes1, $dataSources2, $evidenceCodes2, $isA, $partOf, &$similarities) {


  //print_r ($ontologies);

  //echo "Another: ";
  //print_r ($dataSources1);

  //echo $method;


  $terms1 = array();
  $terms1 = array_pad ($terms1, 3, array());
  getTermIdsFromGeneIdThreeOntologies ($id1, $ontologies, $dataSources1, $evidenceCodes1, $terms1);


  $terms2 = array();
  $terms2 = array_pad ($terms2, 3, array());
  getTermIdsFromGeneIdThreeOntologies ($id2, $ontologies, $dataSources2, $evidenceCodes2, $terms2);


  //no caches
  //2D
  //return the similarity
  $TermComparisonTable = compareTwoGenesFromTermsThreeOntologiesRandomWalk ($terms1, $terms2, $isA, $partOf, $similarities);


  //print_r ($similarities);
  return $TermComparisonTable;
}






//This is the main function of two gene symbols compare
//This function can only be called on time by geneComparetwo2.php
//so, it need to be pulled up in the future

function compareTwoGeneSymbolsRandomWalk ($symbol1, $symbol2, $ontologies, $species1, $dataSources1, $evidenceCodes1, $species2, $dataSources2, $evidenceCodes2, $isA, $partOf) {

  $geneIds1 = array();
  $number1 = getGeneIdsFromGeneSymbol ($symbol1, $ontologies, $species1, $dataSources1, $evidenceCodes1, $geneIds1);
    
  //can be merged into getGeneIdsFromGeneSymbol

  if ($number1 == 0) {

    searchOneSynonym ($symbol1, $synonym1);
        
    if ($symbol1 != $synonym1) {
      echo "\n<p>A synonym of gene " . $symbol1 . " is found: " . $synonym1 . ".</p>"; 
      $number1 = getGeneIdsFromGeneSymbol ($synonym1, $ontologies, $species1, $dataSources1, $evidenceCodes1, $geneIds1);

      if ($number1 == 0) { 
	echo "\n<p>No gene is found for gene: " . $symbol1;
	return;
      }
    }
    else {
      echo "\n<p>No gene is found for gene: " . $symbol1 . " based on the current filter.</p>";      
      return;
    }
  }
  

  $geneIds2 = array();
  $number2 = getGeneIdsFromGeneSymbol ($symbol2, $ontologies, $species2, $dataSources2, $evidenceCodes2, $geneIds2);

  if ($number2 == 0) {
       
    searchOneSynonym ($symbol2, $synonym2);
    
    if ($symbol2 != $synonym2) {
      echo "\n<p>A synonym of gene " . $symbol2 . " is found: " . $synonym2 . ".</p>"; 
      $number2 = getGeneIdsFromGeneSymbol ($synonym2, $ontologies, $species2, $dataSources2, $evidenceCodes2, $geneIds2);

      if ($number2 == 0) { 
	echo "\n<p>No gene is found for gene: " . $symbol2;
	return;
      }
    }
    else {
      echo "\n<p>No gene is found for gene: " . $symbol2 . " based on the current filter.</p>";      
      return;
    }
  }



  //check the flowchart for details
  if ($number1 > 1 || $number2 > 1) {    
    //output geneComparisionTwo2.php with geneComparisionTwo3.php hyperlinks
    //geneComparisionTwo3.php will be called
    displayIdsFromTwoGeneSymbols ($geneIds1, $geneIds2);
    return;
  }


  //the following codes are the same as geneCompareTwo3.php

  //3D

  $similarities = array();
  $similarities = array_pad ($similarities, 3, 0);

  $TermComparisonTable = array();
  $TermComparisonTable = array_pad ($TermComparisonTable, 3, array());

 
  //no need species since gene id can defer to it
  $TermComparisonTable = compareTwoGeneIdsRandomWalk ($geneIds1[0], $geneIds2[0], $ontologies, $dataSources1, $evidenceCodes1, $dataSources2, $evidenceCodes2, $isA, $partOf, $similarities);
  



  //the following is to display

  //options 1: using inputs geneid + user's input (evidence codes and data source) 
  //to getTermAll, including valid evidence codes and data source


  //options 2: using inputs geneid + terms to get evidence codes and data sources
  //better, since no need distinguish the ontology now
  //getTermsAllFromGeneId 

  //print_r( $similarities );
  displayGeneCompareTwo ($geneIds1[0], $geneIds2[0], $similarities, $TermComparisonTable);
   
  return;
}

?>
