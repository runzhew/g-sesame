<?php

include_once( "../Lib/database.php" ); 

//let programs run forever
set_time_limit( 0 );
ignore_user_abort( true );



function flush_rows() {
  global $rows;
  $sqlString = "INSERT INTO graph_path2_temp ( relationship_type_id, term1_id, term2_id, term3_id ) VALUES " . implode( ',', $rows ) . ";";

  mysql_query( $sqlString ) or die( mysql_error() );

  //flush rows
  $rows = array();
}




//recursive method 
function create_path( $term2_id, $term3_id ) {
  global $rows;
  $query = "SELECT relationship_type_id, term1_id FROM term2term WHERE term2_id = $term2_id;";
  $result = mysql_query( $query ) or die( mysql_error() );

  while( list( $rel_id, $term1_id ) = mysql_fetch_row( $result ) ) {
    array_push( $rows, "( $rel_id, $term1_id, $term2_id, $term3_id )" );
    if( count( $rows ) == 100 ) {
      flush_rows();
      echo "$term3_id\n";
    }

    //recursive
    //$term3_id keep the same
    create_path( $term1_id, $term3_id );
  }  
}



//global array
$rows = array();



//database
$link = connectToMySQL();


//we assume that graph_path2 table exist, otherwise, program will not run
//$sqlString = "DROP TABLE graph_path2;";
//mysql_query( $sqlString ) or die( mysql_error() );


//for security reasons, if try to re-create the table graph_path2 by online users,  the program will exit;

//administors have to login mysql to manuallly drop the table to let program 

$sqlStringHead = "CREATE TABLE graph_path2 ";

$sqlStringTail = "( id SERIAL PRIMARY KEY, ";
$sqlStringTail .= "relationship_type_id INTEGER NOT NULL, ";
$sqlStringTail .= "FOREIGN KEY ( relationship_type_id ) REFERENCES term( id ), ";

//parent
$sqlStringTail .= "term1_id INTEGER NOT NULL, ";
$sqlStringTail .= "FOREIGN KEY ( term1_id ) REFERENCES term( id ), ";

//child
$sqlStringTail .= "term2_id INTEGER NOT NULL, ";
$sqlStringTail .= "FOREIGN KEY ( term2_id ) REFERENCES term( id ), ";

//lowest point, the start point
$sqlStringTail .= "term3_id INTEGER NOT NULL, ";
$sqlStringTail .= "FOREIGN KEY ( term3_id ) REFERENCES term( id ) ";
$sqlStringTail .= ");";


$sqlString = $sqlStringHead . $sqlStringTail;



//prepare some error messages
$readme = "\n<p>Click this file ONLY once to build up new table.</p>";

$readme .= "\n<p>For security reasons, if users try to re-create the table graph_path2, the program will exit.</p>";

$readme .= "\n<p>Administors have to login mysql to manuallly drop the table graph_path2 by \"DROP TABLE graph_path2;\" to let program continue run.</p>";


mysql_query( $sqlString ) or die( mysql_error() . $readme );

$sqlString = "CREATE INDEX graph_path2 on graph_path2( term3_id );";
mysql_query( $sqlString ) or die( mysql_error() );





//we assume that graph_path2 table2 does not exist, otherwise, program will not run
$sqlStringHead = "CREATE TABLE graph_path2_temp ";

$sqlString = $sqlStringHead . $sqlStringTail;

mysql_query( $sqlString ) or die( mysql_error() );



$query = "SELECT id FROM term;";
$result = mysql_query( $query ) or die( mysql_error() );

while( list( $id ) = mysql_fetch_row( $result ) ) {
  //same parameter, recursivly called
  create_path( $id, $id );
 }


//insert into the my_graph_table2 including duplicate rows
flush_rows();



//copy distinct rows from graph_path2_temp to graph_path2

echo "<p>Copying distinct rows from graph_path2_temp to graph_path2.</p>";
flush();

$sqlString = "INSERT INTO graph_path2 ( relationship_type_id, term1_id, term2_id, term3_id ) ";
$sqlString .= "SELECT DISTINCT relationship_type_id, term1_id, term2_id, term3_id ";
$sqlString .= "FROM graph_path2_temp;";
mysql_query( $sqlString ) or die( mysql_error() );



//clean the database
$sqlString = "DROP TABLE graph_path2_temp;";
mysql_query( $sqlString ) or die( mysql_error() );


echo "<p>graph_path2 table has been successfully created.</p>";



/*


//the following from 

//http://amigo.geneontology.org/dev/sql/doc/example-queries.html

//Finding ancestor terms and their relationships
Original example:
mysql> SELECT DISTINCT r.term1_id, r.term2_id, r.relationship_type_id FROM graph_path INNER JOIN term AS t ON ( t.id = graph_path.term2_id ) INNER JOIN term AS p ON ( p.id = graph_path.term1_id ) INNER JOIN term2term AS r ON (r.term2_id = p.id) WHERE t.id = 3728;
+----------+----------+----------------------+
| term1_id | term2_id | relationship_type_id |
+----------+----------+----------------------+
|    16307 |     3728 |                    2 |
|    16854 |     3728 |                    2 |
|    16834 |    16854 |                    2 |
|     3727 |    16854 |                    8 |
|    16834 |     3727 |                    2 |
|    16873 |    16834 |                    2 |
|     3618 |    16834 |                    8 |
|     3571 |    16873 |                    2 |
|     3619 |    16873 |                    8 |
|     3571 |     3619 |                    2 |
|        1 |     3571 |                    2 |
|    16873 |     3618 |                    2 |
|    16303 |    16307 |                    2 |
|    16305 |    16307 |                    2 |
|    16302 |    16305 |                    2 |
|    16834 |    16305 |                    2 |
|     3571 |    16302 |                    2 |
|    16302 |    16303 |                    2 |
+----------+----------+----------------------+
18 rows in set (0.00 sec)



new example
mysql> SELECT term1_id, term2_id, relationship_type_id FROM graph_path2 WHERE term3_id = 3728;
+----------+----------+----------------------+
| term1_id | term2_id | relationship_type_id |
+----------+----------+----------------------+
|    16307 |     3728 |                    2 |
|    16303 |    16307 |                    2 |
|    16302 |    16303 |                    2 |
|     3571 |    16302 |                    2 |
|        1 |     3571 |                    2 |
|    16305 |    16307 |                    2 |
|    16302 |    16305 |                    2 |
|    16834 |    16305 |                    2 |
|    16873 |    16834 |                    2 |
|     3571 |    16873 |                    2 |
|     3619 |    16873 |                    8 |
|     3571 |     3619 |                    2 |
|     3618 |    16834 |                    8 |
|    16873 |     3618 |                    2 |
|    16854 |     3728 |                    2 |
|    16834 |    16854 |                    2 |
|     3727 |    16854 |                    8 |
|    16834 |     3727 |                    2 |
+----------+----------+----------------------+
18 rows in set (0.00 sec)

*/


mysql_close( $link );

?>
